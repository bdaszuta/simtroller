#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ scalar outputs conveniently

# TODO
- Waveforms, Trackers
"""
###############################################################################
from numpy import (abs, sqrt, vstack)

import matplotlib.pyplot as plt

from functools import reduce
from typing import Any, Union, Optional, Sequence, Type

import argparse as _ap

# package imports
import simtroller as st

###############################################################################
def sanitize(arg,
             convert_to=None,
             delimiter=",",
             sort=False):
  # sanitize input arguments which are delimited
  try:
    if len(arg) == 1:
      arg = arg[0].split(delimiter)
  except:
    pass

  if convert_to is not None:
    arg = [convert_to(val) for val in arg]

  if sort:
    arg = sorted(arg)
  return tuple(arg)

def listlike_of_str_delim_to_dict(listlike,
                                  delimiter=":",
                                  convert_to=str):
  if listlike is None:
    return {}

  out = {}
  for el in listlike:
    val_A, val_B = el.split(delimiter)
    out[val_A] = sanitize([val_B], convert_to=convert_to)
  return out

# parsing of input options

arg_parser = _ap.ArgumentParser(
  prog="cmd_sca_gra",
  description='simtroller based scalar visualization for gra',
  epilog=":D"
)

arg_parser.add_argument('--dir_data_base',
                        type=str,
                        required=False,
                        help='Path to data directory to process.')

arg_parser.add_argument('--sim_tags',
                        nargs="*",
                        required=False,
                        help='Subdir(s) (of dir_data_base) containing ' +
                             'scalar data.')

arg_parser.add_argument('--dirs_data',
                        nargs="*",
                        required=False,
                        help='Full paths to data directories.')

arg_parser.add_argument('--dir_out',
                        type=str,
                        required=False,
                        default="${PWD}",
                        help='Path to dump images to.')

arg_parser.add_argument('--fn_out',
                        type=str,
                        required=False,
                        default="data",
                        help='Output filename for data & image.')

arg_parser.add_argument('--deduplicate',
                        action='store_true',
                        default=True,
                        required=False,
                        help='Ensure samples in time unique.')

arg_parser.add_argument('--downsample_factor',
                        type=int,
                        default=1,
                        required=False,
                        help='Reduce number of samples being shown')

str_transform = ('Transformation can be applied to data; ' +
                 'seperate with a colon. ' +
                 'Must be an element of: ' +
                 '{abs, err_abs_rel_0, sqrt_abs}')

arg_parser.add_argument('--var_hst',
                        nargs="*",
                        required=False,
                        help='Names of variables to extract from hst ' +
                             'files. Each variable shown in a new panel. ' +
                             str_transform)

arg_parser.add_argument('--plot_scaling',
                        nargs="*",
                        default=["linear", ],
                        required=False,
                        help='Scaling for plot. ' +
                             'One of: {linear, loglog, semilogy}.')

arg_parser.add_argument('--range',
                        nargs="*",
                        required=False,
                        help='Range to restrict independent variable to.')

arg_parser.add_argument('--var_hst_range',
                        nargs="*",
                        required=False,
                        help='Restrict min/max range of dependent variables.')

# legend & labels
arg_parser.add_argument('--legend_enable',
                        action='store_true',
                        default=True,
                        required=False,
                        help='Control whether legend is depicted.')

arg_parser.add_argument('--legend_labels',
                        nargs="*",
                        required=False,
                        help='Provide explicit legend labels.')

arg_parser.add_argument('--plot_show',
                        action='store_true',
                        default=False,
                        required=False,
                        help='Control whether plot is shown.')


args = arg_parser.parse_args()

# extract to variables ########################################################

# directory parsing

opt_dir_data_base = args.dir_data_base
opt_sim_tags = args.sim_tags

opt_dirs_data = args.dirs_data

if ((opt_dir_data_base is None) and
    (opt_sim_tags is None) and
    (opt_dirs_data is None)):
  raise ValueError("Need at least one specification of data directory")

if ((opt_dir_data_base is None) and
    (opt_sim_tags is None)):
  raise ValueError("When providing dir_data_base need at least one sim_tags")

opt_dirs = []
if (opt_dir_data_base is not None) and (opt_sim_tags is not None):
  for sim_tag in opt_sim_tags:
    opt_dirs.append(st.core.io.path_to_absolute([opt_dir_data_base, sim_tag]))

if opt_dirs_data is not None:
  for dd in opt_dirs_data:
    opt_dirs.append(dd)

opt_dir_out = args.dir_out
opt_fn_out = args.fn_out

# other options
opt_deduplicate = args.deduplicate
opt_downsample_factor = args.downsample_factor

opt_var_hst = args.var_hst
opt_var_hst_num = 0 if (opt_var_hst is None) else len(opt_var_hst)


opt_var_hst_range = listlike_of_str_delim_to_dict(args.var_hst_range,
                                                  convert_to=float)

# ... TODO: other scalar var types

opt_var_num = opt_var_hst_num # + TODO: other scalar var types

# process transforms (indicated by colon operator)
map_transform = {
  "identity"   : lambda x: x,
  "abs"        : lambda x: abs(x),
  "sqrt_abs"   : lambda x: sqrt(abs(x)),
  "err_abs_rel": lambda x: abs(1 - x / x[0]),
  "err_abs_rel_1": lambda x: abs(1 - x / x[1])
}

opt_plot_scaling = args.plot_scaling
if len(opt_plot_scaling) == 1:
  opt_plot_scaling = opt_plot_scaling * opt_var_num
elif len(opt_plot_scaling) != opt_var_num:
  raise ValueError("Provide a uniform specification for plot_scalar " +
                   "or one for each.")


# change variable independent axis range
opt_range_restricted = not (args.range is None)
if opt_range_restricted:
  opt_range = sanitize(args.range, float)

opt_legend_enable = args.legend_enable
opt_legend_labels = args.legend_labels
if opt_legend_labels is not None:
  if len(args.legend_labels) != opt_var_num:
    raise ValueError("Number provided legend labels does not match " +
                     "number of datasets to plot.")

opt_plot_show = args.plot_show

###############################################################################

class scrape_dir_scalar(object):
  def __init__(self, dir_data : Any):

    self.dir_data = st.core.io.path_to_absolute(dir_data)

    # extract scalar data
    self.fns_hst, self.fns_txt = self._parse_fns()

  def _parse_fns(self):
    # parse relevant data dir for relevant files (sorted)

    dir_list = st.core.io.dir_list(self.dir_data, recursive=False)
    fns_hst = st.core.io.dir_filter(dir_list,
                                    mode="directories",
                                    extensions=[".hst"])

    fns_txt = st.core.io.dir_filter(dir_list,
                                    mode="directories",
                                    extensions=[".txt"])

    return fns_hst, fns_txt

  def get_var_from_hst(self, var : str, deduplicate : bool=True) -> tuple:
    """
    Extract data from hst, optional deduplication

    Parameters
    ----------
    var : str
      Variable to extract (by column label).

    deduplicate : bool = True
      Control deduplication in time.

    Returns
    -------
      (T, data_var)
    """

    # TODO: N.B. will only extract from first hst encountered
    if len(self.fns_hst) == 0:
      raise ValueError(f"Directory {self.dir_data} contains no hst.")

    fn_hst = self.fns_hst[0]

    labels, data = st.extensions.gra.hst_load(fn_hst)

    if var not in labels:
      print("Have variables:")
      print(labels)
      raise ValueError(f"Choice {var} unknown.")


    ix_time = labels.index("time")
    ix_var  = labels.index(var)

    T, data_var = data[:, ix_time], data[:, ix_var]

    if deduplicate:
      T, data_var = st.core.numerical.scalar_data_deduplicate_sort(
        vstack((T, data_var)).T, column=0
      ).T

    return T, data_var

###############################################################################


# extract general information from files
scraped = [scrape_dir_scalar(dir_sim) for dir_sim in opt_dirs]

# assemble desired figure #####################################################

# setup figure


fig_width = 12
fig_height = 8

fig_A, ax_A = plt.subplots(opt_var_num,
                           sharex=True,
                           figsize=(fig_width, fig_height))

ix_var = 0

if opt_var_hst_num > 0:
  # need to plot history data

  for vix, var in enumerate(opt_var_hst):
    # prepare transforms
    if ":" in var:
      var, var_transform = var.split(":")
    else:
      var_transform = "identity"

    # iterate over known data-sets
    for six, scra in enumerate(scraped):
      data = scra.get_var_from_hst(var, deduplicate=opt_deduplicate)
      time, data_var = data

      # apply transformation before plot
      data_var = map_transform[var_transform](data_var)

      dsf = opt_downsample_factor
      if opt_downsample_factor > 1:
        time_, data_var_ = time[::dsf], data_var[::dsf]
      else:
        time_, data_var_ = time, data_var

      if opt_plot_scaling[vix] == "linear":
        ax_A[ix_var].plot(time_, data_var_)
      elif opt_plot_scaling[vix] == "semilogy":
        ax_A[ix_var].semilogy(time_, data_var_)
      elif opt_plot_scaling[vix] == "loglog":
        ax_A[ix_var].loglog(time_, data_var_)
      else:
        raise ValueError(f"plot_scaling: {opt_plot_scaling[vix]} unknown.")

    # put label on each sub-plot
    if var_transform == "identity":
      ax_A[ix_var].set_ylabel(f"{var}")
    else:
      ax_A[ix_var].set_ylabel(f"{var_transform} o ({var})")

    # rescale y if so desired
    if var in opt_var_hst_range:
      ax_A[ix_var].set_ylim(*opt_var_hst_range[var])

    ix_var += 1

# TODO: check next scalar var types..
# ...

if opt_legend_enable:

  if opt_legend_labels is None:
    # generate labels if none provided
    legend_dir_depth_label = 2
    opt_legend_labels = [
      "/".join(dir.split("/")[-legend_dir_depth_label:])
      for dir in opt_dirs
    ]

  ax_A[0].legend(opt_legend_labels, loc="upper right", fontsize="small")


if opt_range_restricted:
  ax_A[ix_var-1].set_xlim(opt_range)

ax_A[ix_var-1].set_xlabel("t")
fig_A.align_ylabels()
fig_A.tight_layout()
fig_A.subplots_adjust(hspace=0.)


# save output:
st.core.io.dir_generate(opt_dir_out, signal=False)


fn_fig = opt_fn_out + ".png"
fn_full_fig = st.core.io.path_to_absolute([opt_dir_out, fn_fig],
                                          verify=False)

plt.savefig(fn_full_fig)

if opt_plot_show:
  plt.show()
else:
  plt.close()

#
# :D
#
