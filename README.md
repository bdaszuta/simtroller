# simtroller
Tool for processing / managing simulations and inspecting their data.

Some adapted extensions for processing:
  - GR-Athena++
  - BAM

## quickstart
```bash
source init.sh  # add simtroller to PYTHONPATH / any other future setup
```

```python
import simtroller as st
```

## QOL (quality of life - athdf)
- "I ran a simulation and have a directory full of athdf."
- "I want to make some figures quickly from the command line."
- "I remembered to dump things using `ghost_zones = true`."
- This can now be done as follows:

```bash
source init.sh

# path to directory containing *.athdf
export dir_data=...
# output directory for images: structure automatically created if missing
export dir_out=/tmp/debug

# generate plots for range of iterates of 2d data
ipython -i cmd_vis_gra.py -- \
  --dir_data ${dir_data}     \
  --dir_out ${dir_out}       \
  --iterate 2 7              \
  --N_B 16 16                \
  --sampling x1f x2f         \
  --var adm.gxx

# generate 1d slices from 2d data and make plots with 4 process parallel pool
# - Every fifth iterate considered
# - rho variable has absolute value taken
# - plot scaling logarithmic
# - data dumped to obj
ipython -i cmd_vis_gra.py -- \
  --dir_data ${dir_data}     \
  --dir_out ${dir_out}       \
  --iterate 0 20 5           \
  --N_B 16                   \
  --sampling [x1v,x2v]       \
  --range [-50,50] 3.7       \
  --parallel_pool 4          \
  --var rho:abs              \
  --plot_scaling semilogy    \
  --data_save 1

# generate mp4/gif of output (requires ffmpeg)
# - Here all available iterates (inferred) are utilized
# - Superpose quiver in 2d showing fluid prim vel 1, 2
ipython -i cmd_vis_gra.py -- \
  --dir_data ${dir_data}     \
  --dir_out ${dir_out}       \
  --N_B 16 16                \
  --sampling [x1v,x2v]       \
  --range [-75,75] [-75,75]  \
  --parallel_pool 8          \
  --var rho:abs vel1 vel2    \
  --plot_scaling log         \
  --plot_quiver [vel1,vel2]  \
  --make_movie 1             \
  --movie_fps 15

# Note: variables can be transformed with colon operator
```

## QOL (quality of life - hst)
- As for `athdf` but for `hst` and `txt` inspection

```bash
source init.sh

# path to directory containing differing simulations
export dir_data_base=...

# output directory for images: structure automatically created if missing
export dir_out=/tmp/debug

ipython -i cmd_sca_gra.py --                                \
  --dir_data_base=${dir_data_base}                          \
  --sim_tags cx.X5.ninterp1/smr.l04.NM_064.NB_016.XD_0256   \
             cx.X5.ninterp1/smr.l05.NM_064.NB_016.XD_0256   \
             cx.X5.ninterp1/smr.l06.NM_064.NB_016.XD_0256   \
  --var_hst H-norm2:sqrt_abs max-rho:err_abs_rel            \
  --var_hst_range H-norm2:1e-5,1e-2                         \
  --plot_scaling semilogy                                   \
  --range 0 150                                             \
  --dir_out=${dir_out}                                      \
  --fn_out="cx.X5.ninterp1.smr.0256"                        \
  --plot_show

# Note: variables can be transformed with colon operator
```

## Package structure
Run `dir` within interpreter for function listing.

simtroller.core
simtroller.core.io
simtroller.core.numerical
simtroller.core.primitives
simtroller.core.shell

simtroller.extensions
simtroller.extensions.bam
simtroller.extensions.gra
simtroller.waveforms

See `examples` for some self-contained scripts that demonstrate functionality. The folder `scripts` also may be helpful.

When running from terminal set environmental variable `NR_DATA` to correctly parse locations (see script first).

### note on parallel usage
- Some functions have a `parallel_pool` kwarg. In these cases execution may be performed in parallel.

- Cheat sheet for running interactive scripts:
```bash
# interactive run on cluster
srun --nodes=1 --partition=b_test -n 48 -c 1 --time=03:00:00 --pty bash -i
```
- Cheat sheet for making movie. Given a directory `$PWD/rho_pin` of `png` convert to `mp4` using `ffmpeg`
```bash
ffmpeg -framerate 40 -pattern_type glob -i "$PWD/rho_pin/*.png" -c:v libx264 -pix_fmt yuv420p rho_pin_log10.mp4
```

## satisfy package requirements with conda
```bash
# prevent issues with hard/soft links
conda config --set always_copy true

# create new environment
conda create -n dev -c conda-forge python=3.11 \
  numpy                         \
  pandas                        \
  scipy                         \
  mpmath                        \
  sympy                         \
  ipython                       \
  numba                         \
  h5py                          \
  dill                          \
  matplotlib                    \
  pytest                        \
  pylint                        \
  mypy                          \
  conda-pack

# if there are conda-pack issues try conda-forge channel:
# conda install -c conda-forge conda-pack

# pack it (if you want to deploy)
export ENV_CONDA_NAME=dev
conda pack -n ${ENV_CONDA_NAME} --ignore-missing-files
```
### deployment on (e.g. cluster) with conda
```bash
export ENV_CONDA_NAME=dev
mkdir -p conda_${ENV_CONDA_NAME}
tar -xzf ${ENV_CONDA_NAME}.tar.gz -C conda_${ENV_CONDA_NAME}

# activate via
source ${HOME}/soft/conda_${ENV_CONDA_NAME}/bin/activate

# deactivate via
source ${HOME}/soft/conda_${ENV_CONDA_NAME}/bin/deactivate
```

## possible issues
When segmentation faults occur with numba-jit functions it can be helpful to call function_name.pyfunc - this will execute the native python implementation and can reveal things like index errors etc.

Templated submission scripts may have issues if submitted from activated conda environment.

## internals: naming conventions

In general the naming scheme for functions is ${thing}_${operation}


## TODO:
- Clean up window / extrap to strain FFI
- Add initialisation based on .json profile
- Incorporate `spack` environments for execution policies
