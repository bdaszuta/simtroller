#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ athdf outputs conveniently

# TODO
- title: plot 2d vars (include transform)
- title: dx min/max & slicing info
- args: collate & save processed data for later post-proc
- consider combining scalar processing?
- scalar trackers could be used for moving grid
- non-Cartesian
"""
###############################################################################
import matplotlib.pyplot as plt

from functools import reduce
from typing import Any, Union, Optional, Sequence, Type

from numpy import (abs, argsort, array, float64, linspace,
                   log10, logical_and, logical_not,
                   meshgrid, sign, sqrt)

import argparse as _ap

# package imports
import simtroller as st

###############################################################################
def str_to_bool(input : str) -> bool:
  if input in {"f", "F", "false", "False", "0", False}:
    return False
  return True

def sanitize_nargs_as_tuple_of_tuple(
  input,
  delimiter : str=",",
  convert_to: Any=None
):
  if input is None:
    return input

  # TODO: if spaces occur i.e. "[a, b]" then parsing fails
  pinput = st.core.primitives.str_flat_list_like_parser(
    delimiter.join(input), delimiter=delimiter, convert_to=convert_to
  )

  if not isinstance(pinput[0], tuple):
    pinput = (pinput, )

  return pinput

def narray_from_range(ranges, N_I):
  # given range specification and num. of interp. samples, make grids

  gen_N_I = (N for N in N_I)

  ret = []
  for rix, ran in enumerate(ranges):
    if isinstance(ran, tuple):
      ret.append(linspace(*ran, num=next(gen_N_I)))
    else:
      ret.append(array([ran, ], dtype=float64))
  return tuple(ret)

def iterate_range(start, end=None, step=1):
  # Ranges from: (start,), (start, end), (start, end, step)
  # with end-points included
  if end is None:
    return range(start, start+1)
  return range(start, end+1, step)

def sequence_take_by_bool(seq, bool_seq, truth_value=True):
  # given two sequences of equal length, take from one by truth until exhausted
  return (val for val, tval in zip(seq, bool_seq) if (tval == truth_value))

def dict_singleton_container_expand(
  opt              : dict,
  key_repeat       : str,
  key_repeat_by    : str,
  fail_on_none     : bool = True,
  fail_on_mismatch : bool = False
):
  # Given a unit-length listlike extend (by copy) to the length of another
  if opt[key_repeat] is None:
    if fail_on_none:
      raise ValueError(f"'{key_repeat}' cannot correspond to a " +
                       f"'None' entry.")

    return

  len_kr  = len(opt[key_repeat])
  len_krb = len(opt[key_repeat_by])

  if len_kr == 1:
    opt[key_repeat] = opt[key_repeat] * len_krb
  elif len_kr != len_krb:
    if fail_on_mismatch:
      raise ValueError(f"'{key_repeat}' must be length 1 or same length " +
                       f"as '{key_repeat_by}'.")

def dict_sort_iterable_elements(
  opt: dict,
  keys_to_sort : Union[str, list, tuple],
  key_sort_by  : str,
  fail_skip    : bool = False
):
  # Given dictionary containing lists, sort those by a list-element of the
  # dictionary.

  # use numpy for sorting
  ix_sort = argsort(opt[key_sort_by])

  keys_to_sort = st.core.primitives.tuple_like(keys_to_sort)

  for key in keys_to_sort:
    try:
      to_sort = opt[key]
      opt[key] = tuple(to_sort[ix] for ix in ix_sort)
    except:
      if fail_skip:
        pass
      else:
        raise ValueError("Sorting failed")

def sequence_count_by_type(seq, type=None):
  # given a sequence count the number of times a given type appears
  count = 0
  for el in seq:
    count += isinstance(el, type)
  return count

def ensure_tuple_len(input, length=2):
  if len(input) == 1:
    return tuple([input[0], ] * length)

  return tuple(input[ix] for ix in range(length))

# parsing of input options

arg_parser = _ap.ArgumentParser(
  prog="cmd_vis_gra",
  description='simtroller based athdf visualization for gra',
  epilog=":D"
)

arg_parser.add_argument('--dir_data',
                        type=str,
                        required=True,
                        help='Path to data directory to process.')

arg_parser.add_argument('--dir_out',
                        type=str,
                        required=False,
                        default="${PWD}",
                        help='Ouput directory.')

arg_parser.add_argument('--N_B',
                        type=int,
                        nargs="*",
                        required=True,
                        help='MeshBlock cell-size (without NGHOST).')

arg_parser.add_argument('--sampling',
                        nargs="*",
                        type=str,
                        required=True,
                        help='Specify axes and (f)ace or (v)olume centering.' +
                             'Example: x1f x2v. ' +
                             'Example (multiple variables): ' +
                             '[x1v,x2v] [x1f,x2f]')

arg_parser.add_argument('--range',
                        nargs="*",
                        required=False,
                        help='Ranges to restrict sampling to.')

arg_parser.add_argument('--interpolate_N',
                        nargs="*",
                        type=int,
                        required=False,
                        default=[256, ],
                        help='Number of samples for interpolation when ' +
                             'range restricts an axis to a scalar.')

arg_parser.add_argument("--interpolate",
                        action='store_true',
                        default=False,
                        required=False,
                        help='Control directly whether to interpolate.')

arg_parser.add_argument("--interpolate_method",
                        default="linear",
                        required=False,
                        help='Control interpolation method. {"linear", "FH"}')

str_transform = ('Transformation can be applied to data; ' +
                 'seperate with a colon. ' +
                 'Must be an element of: ' +
                 '{id, abs, sign, sqr, sqrt_abs}')

arg_parser.add_argument('--var',
                        nargs="*",
                        type=str,
                        required=True,
                        help='Name of variable(s) to extract. ' +
                             str_transform)

arg_parser.add_argument('--boundary_levels',
                        nargs="*",
                        type=int,
                        default=(0, 20),
                        required=False,
                        help='Show specified levels.')

arg_parser.add_argument('--iterate',
                        nargs="*",
                        type=int,
                        required=False,
                        help='Iterates to parse. Optionally provide: ' +
                             'First, last, step. ' +
                             'If not provided, all iterates parsed.')

arg_parser.add_argument('--time',
                        nargs="*",
                        type=float,
                        required=False,
                        help='Time(s) to map to iterates.')

arg_parser.add_argument('--plot_quiver',
                        nargs="*",
                        required=False,
                        help='Optionally superpose quiver plot of fields.')

arg_parser.add_argument('--plot_scaling',
                        type=str,
                        default="linear",
                        required=False,
                        help='Scaling for plot. ' +
                             'For 1d, one of: {linear, loglog, semilogy}.' +
                             'For nd (n>1), one of: {linear, log}.')

arg_parser.add_argument('--plot_range',
                        nargs="*",
                        default=None,
                        required=False,
                        help='Restrict min/max range of dependent variables.')

arg_parser.add_argument('--plot_show',
                        type=str,
                        default=False,
                        required=False,
                        help='Control whether plot is shown.')

arg_parser.add_argument('--plot_save',
                        type=str,
                        default=True,
                        required=False,
                        help='Control whether plot is saved.')

arg_parser.add_argument('--plot_boundaries',
                        type=str,
                        default=True,
                        required=False,
                        help='Control whether MeshBlock boundaries are shown.')

arg_parser.add_argument('--parallel_pool',
                        type=int,
                        required=False,
                        help='Spin up a parallel pool for faster processing.')

arg_parser.add_argument('--data_save',
                        type=str,
                        default=False,
                        required=False,
                        help='Control whether parsed data is saved.')

arg_parser.add_argument('--no_plot',
                        type=str,
                        default=False,
                        required=False,
                        help='Control whether to make plots at all.')

arg_parser.add_argument('--make_movie',
                        type=str,
                        default=False,
                        required=False,
                        help='Make gif & mp4 from png (requires ffmpeg).')

arg_parser.add_argument('--movie_fps',
                        type=int,
                        default=30,
                        required=False,
                        help='Specify fps option when making movie.')


# TODO: if scalar visualisation script is fused parse subset
# args_a = arg_parser.parse_known_args()
args = arg_parser.parse_args()

###############################################################################

# convert sampling specification to tuple of tuples

all_opt = {
  "dir_data": args.dir_data,
  "dir_out" : args.dir_out,
  "sampling": sanitize_nargs_as_tuple_of_tuple(args.sampling,
                                               delimiter=",",
                                               convert_to=str),
  "var"            : tuple(args.var),
  "var_transform"  : None,
  "N_B"            : None,
  "boundary_levels": ensure_tuple_len(args.boundary_levels, length=2),
  "opt"    : {},
  # Additional plot element control
  "plot_quiver": sanitize_nargs_as_tuple_of_tuple(args.plot_quiver,
                                                  delimiter=",",
                                                  convert_to=str),
  "plot_show"   : str_to_bool(args.plot_show),
  "plot_save"   : str_to_bool(args.plot_save),
  "plot_scaling": args.plot_scaling,
  "plot_range"  : sanitize_nargs_as_tuple_of_tuple(args.plot_range,
                                                   delimiter=",",
                                                   convert_to=float),
  "plot_boundaries": str_to_bool(args.plot_boundaries),
  "parallel_pool"  : args.parallel_pool,
  "data_save"      : str_to_bool(args.data_save),
  "no_plot"        : str_to_bool(args.no_plot),
  "make_movie"     : str_to_bool(args.make_movie),
  "movie_fps"      : args.movie_fps,
}

# parse variable transformations
all_opt["var"] = [var + ":id" if ":" not in var else var
                  for var in all_opt["var"]]

all_opt["var_transform"] = {
  var[:var.index(":")]: var[var.index(":")+1:]
  for var in all_opt["var"]
}

all_opt["var"] = tuple(var[:var.index(":")] for var in all_opt["var"])


dict_singleton_container_expand(all_opt, "sampling", "var",
                                fail_on_mismatch=True)

# variable transformation functions (indicated by colon operator)
map_transform = {
  "id"      : lambda x: x,
  "abs"     : lambda x: abs(x),
  "sign"    : lambda x: sign(x),
  "sqr"     : lambda x: x * x,
  "sqrt_abs": lambda x: sqrt(abs(x)),
}

# iterate over each variable / sampling #######################################

for var, sampling in zip(all_opt["var"], all_opt["sampling"]):

  # setup each variable -------------------------------------------------------
  opt = {
    "sampling": sampling,
    "N_B"     : args.N_B,
    "range"   : args.range,
    "dim_data"    : len(sampling),
    "reduced_dim_data": None,
    "reduced_axis"    : [False, ] * len(sampling),
    "reduced_sampling": None,
    "reduced_sampling_fixed": None,
    "interpolate"       : args.interpolate,
    "interpolate_N"     : args.interpolate_N,
    "interpolate_method": args.interpolate_method,
    "var"    : var,
  }

  dict_singleton_container_expand(opt, "N_B", "sampling",
                                  fail_on_mismatch=True)
  dict_singleton_container_expand(opt, "range", "sampling",
                                  fail_on_none=False,
                                  fail_on_mismatch=True)

  # parse range specifications to numerical types
  # TODO: if spaces occur i.e. "[a, b]" then parsing fails
  if opt["range"] is not None:
    opt["range"] = st.core.primitives.str_flat_list_like_parser(
      ",".join(opt["range"]), delimiter=",", convert_to=float
    )

  # parse resultant dimension & axis reductions
  if opt["range"] is not None:
    opt["reduced_dim_data"] = sequence_count_by_type(opt["range"], tuple)

    opt["reduced_axis"] = tuple(
      not isinstance(ax_range, tuple)
                     for ax, ax_range in enumerate(opt["range"])
    )

  else:
    opt["reduced_dim_data"] = opt["dim_data"]

  opt["reduced_sampling"] = tuple(
    array(opt["sampling"])[logical_not(array(opt["reduced_axis"]))]
  )

  opt["reduced_sampling_fixed"] = tuple(
    array(opt["sampling"])[array(opt["reduced_axis"])]
  )

  # interpolation settings
  dict_singleton_container_expand(opt, "interpolate_N", "reduced_sampling",
                                  fail_on_none=False,
                                  fail_on_mismatch=True)

  # variable canonicalization -------------------------------------------------
  # reorder with 'sampling'
  dict_sort_iterable_elements(
    opt,
    ("sampling", "N_B", "range", "reduced_axis"),
    "sampling",
    fail_skip=True
  )

  # reorder with 'reduced_sampling_fixed'
  dict_sort_iterable_elements(
    opt,
    ("reduced_sampling_fixed", ),
    "reduced_sampling_fixed",
    fail_skip=True
  )

  # reorder with 'reduced_sampling'
  dict_sort_iterable_elements(
    opt,
    ("reduced_sampling", "interpolate_N"),
    "reduced_sampling",
    fail_skip=True
  )

  # other runtime settings ----------------------------------------------------

  # reductions require interpolation
  reduction_required = opt["dim_data"] != opt["reduced_dim_data"]
  if reduction_required:
    opt["interpolate"] = True


  # retain settings by var
  all_opt["opt"][var] = opt
  all_opt["N_B"] = opt["N_B"]

###############################################################################
# extract general information from files
sim_data_meta = st.extensions.gra.scrape_dir_athdf(
  all_opt["dir_data"], all_opt["N_B"]
)

# storage for retention of data (if desired)
sim_data = {}

# further processing of options based on data characteristics -----------------
for var, sampling in zip(all_opt["var"], all_opt["sampling"]):

  if args.iterate is None:
    if args.time is not None:
      # use time range to get iters
      time = ensure_tuple_len(args.time, length=2)
      args.iterate = tuple(
        sim_data_meta.get_iter_from_time(var, sampling, t)
        for t in time
      )

    else:
      # get all iterates available
      args.iterate = sim_data_meta.get_iter_range(var, sampling)

  all_opt["opt"][var]["iterate"] = iterate_range(*args.iterate)

###############################################################################
# process iterate data --------------------------------------------------------

def process_iterate_data(iterate, sim_data_meta):

  # populate
  data_iterate = {}

  # prepare grid and function data required for current iterate
  for var in all_opt["var"]:

    # options for current variable
    opt = all_opt["opt"][var]
    sampling = all_opt["opt"][var]["sampling"]

    # setting for current data set
    interpolate = opt["interpolate"]
    interpolate_method = opt["interpolate_method"]
    interpolate_N = opt["interpolate_N"]
    dim_data = opt["dim_data"]

    out, _, dg, _ = sim_data_meta.get_var_info(var, sampling, True)
    Time = sim_data_meta.get_iter_time(out, iterate=iterate)

    # retain some ghosts as this allows better interpolation when required
    strip_dg = 1 # number of ghosts to strip

    xyz_s = sim_data_meta.get_grid(out,
                                   sampling=sampling,
                                   iterate=iterate,
                                   strip_dg=strip_dg)

    # raw data on the sampling
    f_R = sim_data_meta.get_var(
      var,
      sampling,
      iterate=iterate,
      with_ghosts=True,
      strip_dg=strip_dg
    )

    # sample (f)ace centered for grid structure generation
    opt_sampling_f = tuple(smp[:2] + "f" for smp in sampling)

    # grid structure (i.e. (f)ace-centered without ghosts)
    xyz_G = sim_data_meta.get_grid(out,
                                   sampling=opt_sampling_f,
                                   iterate=iterate,
                                   strip_dg=dg)

    xyz_L = sim_data_meta.get_grid_levels(out, iterate=iterate)

    # retain grid spacing extrema
    dx_extrema = [st.extensions.gra.coordinates_get_dx_extrema(gr)
                  for gr in xyz_G]

    # process function samples
    if interpolate:

      # infer range if not provided
      if opt["range"] is None:
        opt["range"] = tuple(
          (ax.min(), ax.max()) for ax in xyz_G
        )

      # perform interpolation as required by range
      xyz_I = narray_from_range(opt["range"], interpolate_N)

      # slice away extraneous entry (VC dump fix)
      xyz_s_ = [xyz_[:,:f_R.shape[xix+1]] for xix, xyz_ in enumerate(xyz_s)]

      if dim_data == 3:
        f_I = st.extensions.gra.interpolate_3d_MeshBlock_field(
          *xyz_s_, f_R,
          *xyz_I, method=interpolate_method
        ).squeeze()
      elif dim_data == 2:
        f_I = st.extensions.gra.interpolate_2d_MeshBlock_field(
          *xyz_s_, f_R,
          *xyz_I, method=interpolate_method
        ).squeeze()
      elif dim_data == 1:
        f_I = st.extensions.gra.interpolate_1d_MeshBlock_field(
          *xyz_s_, f_R,
          *xyz_I, method=interpolate_method
        ).squeeze()

    else:
      # no re-ranging / interp (just slice away ghosts)
      xyz_I = sim_data_meta.get_grid(out,
                                    sampling=sampling,
                                    iterate=iterate,
                                    strip_dg=dg)

      f_I = sim_data_meta.get_var(
        var,
        sampling,
        iterate=iterate,
        with_ghosts=True,
        strip_dg=dg
      )

      if opt["range"] is not None:
        # Remove MeshBlock data not in range (retains ghosts)

        # just need end-points for clip
        xyz_T = narray_from_range(opt["range"], [2,] * len(opt["range"]))

        if dim_data == 3:
          xyz_I, f_I = st.extensions.gra.clip_3d_MeshBlock_field_to_range(
            *xyz_I, f_I, *xyz_T)
        elif dim_data == 2:
          xyz_I, f_I = st.extensions.gra.clip_2d_MeshBlock_field_to_range(
            *xyz_I, f_I, *xyz_T)
        elif dim_data == 1:
          xyz_I, f_I = st.extensions.gra.clip_1d_MeshBlock_field_to_range(
            *xyz_I, f_I, *xyz_T)
        else:
          raise ValueError(f"Cannot process dim_data={dim_data}.")

    # process ranges (for constraining MeshBlock boundaries)
    range_G = [(ax.min(), ax.max()) for ax in xyz_I]
    if opt["range"] is None:
      opt["range"] = range_G

    # slice to allowed levels for plot
    L_min, L_max = all_opt["boundary_levels"]
    xyz_G = [ax[logical_and(L_min<=xyz_L, xyz_L<=L_max)]
            for ax in xyz_G]

    if dim_data == 3:
      X_G = st.extensions.gra.clip_3d_MeshBlock_to_range(*xyz_G, *range_G)
    elif dim_data == 2:
      X_G = st.extensions.gra.clip_2d_MeshBlock_to_range(*xyz_G, *range_G)
    elif dim_data == 1:
      X_G = st.extensions.gra.clip_1d_MeshBlock_to_range(*xyz_G, *range_G)
    else:
      raise ValueError(f"Cannot process dim_data={dim_data}.")

    # reduce to edges defining MeshBlock boundaries
    X_G = [
      X[:, 0::N_B] for X, N_B in zip(X_G, opt["N_B"])
    ]

    # have processed data: (xyz_I, f_I)
    key = (var, sampling)
    data_iterate[key] = {
      "iterate"         : iterate,
      "xyz_I"           : xyz_I,
      "f_I"             : map_transform[all_opt["var_transform"][var]](f_I),
      "xyz_G"           : X_G,
      "xyz_L"           : xyz_L,
      "Time"            : Time,
      "reduced_dim_data": opt["reduced_dim_data"],
      "reduced_axis"    : opt["reduced_axis"],
      "reduced_sampling": opt["reduced_sampling"],
      "dx_extrema"      : dx_extrema,
    }

  return data_iterate

###############################################################################
# process iterate plot --------------------------------------------------------

def get_plot_settings(data_iterate):
  # TODO: shift to JSON?

  # logic delegated based on data dimension
  dim_plot = {item["reduced_dim_data"]
              for key, item in data_iterate.items()}.pop()

  # defaults for plot style ---------------------------------------------------
  plot_MeshBlock_boundaries_kwargs = {
    "color": "gray",
    "linewidth": 0.8,
    "alpha": 0.5,
  }

  subplot_kwargs = {}
  if dim_plot == 3:
    subplot_kwargs["projection"] = "3d"

  subplots_adjust_2d_kwargs = {
    "top"   : 0.967,
    "bottom": 0.056,
    "left"  : 0.072,
    "right" : 0.92,
    "hspace": 0.2,
    "wspace": 0.2
  }

  subplots_adjust_1d_kwargs = {
    "top"   : 0.94,
    "bottom": 0.101,
    "left"  : 0.052,
    "right" : 0.99,
    "hspace": 0.2,
    "wspace": 0.2
  }

  plot_2d_quiver_kwargs = {
    "relative_cutoff": 0.005,
    "factor_downsample": int(next(iter(args.interpolate_N)) // 64),
  }

  quiver2_kwargs = {
    "color"      : "w",
    "width"      : 0.001,
    "minlength"  : 0,
    "alpha"      : 0.5,
    "angles"     : "xy",
    "headwidth"  : 7,
    "scale_units": "width",
    "scale"      : 20,
  }

  legend_kwargs = {
    "loc"       : "upper right",
    "framealpha": 0.2,
    "fontsize"  : "small",
  }

  figsize = (8.5, 8.5) if dim_plot > 1 else (8.5, 4.5)

  plot_settings = {
    "dim_plot": dim_plot,
    "plot_MeshBlock_boundaries_kwargs": plot_MeshBlock_boundaries_kwargs,
    "subplot_kwargs": subplot_kwargs,
    "subplots_adjust_2d_kwargs": subplots_adjust_2d_kwargs,
    "subplots_adjust_1d_kwargs": subplots_adjust_1d_kwargs,
    "plot_2d_quiver_kwargs": plot_2d_quiver_kwargs,
    "quiver2_kwargs": quiver2_kwargs,
    "legend_kwargs": legend_kwargs,
    "figsize": figsize,
  }

  return plot_settings

# functions for adding elements to figures ------------------------------------

def add_fig(plot_settings):
  fig = plt.figure(1, figsize=plot_settings["figsize"])
  gs = fig.add_gridspec(1,1, height_ratios=(1, ), width_ratios=(1, ))
  ax = fig.add_subplot(gs[0, 0], **plot_settings["subplot_kwargs"])
  return ax

def add_MeshBlock_structure_nd(ax, data_iterate=None, plot_settings=None):
  # use first variable to inform MeshBlock structure
  data_key_first = next(iter(data_iterate))

  # extract grid information of non-reduced variables
  xyz_G_NR = sequence_take_by_bool(
    data_iterate[data_key_first]["xyz_G"],
    data_iterate[data_key_first]["reduced_axis"],
    truth_value=False
  )

  xyz_G_NR = tuple(xyz_G_NR)

  if reduce(lambda x, y: x + y, [el.size for el in xyz_G_NR]) > 0:
    # add MeshBlock structure
    st.extensions.gra.plot_nd_MeshBlock_structure(
      ax, *xyz_G_NR,
      plot_kwargs=plot_settings["plot_MeshBlock_boundaries_kwargs"])

def add_pcolormesh_2d(ax, data_iterate=None, plot_settings=None):
  # first variable as pcolormesh
  data_iterate_first = data_iterate[next(iter(data_iterate))]
  xyz_I = data_iterate_first["xyz_I"]
  f_I = data_iterate_first["f_I"]

  # extract independent axes
  xyz_I_NR = sequence_take_by_bool(
    xyz_I, data_iterate_first["reduced_axis"],
    truth_value=False
  )

  plot_scaling = all_opt["plot_scaling"]
  if plot_scaling == "linear":
    f_I_ = f_I
  elif plot_scaling == "log":
    f_I_ = log10(f_I)
  else:
    raise ValueError(f"Option plot_scaling={plot_scaling} of unknown value.")

  if all_opt["plot_range"] is not None:
    vmin, vmax = all_opt["plot_range"][0]
  else:
    vmin, vmax = 0.9 * f_I_.min(), 1.1 * f_I_.max()

  st.extensions.gra.plot_2d_raw(
    ax, *xyz_I_NR, f_I_,
    vmin=vmin,
    vmax=vmax,
    cmap="inferno"
  )

def add_quiver_2d(ax, vars_quiver, data_iterate=None, plot_settings=None):
  # add quiver plot in 2d
  data_quiver = []

  for key, data in data_iterate.items():
    var, sampling = key

    if var in vars_quiver:
      xyz_I = data["xyz_I"]
      f_I = data["f_I"]

      data_quiver.append(f_I)

  if len(data_quiver) != 2:
    raise ValueError(f"Could not find 2 variables for quiver plot")

  # reduce to salient axes (i.e. those that are not fixed)
  data_key_first = next(iter(data_iterate))
  xyz_I_NR = sequence_take_by_bool(
    xyz_I,
    data_iterate[data_key_first]["reduced_axis"],
    truth_value=False
  )

  st.extensions.gra.plot_2d_quiver(
    ax, *xyz_I_NR, *data_quiver,
    **plot_settings["plot_2d_quiver_kwargs"],
    quiver_kwargs=plot_settings["quiver2_kwargs"]
  )

def finalize_plot(ax, dim=3, data_iterate=None, plot_settings=None):
  # - re-range
  # - correct overall fig. geometry

  # use first variable to inform plot range
  data_key_first = next(iter(data_iterate))
  var_first, sampling_first = data_key_first

  range_first_NR = sequence_take_by_bool(
    all_opt["opt"][var_first]["range"],
    data_iterate[data_key_first]["reduced_axis"],
    truth_value=False
  )

  try:
    fcns = (ax.set_xlim, ax.set_ylim, ax.set_zlim)
  except AttributeError:
    fcns = (ax.set_xlim, ax.set_ylim, )

  for ran, fcn in zip(range_first_NR, fcns):
    fcn(*ran)

  if dim == 1:
    if all_opt["plot_range"] is not None:
      ax.set_ylim(*all_opt["plot_range"])

  # add labels
  data_key_first = next(iter(data_iterate))
  Time = data_iterate[data_key_first]["Time"]
  iterate = data_iterate[data_key_first]["iterate"]
  title = f"iterate: {int(iterate):05} T: {Time:.2e}"

  if dim == 2:
    ax.set_xlabel(data_iterate[data_key_first]["reduced_sampling"][0][:-1])
    ax.set_ylabel(data_iterate[data_key_first]["reduced_sampling"][1][:-1])
    title += " var: " + ",".join(all_opt["var"])
  elif dim == 1:
    ax.set_xlabel(data_iterate[data_key_first]["reduced_sampling"][0][:-1])

  ax.set_title(title)

  # prevent plots from "jiggling" their panel geometry
  if dim == 2:
    plt.subplots_adjust(**plot_settings["subplots_adjust_2d_kwargs"])
  elif dim == 1:
    plt.subplots_adjust(**plot_settings["subplots_adjust_1d_kwargs"])

def make_plot_3d(data_iterate=None, plot_settings=None):
  # - have access to data_iterate;
  # - reduced dimension is 3;
  # - plot MeshBlock structure
  # - TODO: other handlers
  ax = add_fig(plot_settings)
  if all_opt["plot_boundaries"]:
    add_MeshBlock_structure_nd(ax,
                               data_iterate=data_iterate,
                               plot_settings=plot_settings)

def make_plot_2d(data_iterate=None, plot_settings=None):
  # - have access to data_iterate;
  # - reduced dimension is 2;
  # - plot first variable
  # - optionally cmd arguments
  # plot_quiver, TODO: plot_streamlines?
  # allow to superpose additional data
  ax = add_fig(plot_settings)

  if all_opt["plot_boundaries"]:
    add_MeshBlock_structure_nd(ax,
                               data_iterate=data_iterate,
                               plot_settings=plot_settings)

  add_pcolormesh_2d(ax,
                    data_iterate=data_iterate,
                    plot_settings=plot_settings)

  if all_opt["plot_quiver"] is not None:
    for vars_quiver in all_opt["plot_quiver"]:
      add_quiver_2d(ax, vars_quiver,
                    data_iterate=data_iterate,
                    plot_settings=plot_settings)

  finalize_plot(ax, dim=2,
                data_iterate=data_iterate,
                plot_settings=plot_settings)

def make_plot_1d(data_iterate=None, plot_settings=None):
  # have access to data_iterate;
  # reduced dimension is 1;
  # make plot of all variables superposed

  ax = add_fig(plot_settings)

  if all_opt["plot_boundaries"]:
    add_MeshBlock_structure_nd(ax,
                               data_iterate=data_iterate,
                               plot_settings=plot_settings)

  # cycle over variables
  for key, var_data in data_iterate.items():
    var, sampling = key

    # extract independent axis
    s = sequence_take_by_bool(
      var_data["xyz_I"], var_data["reduced_axis"],
      truth_value=False
    )

    var_transform = all_opt["var_transform"][var]
    if var_transform == "id":
      label = f"{var}"
    else:
      label = f"{var_transform} o [{var}]"


    # in 1d so only one element; flatten for plots
    s_ = next(s).flatten()
    f_ = var_data["f_I"].flatten()

    plot_scaling = all_opt["plot_scaling"]
    if plot_scaling == "linear":
      ax.plot(
        s_, f_,
        label=label
      )
    elif plot_scaling == "semilogy":
      ax.semilogy(
        s_, f_,
        label=label
      )
    elif plot_scaling == "loglog":
      ax.loglog(
        s_, f_,
        label=label
      )
    else:
      raise ValueError(f"Option plot_scaling={plot_scaling} of unknown value.")

  # labels
  ax.legend(**plot_settings["legend_kwargs"])

  finalize_plot(ax, dim=1,
                data_iterate=data_iterate,
                plot_settings=plot_settings)

def generate_filename(iterate=None):
  # generate filename stem (without extension or full path)

  var_first = next(iter(all_opt["opt"]))

  lbl_var = ",".join(all_opt["opt"].keys())

  if iterate is not None:
    lbl_iterate = f"{int(iterate):05}"

  lbl_plot_scaling = all_opt["plot_scaling"]
  lbl_reduced_slicing = "_".join(
    (el[:2] for el in all_opt["opt"][var_first]["reduced_sampling"])
  )

  lbl_range = []
  for el in all_opt["opt"][var_first]["range"]:
    if isinstance(el, tuple):
      lbl_range.append(",".join(f"{el_:.2e}" for el_ in el))
    else:
      lbl_range.append(f"{el:.2e}")

  lbl_range = "_".join(lbl_range)

  if iterate is not None:
    fn_fig = (
      f"{lbl_var}_" +
      f"{lbl_plot_scaling}_{lbl_range}_{lbl_reduced_slicing}_{lbl_iterate}"
    )
  else:
    fn_fig = (
      f"{lbl_var}_" +
      f"{lbl_plot_scaling}_{lbl_range}_{lbl_reduced_slicing}"
    )

  return fn_fig

# loop over iterates ----------------------------------------------------------

# main logic (such that it can be parallelised)
def process_iterate(iterate, sim_data_meta):
  data_iterate = process_iterate_data(iterate, sim_data_meta)

  if not all_opt["no_plot"]:
    plot_settings = get_plot_settings(data_iterate)

    if plot_settings["dim_plot"] == 3:
      make_plot_3d(data_iterate, plot_settings)
    elif plot_settings["dim_plot"] == 2:
      make_plot_2d(data_iterate, plot_settings)
    elif plot_settings["dim_plot"] == 1:
      make_plot_1d(data_iterate, plot_settings)
    else:
      raise ValueError("Unknown dim_plot.")

    fn_fig = generate_filename(iterate) + ".png"
    fn_full_fig = st.core.io.path_to_absolute([all_opt["dir_out"], fn_fig],
                                              verify=False)

    if all_opt["plot_save"]:
      plt.savefig(fn_full_fig)

    if all_opt["plot_show"]:
      plt.show()

    plt.close()

    return iterate, data_iterate, fn_full_fig

  return iterate, data_iterate, None

# get iterates and switch serial / parallel_pool ------------------------------
var_first = next(iter(all_opt["opt"]))
var_iterate = all_opt["opt"][var_first]["iterate"]

# retain settings
sim_data["all_opt"] = all_opt

# ensure output directory exists
st.core.io.dir_generate(all_opt["dir_out"], signal=False)

if all_opt["parallel_pool"] is not None:
  # spin up a parallel pool
  pool = st.core.parallel_pool_start(all_opt["parallel_pool"])
  par_res = [
    st.core.parallel_pool_apply_async(
      pool,
      process_iterate,
      (iterate, sim_data_meta),
      method="dill")
    for iterate in var_iterate
  ]

  # process work
  fns_figs = []
  for ix, pr in enumerate(par_res):
    iterate, data_iterate, fn_full_fig = pr.get()
    fns_figs.append(fn_full_fig)

    if all_opt["data_save"]:
      sim_data[iterate] = data_iterate

    print(f"Processed: {ix}")

else:
  # serial execution
  sim_data["all_opt"] = all_opt

  for ix, iterate in enumerate(var_iterate):
    iterate, data_iterate, fn_full_fig = process_iterate(iterate,
                                                         sim_data_meta)

    if all_opt["data_save"]:
      sim_data[iterate] = data_iterate

    print(f"Processed: {ix}")

# final (serial) tasks --------------------------------------------------------
fn_stem = generate_filename(None)
fn_fig_ext = "png"

# dump data as pickle if required
if all_opt["data_save"]:
  fn_full_obj = st.core.io.path_to_absolute(
    [all_opt["dir_out"], fn_stem + ".obj"],
    verify=False)

  st.core.io.pickle_dump(sim_data, fn_full_obj, compression="lzma")

# make movie if required
if all_opt["make_movie"]:
  print(f"Making movie... @{all_opt['movie_fps']} fps")

  path_run = st.core.io.path_cwd_get()
  st.core.io.path_cwd_set(all_opt['dir_out'])

  # Take first filename, strip iter, use as pattern
  # TODO: N.B. this does not respect "step" in --iterate=min,max,step

  # 5 digit iterations for athdf
  fn_fig_pattern = "*".join([fn_stem, "." + fn_fig_ext])

  fn_movie = fn_stem + "_movie"

  movie_fps = all_opt['movie_fps']

  # make mp4
  result = st.core.shell.shell_interact(
    ["ffmpeg",
     "-y", # auto overwrite
     "-framerate", str(movie_fps),
     "-pattern_type", "glob", "-i", f'"{fn_fig_pattern}"',
     "-c:v", "libx264", "-pix_fmt", "yuv420p",
     fn_movie + ".mp4"],
    method='os_system',
    capture_output=True
  )

  # make gif
  result = st.core.shell.shell_interact(
    ["ffmpeg",
     "-y", # auto overwrite
     "-ss", "0", "-t", "0",
     "-i", fn_movie + ".mp4",
     "-filter_complex", # need fifo so that frames are not dropped
     f'"[0:v] fps={movie_fps},split [a][b];[b]fifo[bb];[a] ' +
     f'palettegen [p];[bb][p] paletteuse"',
     fn_movie + ".gif"],
    method='os_system',
    capture_output=True
  )

  st.core.io.path_cwd_set(path_run)

#
# :D
#
