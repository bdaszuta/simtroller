"""
 ,-*
(_)

@author: Boris Daszuta
@function: `simtroller` library: A tool for processing / managing simulations
and inspection of data generated.
"""
import os as _os

from simtroller import core
from simtroller import extensions

# from simtroller.core.io import *
# from simtroller.core.primitives import *
# from simtroller.core.shell import *

# is convenient to have the package directory accessible
_package_core_path = core.io.path_strip(
  _os.path.realpath(core.__file__),
  mode="filename"
)
package_path = core.io.path_strip(
  _package_core_path,
  mode="filename"
)

#
# :D
#
