"""
 ,-*
(_)

@author: Boris Daszuta
@function: Collect extensions of `simtroller` tailored to specific codes here.
"""
from simtroller.extensions import bam
from simtroller.extensions import gra
from simtroller.extensions import waveforms
from simtroller.extensions.plotting import (plot_set_defaults,
                                            plot_colors)


# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("plotting"))
# =============================================================================

#
# :D
#
