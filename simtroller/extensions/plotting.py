#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: General plotting.
"""
###############################################################################
# python imports
from matplotlib.pyplot import (rc, )
###############################################################################

# font defaults -------------------------------------------
_SMALL_SIZE = 8
_MEDIUM_SIZE = 10
_BIGGER_SIZE = 12
_LINE_WIDTH = 0.9

# colors --------------------------------------------------
# see: https://matplotlib.org/stable/gallery/color/named_colors.html
plot_colors = [
  'red', 'blue', 'green',
  'cyan', 'lime', 'darkred',
  'darkolivegreen', 'dodgerblue', 'pink',
  'indigo', 'orange', 'darkgreen',
  'magenta', 'darksalmon', 'saddlebrown'
]


def plot_set_defaults() -> None:
  """
  Enforce opinionated defaults on plots.
  """
  # TODO: LaTeX etc by passed flag

  rc('font',   size=_SMALL_SIZE)          # controls default text sizes
  rc('axes',   titlesize=_SMALL_SIZE)     # fontsize of the axes title
  rc('axes',   labelsize=_SMALL_SIZE)     # fontsize of the x and y labels
  rc('xtick',  labelsize=_SMALL_SIZE)     # fontsize of the tick labels
  rc('ytick',  labelsize=_SMALL_SIZE)     # fontsize of the tick labels
  rc('legend', fontsize=_SMALL_SIZE)      # legend fontsize
  rc('figure', titlesize=_BIGGER_SIZE)    # fontsize of the figure title
  rc('lines',  linewidth=_LINE_WIDTH)     # default widths of lines

#
# :D
#
