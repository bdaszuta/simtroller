#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Various interpolation functions for GR-Athena++.
"""
###############################################################################
# python imports
from typing import TYPE_CHECKING, Optional
from numpy import ndarray

# pylint: disable=redefined-builtin
from numpy import (zeros, zeros_like)

# package imports
from simtroller.core.numba import JITI
from simtroller.core.numerical import (grid_1d_interpolate,
                                       grid_2d_interpolate,
                                       grid_3d_interpolate,
                                       grid_1d_interpolate_scipy,
                                       grid_2d_interpolate_scipy,
                                       grid_3d_interpolate_scipy,
                                       ndarray_get_sorted_argmin,
                                       interval_is_intersection_empty)
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

# deprecated
def _interpolate_1d_MeshBlock_field(x           : ndarray,
                                   F           : ndarray,
                                   x_I         : ndarray,
                                   d           : int=4,
                                   F_I         : Optional[ndarray]=None,
                                   F_I_scratch : Optional[ndarray]=None,
                                   method      : str="FH") -> ndarray:
  """
  Given a one dimensional field over a MeshBlock perform interpolation.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  F : ndarray
    Sampled function

  x_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  F_I_scratch : Optional[ndarray]=None
    Optionally pass pre-allocated array.

  method : str="FH"
    Method to use. Must be element of {"BT", "FH"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses simtroller.core.numerical.grid_1d_interpolate
  """
  multiplicity = zeros_like(x_I)

  if F_I_scratch is None:
    F_I_scratch = zeros_like(x_I, dtype=F.dtype)
  if F_I is None:
    F_I = zeros_like(x_I, dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]

    # check current block relevant
    if not interval_is_intersection_empty(x_B[0], x_B[-1], x_I[0], x_I[-1]):
      # get result indical range
      ix_gsa_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_a]:
        ix_gsa_a = ix_gsa_a + 1

      if x_B[-1] >= x_I[ix_gsa_b]:
        ix_gsa_b = min(ix_gsa_b + 1, x_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_a, ix_gsa_b)
      multiplicity[sl_x] += 1

      grid_1d_interpolate(
                x_B, F[ix_MB],
                x_I[sl_x],
                d=d,
                F_I=F_I_scratch[sl_x],
                method=method)

      F_I[sl_x] += F_I_scratch[sl_x]

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

# use jit
def _interpolate_1d_MeshBlock_field_direct(
  x           : ndarray,
  F           : ndarray,
  x_I         : ndarray,
  d           : int=4,
  F_I         : Optional[ndarray]=None,
  F_I_scratch : Optional[ndarray]=None,
  method      : str="FH"
) -> ndarray:
  multiplicity = zeros_like(x_I)

  if F_I_scratch is None:
    F_I_scratch = zeros_like(x_I, dtype=F.dtype)
  if F_I is None:
    F_I = zeros_like(x_I, dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]

    # check current block relevant
    if not interval_is_intersection_empty(x_B[0], x_B[-1], x_I[0], x_I[-1]):
      # get result indical range
      ix_gsa_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_a]:
        ix_gsa_a = ix_gsa_a + 1

      if x_B[-1] >= x_I[ix_gsa_b]:
        ix_gsa_b = min(ix_gsa_b + 1, x_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_a, ix_gsa_b)
      multiplicity[sl_x] += 1

      grid_1d_interpolate(
                x_B, F[ix_MB],
                x_I[sl_x],
                d=d,
                F_I=F_I_scratch[sl_x],
                method=method)

      F_I[sl_x] += F_I_scratch[sl_x]

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

# no jit
def _interpolate_1d_MeshBlock_field_scipy(
  x           : ndarray,
  F           : ndarray,
  x_I         : ndarray,
  F_I         : Optional[ndarray]=None,
  method      : str="linear"
) -> ndarray:
  multiplicity = zeros_like(x_I)

  if F_I is None:
    F_I = zeros_like(x_I, dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]

    # check current block relevant
    if not interval_is_intersection_empty(x_B[0], x_B[-1], x_I[0], x_I[-1]):
      # get result indical range
      ix_gsa_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_a]:
        ix_gsa_a = ix_gsa_a + 1

      if x_B[-1] >= x_I[ix_gsa_b]:
        ix_gsa_b = min(ix_gsa_b + 1, x_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_a, ix_gsa_b)
      multiplicity[sl_x] += 1

      F_I[sl_x] += grid_1d_interpolate_scipy(
                x_B, F[ix_MB],
                x_I[sl_x],
                method=method)

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I
def interpolate_1d_MeshBlock_field(x           : ndarray,
                                   F           : ndarray,
                                   x_I         : ndarray,
                                   d           : int=4,
                                   F_I         : Optional[ndarray]=None,
                                   F_I_scratch : Optional[ndarray]=None,
                                   method      : str="linear") -> ndarray:
  """
  Given a one dimensional field over a MeshBlock perform interpolation.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  F : ndarray
    Sampled function

  x_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  F_I_scratch : Optional[ndarray]=None
    Optionally pass pre-allocated array.

  method : str="linear"
    Method to use. Must be element of {"BT", "FH", "linear", "cubic"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses:
  - simtroller.core.numerical.grid_1d_interpolate
  - simtroller.core.numerical.grid_1d_interpolate_scipy
  """
  if method in {"linear", "cubic"}:
    return _interpolate_1d_MeshBlock_field_scipy(
      x,
      F,
      x_I,
      F_I=F_I,
      method=method
    )
  else:
    return _interpolate_1d_MeshBlock_field_direct(
      x,
      F,
      x_I,
      d=d,
      F_I=F_I,
      F_I_scratch=F_I_scratch,
      method=method
    )

# deprecated
def _interpolate_2d_MeshBlock_field(x           : ndarray,
                                   y           : ndarray,
                                   F           : ndarray,
                                   x_I         : ndarray,
                                   y_I         : ndarray,
                                   d           : int=4,
                                   F_I         : Optional[ndarray]=None,
                                   F_I_scratch : Optional[ndarray]=None,
                                   method      : str="FH") -> ndarray:
  """
  Given a two dimensional field over a MeshBlock perform interpolation.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  F : ndarray
    Sampled function

  x_I : ndarray
    Grid to interpolate to.

  y_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  F_I_scratch : Optional[ndarray]=None
    Optionally pass pre-allocated array.

  method : str="FH"
    Method to use. Must be element of {"BT", "FH"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses simtroller.core.numerical.grid_2d_interpolate
  """
  multiplicity = zeros((x_I.size, y_I.size))

  if F_I_scratch is None:
    F_I_scratch = zeros((x_I.size, y_I.size), dtype=F.dtype)
  if F_I is None:
    F_I = zeros((x_I.size, y_I.size), dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_I[0], x_I[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_I[0], y_I[-1]))):

      # get result indical range
      ix_gsa_x_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_x_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_x_a]:
        ix_gsa_x_a = ix_gsa_x_a + 1

      if x_B[-1] >= x_I[ix_gsa_x_b]:
        ix_gsa_x_b = min(ix_gsa_x_b + 1, x_I.size)

      # get result indical range
      ix_gsa_y_a = ndarray_get_sorted_argmin(y_B[0],  y_I)
      ix_gsa_y_b = ndarray_get_sorted_argmin(y_B[-1], y_I)

      # ensure block windows maximal extent of target interpolation
      if y_B[0] > y_I[ix_gsa_y_a]:
        ix_gsa_y_a = ix_gsa_y_a + 1

      if y_B[-1] >= y_I[ix_gsa_y_b]:
        ix_gsa_y_b = min(ix_gsa_y_b + 1, y_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_x_a, ix_gsa_x_b)
      sl_y = slice(ix_gsa_y_a, ix_gsa_y_b)

      multiplicity[sl_x, sl_y] += 1

      grid_2d_interpolate(
        x_B, y_B, F[ix_MB],
        x_I[sl_x],
        y_I[sl_y], d=d, method=method,
        F_I=F_I_scratch[sl_x, sl_y])

      F_I[sl_x, sl_y] += F_I_scratch[sl_x, sl_y]

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

# use jit
def _interpolate_2d_MeshBlock_field_direct(
  x           : ndarray,
  y           : ndarray,
  F           : ndarray,
  x_I         : ndarray,
  y_I         : ndarray,
  d           : int=4,
  F_I         : Optional[ndarray]=None,
  F_I_scratch : Optional[ndarray]=None,
  method      : str="FH"
) -> ndarray:
  multiplicity = zeros((x_I.size, y_I.size))

  if F_I_scratch is None:
    F_I_scratch = zeros((x_I.size, y_I.size), dtype=F.dtype)
  if F_I is None:
    F_I = zeros((x_I.size, y_I.size), dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_I[0], x_I[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_I[0], y_I[-1]))):

      # get result indical range
      ix_gsa_x_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_x_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_x_a]:
        ix_gsa_x_a = ix_gsa_x_a + 1

      if x_B[-1] >= x_I[ix_gsa_x_b]:
        ix_gsa_x_b = min(ix_gsa_x_b + 1, x_I.size)

      # get result indical range
      ix_gsa_y_a = ndarray_get_sorted_argmin(y_B[0],  y_I)
      ix_gsa_y_b = ndarray_get_sorted_argmin(y_B[-1], y_I)

      # ensure block windows maximal extent of target interpolation
      if y_B[0] > y_I[ix_gsa_y_a]:
        ix_gsa_y_a = ix_gsa_y_a + 1

      if y_B[-1] >= y_I[ix_gsa_y_b]:
        ix_gsa_y_b = min(ix_gsa_y_b + 1, y_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_x_a, ix_gsa_x_b)
      sl_y = slice(ix_gsa_y_a, ix_gsa_y_b)

      multiplicity[sl_x, sl_y] += 1

      grid_2d_interpolate(
        x_B, y_B, F[ix_MB],
        x_I[sl_x],
        y_I[sl_y], d=d, method=method,
        F_I=F_I_scratch[sl_x, sl_y])

      F_I[sl_x, sl_y] += F_I_scratch[sl_x, sl_y]

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

# no jit
def _interpolate_2d_MeshBlock_field_scipy(
  x           : ndarray,
  y           : ndarray,
  F           : ndarray,
  x_I         : ndarray,
  y_I         : ndarray,
  F_I         : Optional[ndarray]=None,
  method      : str="linear"
) -> ndarray:
  multiplicity = zeros((x_I.size, y_I.size))

  if F_I is None:
    F_I = zeros((x_I.size, y_I.size), dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_I[0], x_I[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_I[0], y_I[-1]))):

      # get result indical range
      ix_gsa_x_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_x_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_x_a]:
        ix_gsa_x_a = ix_gsa_x_a + 1

      if x_B[-1] >= x_I[ix_gsa_x_b]:
        ix_gsa_x_b = min(ix_gsa_x_b + 1, x_I.size)

      # get result indical range
      ix_gsa_y_a = ndarray_get_sorted_argmin(y_B[0],  y_I)
      ix_gsa_y_b = ndarray_get_sorted_argmin(y_B[-1], y_I)

      # ensure block windows maximal extent of target interpolation
      if y_B[0] > y_I[ix_gsa_y_a]:
        ix_gsa_y_a = ix_gsa_y_a + 1

      if y_B[-1] >= y_I[ix_gsa_y_b]:
        ix_gsa_y_b = min(ix_gsa_y_b + 1, y_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_x_a, ix_gsa_x_b)
      sl_y = slice(ix_gsa_y_a, ix_gsa_y_b)

      multiplicity[sl_x, sl_y] += 1

      F_I[sl_x, sl_y] += grid_2d_interpolate_scipy(
        x_B, y_B, F[ix_MB],
        x_I[sl_x],
        y_I[sl_y], method=method)

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

def interpolate_2d_MeshBlock_field(x           : ndarray,
                                   y           : ndarray,
                                   F           : ndarray,
                                   x_I         : ndarray,
                                   y_I         : ndarray,
                                   d           : int=4,
                                   F_I         : Optional[ndarray]=None,
                                   F_I_scratch : Optional[ndarray]=None,
                                   method      : str="linear") -> ndarray:
  """
  Given a two dimensional field over a MeshBlock perform interpolation.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  F : ndarray
    Sampled function

  x_I : ndarray
    Grid to interpolate to.

  y_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  F_I_scratch : Optional[ndarray]=None
    Optionally pass pre-allocated array.

  method : str="linear"
    Method to use. Must be element of {"BT", "FH", "linear", "nearest"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses:
  - simtroller.core.numerical.grid_2d_interpolate
  - simtroller.core.numerical.grid_2d_interpolate_scipy
  """
  if method in {"linear", "nearest"}:
    return _interpolate_2d_MeshBlock_field_scipy(
      x, y,
      F,
      x_I, y_I,
      F_I=F_I,
      method=method
    )
  else:
    return _interpolate_2d_MeshBlock_field_direct(
      x, y,
      F,
      x_I, y_I,
      d=d,
      F_I=F_I,
      F_I_scratch=F_I_scratch,
      method=method
    )

# deprecated
def _interpolate_3d_MeshBlock_field(x           : ndarray,
                                   y           : ndarray,
                                   z           : ndarray,
                                   F           : ndarray,
                                   x_I         : ndarray,
                                   y_I         : ndarray,
                                   z_I         : ndarray,
                                   d           : int=4,
                                   F_I         : Optional[ndarray]=None,
                                   F_I_scratch : Optional[ndarray]=None,
                                   method      : str="FH") -> ndarray:
  """
  Given a three dimensional field over a MeshBlock perform interpolation.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  z : ndarray
    Grid over which function is sampled.

  F : ndarray
    Sampled function

  x_I : ndarray
    Grid to interpolate to.

  y_I : ndarray
    Grid to interpolate to.

  z_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  F_I_scratch : Optional[ndarray]=None
    Optionally pass pre-allocated array.

  method : str="FH"
    Method to use. Must be element of {"BT", "FH"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses simtroller.core.numerical.grid_3d_interpolate
  """
  multiplicity = zeros((x_I.size, y_I.size, z_I.size))

  if F_I_scratch is None:
    F_I_scratch = zeros((x_I.size, y_I.size, z_I.size), dtype=F.dtype)
  if F_I is None:
    F_I = zeros((x_I.size, y_I.size, z_I.size), dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]
    z_B = z[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_I[0], x_I[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_I[0], y_I[-1])) and
        (not interval_is_intersection_empty(z_B[0], z_B[-1],
                                            z_I[0], z_I[-1]))):

      # get result indical range
      ix_gsa_x_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_x_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_x_a]:
        ix_gsa_x_a = ix_gsa_x_a + 1

      if x_B[-1] >= x_I[ix_gsa_x_b]:
        ix_gsa_x_b = min(ix_gsa_x_b + 1, x_I.size)

      # get result indical range
      ix_gsa_y_a = ndarray_get_sorted_argmin(y_B[0],  y_I)
      ix_gsa_y_b = ndarray_get_sorted_argmin(y_B[-1], y_I)

      # ensure block windows maximal extent of target interpolation
      if y_B[0] > y_I[ix_gsa_y_a]:
        ix_gsa_y_a = ix_gsa_y_a + 1

      if y_B[-1] >= y_I[ix_gsa_y_b]:
        ix_gsa_y_b = min(ix_gsa_y_b + 1, y_I.size)

      # get result indical range
      ix_gsa_z_a = ndarray_get_sorted_argmin(z_B[0],  z_I)
      ix_gsa_z_b = ndarray_get_sorted_argmin(z_B[-1], z_I)

      # ensure block windows maximal extent of target interpolation
      if z_B[0] > z_I[ix_gsa_z_a]:
        ix_gsa_z_a = ix_gsa_z_a + 1

      if z_B[-1] >= z_I[ix_gsa_z_b]:
        ix_gsa_z_b = min(ix_gsa_z_b + 1, z_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_x_a, ix_gsa_x_b)
      sl_y = slice(ix_gsa_y_a, ix_gsa_y_b)
      sl_z = slice(ix_gsa_z_a, ix_gsa_z_b)

      multiplicity[sl_x, sl_y, sl_z] += 1

      grid_3d_interpolate(
        x_B, y_B, z_B, F[ix_MB],
        x_I[sl_x],
        y_I[sl_y],
        z_I[sl_z], d=d, method=method,
        F_I=F_I_scratch[sl_x, sl_y, sl_z])

      F_I[sl_x, sl_y, sl_z] += F_I_scratch[sl_x, sl_y, sl_z]

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I


# use jit
def _interpolate_3d_MeshBlock_field_direct(
  x           : ndarray,
  y           : ndarray,
  z           : ndarray,
  F           : ndarray,
  x_I         : ndarray,
  y_I         : ndarray,
  z_I         : ndarray,
  d           : int=4,
  F_I         : Optional[ndarray]=None,
  F_I_scratch : Optional[ndarray]=None,
  method      : str="FH"
) -> ndarray:
  multiplicity = zeros((x_I.size, y_I.size, z_I.size))

  if F_I_scratch is None:
    F_I_scratch = zeros((x_I.size, y_I.size, z_I.size), dtype=F.dtype)
  if F_I is None:
    F_I = zeros((x_I.size, y_I.size, z_I.size), dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]
    z_B = z[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_I[0], x_I[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_I[0], y_I[-1])) and
        (not interval_is_intersection_empty(z_B[0], z_B[-1],
                                            z_I[0], z_I[-1]))):

      # get result indical range
      ix_gsa_x_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_x_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_x_a]:
        ix_gsa_x_a = ix_gsa_x_a + 1

      if x_B[-1] >= x_I[ix_gsa_x_b]:
        ix_gsa_x_b = min(ix_gsa_x_b + 1, x_I.size)

      # get result indical range
      ix_gsa_y_a = ndarray_get_sorted_argmin(y_B[0],  y_I)
      ix_gsa_y_b = ndarray_get_sorted_argmin(y_B[-1], y_I)

      # ensure block windows maximal extent of target interpolation
      if y_B[0] > y_I[ix_gsa_y_a]:
        ix_gsa_y_a = ix_gsa_y_a + 1

      if y_B[-1] >= y_I[ix_gsa_y_b]:
        ix_gsa_y_b = min(ix_gsa_y_b + 1, y_I.size)

      # get result indical range
      ix_gsa_z_a = ndarray_get_sorted_argmin(z_B[0],  z_I)
      ix_gsa_z_b = ndarray_get_sorted_argmin(z_B[-1], z_I)

      # ensure block windows maximal extent of target interpolation
      if z_B[0] > z_I[ix_gsa_z_a]:
        ix_gsa_z_a = ix_gsa_z_a + 1

      if z_B[-1] >= z_I[ix_gsa_z_b]:
        ix_gsa_z_b = min(ix_gsa_z_b + 1, z_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_x_a, ix_gsa_x_b)
      sl_y = slice(ix_gsa_y_a, ix_gsa_y_b)
      sl_z = slice(ix_gsa_z_a, ix_gsa_z_b)

      multiplicity[sl_x, sl_y, sl_z] += 1

      grid_3d_interpolate(
        x_B, y_B, z_B, F[ix_MB],
        x_I[sl_x],
        y_I[sl_y],
        z_I[sl_z], d=d, method=method,
        F_I=F_I_scratch[sl_x, sl_y, sl_z])

      F_I[sl_x, sl_y, sl_z] += F_I_scratch[sl_x, sl_y, sl_z]

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

# no jit
def _interpolate_3d_MeshBlock_field_scipy(
  x           : ndarray,
  y           : ndarray,
  z           : ndarray,
  F           : ndarray,
  x_I         : ndarray,
  y_I         : ndarray,
  z_I         : ndarray,
  F_I         : Optional[ndarray]=None,
  method      : str="linear"):

  multiplicity = zeros((x_I.size, y_I.size, z_I.size))

  if F_I is None:
    F_I = zeros((x_I.size, y_I.size, z_I.size), dtype=F.dtype)

  # iterate over blocks
  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]
    z_B = z[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_I[0], x_I[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_I[0], y_I[-1])) and
        (not interval_is_intersection_empty(z_B[0], z_B[-1],
                                            z_I[0], z_I[-1]))):

      # get result indical range
      ix_gsa_x_a = ndarray_get_sorted_argmin(x_B[0],  x_I)
      ix_gsa_x_b = ndarray_get_sorted_argmin(x_B[-1], x_I)

      # ensure block windows maximal extent of target interpolation
      if x_B[0] > x_I[ix_gsa_x_a]:
        ix_gsa_x_a = ix_gsa_x_a + 1

      if x_B[-1] >= x_I[ix_gsa_x_b]:
        ix_gsa_x_b = min(ix_gsa_x_b + 1, x_I.size)

      # get result indical range
      ix_gsa_y_a = ndarray_get_sorted_argmin(y_B[0],  y_I)
      ix_gsa_y_b = ndarray_get_sorted_argmin(y_B[-1], y_I)

      # ensure block windows maximal extent of target interpolation
      if y_B[0] > y_I[ix_gsa_y_a]:
        ix_gsa_y_a = ix_gsa_y_a + 1

      if y_B[-1] >= y_I[ix_gsa_y_b]:
        ix_gsa_y_b = min(ix_gsa_y_b + 1, y_I.size)

      # get result indical range
      ix_gsa_z_a = ndarray_get_sorted_argmin(z_B[0],  z_I)
      ix_gsa_z_b = ndarray_get_sorted_argmin(z_B[-1], z_I)

      # ensure block windows maximal extent of target interpolation
      if z_B[0] > z_I[ix_gsa_z_a]:
        ix_gsa_z_a = ix_gsa_z_a + 1

      if z_B[-1] >= z_I[ix_gsa_z_b]:
        ix_gsa_z_b = min(ix_gsa_z_b + 1, z_I.size)

      # slice relevant ranges
      sl_x = slice(ix_gsa_x_a, ix_gsa_x_b)
      sl_y = slice(ix_gsa_y_a, ix_gsa_y_b)
      sl_z = slice(ix_gsa_z_a, ix_gsa_z_b)

      multiplicity[sl_x, sl_y, sl_z] += 1

      F_I[sl_x, sl_y, sl_z] += grid_3d_interpolate_scipy(
        x_B, y_B, z_B, F[ix_MB],
        x_I[sl_x],
        y_I[sl_y],
        z_I[sl_z], method=method)

  # deal with any multiplicity conditions
  F_I[:] /= multiplicity[:]

  return F_I

# interface (switches between jit capable & not)
def interpolate_3d_MeshBlock_field(x           : ndarray,
                                   y           : ndarray,
                                   z           : ndarray,
                                   F           : ndarray,
                                   x_I         : ndarray,
                                   y_I         : ndarray,
                                   z_I         : ndarray,
                                   d           : int=4,
                                   F_I         : Optional[ndarray]=None,
                                   F_I_scratch : Optional[ndarray]=None,
                                   method      : str="linear") -> ndarray:
  """
  Given a three dimensional field over a MeshBlock perform interpolation.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  z : ndarray
    Grid over which function is sampled.

  F : ndarray
    Sampled function

  x_I : ndarray
    Grid to interpolate to.

  y_I : ndarray
    Grid to interpolate to.

  z_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  F_I_scratch : Optional[ndarray]=None
    Optionally pass pre-allocated array.

  method : str="linear"
    Method to use. Must be element of {"BT", "FH", "linear", "nearest"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses:
  - simtroller.core.numerical.grid_3d_interpolate
  - simtroller.core.numerical.grid_3d_interpolate_scipy
  """
  if method in {"linear", "nearest"}:
    return _interpolate_3d_MeshBlock_field_scipy(
      x, y, z,
      F,
      x_I, y_I, z_I,
      F_I=F_I,
      method=method
    )
  else:
    return _interpolate_3d_MeshBlock_field_direct(
      x, y, z,
      F,
      x_I, y_I, z_I,
      d=d,
      F_I=F_I,
      F_I_scratch=F_I_scratch,
      method=method
    )

def clip_1d_MeshBlock_field_to_range(x   : ndarray,
                                     fcn : ndarray,
                                     x_R : ndarray) -> tuple[tuple[ndarray,],
                                                             ndarray]:
  """
  Discard one dimensional MeshBlock data outside provided range.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  fcn : ndarray
    Sampled function

  x_R : ndarray
    Range to restrict by.

  Returns
  -------
  Restricted ((x, ), fcn)
  """
  x_ = zeros_like(x)
  fcn_ = zeros_like(fcn)

  ix = 0

  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_R[0], x_R[-1]))):
      x_[ix, ...] = x_B
      fcn_[ix, ...] = fcn[ix_MB]

      ix += 1

  return (x_[:ix, ...], ), fcn_[:ix, ...]

def clip_2d_MeshBlock_field_to_range(x   : ndarray,
                                     y   : ndarray,
                                     fcn : ndarray,
                                     x_R : ndarray,
                                     y_R : ndarray) -> tuple[tuple[ndarray,
                                                                   ndarray],
                                                             ndarray]:
  """
  Discard two dimensional MeshBlock data outside provided range.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  fcn : ndarray
    Sampled function

  x_R : ndarray
    Range to restrict by.

  y_R : ndarray
    Range to restrict by.

  Returns
  -------
  Restricted ((x, y), fcn)
  """
  x_ = zeros_like(x)
  y_ = zeros_like(y)
  fcn_ = zeros_like(fcn)

  ix = 0

  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_R[0], x_R[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_R[0], y_R[-1]))):
      x_[ix, ...] = x_B
      y_[ix, ...] = y_B
      fcn_[ix, ...] = fcn[ix_MB]

      ix += 1

  return (x_[:ix, ...], y_[:ix, ...]), fcn_[:ix, ...]

def clip_3d_MeshBlock_field_to_range(x   : ndarray,
                                     y   : ndarray,
                                     z   : ndarray,
                                     fcn : ndarray,
                                     x_R : ndarray,
                                     y_R : ndarray,
                                     z_R : ndarray) -> tuple[tuple[ndarray,
                                                                   ndarray,
                                                                   ndarray],
                                                             ndarray]:
  """
  Discard three dimensional MeshBlock data outside provided range.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  z : ndarray
    Grid over which function is sampled.

  fcn : ndarray
    Sampled function

  x_R : ndarray
    Range to restrict by.

  y_R : ndarray
    Range to restrict by.

  z_R : ndarray
    Range to restrict by.

  Returns
  -------
  Restricted ((x, y, z), fcn)
  """
  x_ = zeros_like(x)
  y_ = zeros_like(y)
  z_ = zeros_like(z)
  fcn_ = zeros_like(fcn)

  ix = 0

  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]
    z_B = z[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_R[0], x_R[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_R[0], y_R[-1])) and
        (not interval_is_intersection_empty(z_B[0], z_B[-1],
                                            z_R[0], z_R[-1]))):
      x_[ix, ...] = x_B
      y_[ix, ...] = y_B
      z_[ix, ...] = z_B
      fcn_[ix, ...] = fcn[ix_MB]

      ix += 1

  return (x_[:ix, ...], y_[:ix, ...], z_[:ix, ...]), fcn_[:ix, ...]

def clip_1d_MeshBlock_to_range(x   : ndarray,
                               x_R : ndarray) -> tuple[ndarray,]:
  """
  Discard one dimensional MeshBlock data outside provided range.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  x_R : ndarray
    Range to restrict by.

  Returns
  -------
  Restricted (x, )
  """
  x_ = zeros_like(x)

  ix = 0

  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_R[0], x_R[-1]))):
      x_[ix, ...] = x_B

      ix += 1

  return (x_[:ix, ...], )

def clip_2d_MeshBlock_to_range(x   : ndarray,
                               y   : ndarray,
                               x_R : ndarray,
                               y_R : ndarray) -> tuple[ndarray, ndarray]:
  """
  Discard two dimensional MeshBlock data outside provided range.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  x_R : ndarray
    Range to restrict by.

  y_R : ndarray
    Range to restrict by.

  Returns
  -------
  Restricted (x, y)
  """
  x_ = zeros_like(x)
  y_ = zeros_like(y)

  ix = 0

  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_R[0], x_R[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_R[0], y_R[-1]))):
      x_[ix, ...] = x_B
      y_[ix, ...] = y_B

      ix += 1

  return (x_[:ix, ...], y_[:ix, ...])

def clip_3d_MeshBlock_to_range(x   : ndarray,
                               y   : ndarray,
                               z   : ndarray,
                               x_R : ndarray,
                               y_R : ndarray,
                               z_R : ndarray) -> tuple[ndarray,
                                                       ndarray,
                                                       ndarray]:
  """
  Discard three dimensional MeshBlock data outside provided range.

  Parameters
  ----------
  x : ndarray
    Grid over which function is sampled.

  y : ndarray
    Grid over which function is sampled.

  z : ndarray
    Grid over which function is sampled.

  x_R : ndarray
    Range to restrict by.

  y_R : ndarray
    Range to restrict by.

  z_R : ndarray
    Range to restrict by.

  Returns
  -------
  Restricted (x, y, z)
  """
  x_ = zeros_like(x)
  y_ = zeros_like(y)
  z_ = zeros_like(z)

  ix = 0

  for ix_MB in range(x.shape[0]):
    x_B = x[ix_MB]
    y_B = y[ix_MB]
    z_B = z[ix_MB]

    # check current block relevant
    if ((not interval_is_intersection_empty(x_B[0], x_B[-1],
                                            x_R[0], x_R[-1])) and
        (not interval_is_intersection_empty(y_B[0], y_B[-1],
                                            y_R[0], y_R[-1])) and
        (not interval_is_intersection_empty(z_B[0], z_B[-1],
                                            z_R[0], z_R[-1]))):
      x_[ix, ...] = x_B
      y_[ix, ...] = y_B
      z_[ix, ...] = z_B

      ix += 1

  return (x_[:ix, ...], y_[:ix, ...], z_[:ix, ...])


###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  _interpolate_1d_MeshBlock_field_direct = JITI(
    _interpolate_1d_MeshBlock_field_direct
  )

  _interpolate_2d_MeshBlock_field_direct = JITI(
    _interpolate_2d_MeshBlock_field_direct
  )

  _interpolate_3d_MeshBlock_field_direct = JITI(
    _interpolate_3d_MeshBlock_field_direct
  )

  clip_1d_MeshBlock_field_to_range = JITI(clip_1d_MeshBlock_field_to_range)
  clip_2d_MeshBlock_field_to_range = JITI(clip_2d_MeshBlock_field_to_range)
  clip_3d_MeshBlock_field_to_range = JITI(clip_3d_MeshBlock_field_to_range)

  clip_1d_MeshBlock_to_range = JITI(clip_1d_MeshBlock_to_range)
  clip_2d_MeshBlock_to_range = JITI(clip_2d_MeshBlock_to_range)
  clip_3d_MeshBlock_to_range = JITI(clip_3d_MeshBlock_to_range)

#
# :D
#
