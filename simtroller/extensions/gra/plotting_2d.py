#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Plotting in 2d for GR-Athena++ data.
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from functools import lru_cache
from typing import TYPE_CHECKING, Any, Optional, Set

from matplotlib.pyplot import (colorbar, get_cmap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# pylint: disable=redefined-builtin
from numpy import (broadcast_arrays, inf, meshgrid, sqrt, sum)
from numpy.typing import NDArray

# package imports
from simtroller.core.primitives import sequence_generator_flatten
from simtroller.extensions.gra import hdf5_get_vars

###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

# TODO: typing could be improved for input plotting classes

def plot_2d_interpolated(ax            : Any,
                         x_I           : NDArray,
                         y_I           : NDArray,
                         F_I           : NDArray,
                         cmap          : Any="inferno",
                         vmin          : Optional[float]=None,
                         vmax          : Optional[float]=None,
                         show_colorbar : bool=True,
                         plot_kwargs   : Optional[dict]=None) -> None:
  """
  Plot 2d interpolated data.

  Parameters
  ----------
  ax : Any
    matplotlib.axes

  x_I : NDArray
    x-ordinates of sampled field.

  y_I : NDArray
    y-ordinates of sampled field.

  F_I : NDArray
    Sampled field.

  cmap : Any="inferno"
    Color map to use. May be a string or actual color-map.

  vmin : Optional[float]=None
    Minimum value for color map.

  vmax : Optional[float]=None
    Maximum value for color map.

  show_colorbar : bool=True
    Control whether colorbar is added.

  plot_kwargs : Optional[dict]=None
    Additional kwargs to ax.plot
  """

  if plot_kwargs is None:
    plot_kwargs = {}

  if isinstance(cmap, str):
    selected_cmap = get_cmap(cmap)
  else:
    selected_cmap = cmap

  if vmin is None:
    vmin = F_I.min()
  if vmax is None:
    vmax = F_I.max()

  psm = ax.pcolormesh(
    *tuple(sequence_generator_flatten((meshgrid(x_I, y_I, indexing="ij"),
                                       F_I))),
    cmap=selected_cmap,
    vmin=vmin,
    vmax=vmax,
    **plot_kwargs)

  if show_colorbar:
    # add an axis for colorbar [se #44682146]
    divider_im = make_axes_locatable(ax)
    cax = divider_im.append_axes("right", size="5%", pad=0.02)
    _ = colorbar(psm, ax=ax, cax=cax)

def plot_2d_raw(ax            : Any,
                xa            : NDArray,
                xb            : NDArray,
                field_data    : NDArray,
                cmap          : Any="inferno",
                vmin          : Optional[float]=None,
                vmax          : Optional[float]=None,
                show_colorbar : bool=True,
                plot_kwargs   : Optional[dict]=None) -> None:
  """
  Plot 2d raw data.

  Parameters
  ----------
  ax : Any
    matplotlib.axes

  xa : NDArray
    x-ordinates of sampled field; native athdf structure or flat interpolated.

  xb : NDArray
    y-ordinates of sampled field; native athdf structure or flat interpolated.

  field_data : NDArray
    Sampled field; native athdf structure or flat interpolated.

  cmap : Any="inferno"
    Color map to use. May be a string or actual color-map.

  vmin : Optional[float]=None
    Minimum value for color map.

  vmax : Optional[float]=None
    Maximum value for color map.

  show_colorbar : bool=True
    Control whether colorbar is added.

  plot_kwargs : Optional[dict]=None
    Additional kwargs to ax.plot
  """

  if plot_kwargs is None:
    plot_kwargs = {}

  if isinstance(cmap, str):
    selected_cmap = get_cmap(cmap)
  else:
    selected_cmap = cmap

  if vmin is None:
    vmin = field_data.min()
  if vmax is None:
    vmax = field_data.max()

  if field_data.ndim == 3:
    for ix, fdata in enumerate(field_data):
      psm = ax.pcolormesh(
        *tuple(sequence_generator_flatten(
          (meshgrid(xa[ix], xb[ix], indexing="ij"), fdata))),
        cmap=selected_cmap,
        vmin=vmin,
        vmax=vmax,
        **plot_kwargs
      )
  else:
    psm = ax.pcolormesh(
      *tuple(sequence_generator_flatten(
        (meshgrid(xa, xb, indexing="ij"), field_data))),
      cmap=selected_cmap,
      vmin=vmin,
      vmax=vmax,
      **plot_kwargs
    )

  if show_colorbar:
    # add an axis for colorbar [se #44682146]
    divider_im = make_axes_locatable(ax)
    cax = divider_im.append_axes("right", size="5%", pad=0.02)
    _ = colorbar(psm, ax=ax, cax=cax)

def plot_2d_quiver(ax               : Any,
                   xa               : NDArray,
                   xb               : NDArray,
                   field_data_a     : NDArray,
                   field_data_b     : NDArray,
                   relative_cutoff  : float=0.05,
                   factor_downsample: int=2,
                   quiver_kwargs    : Optional[dict]=None) -> None:
  """
  Plot 2d quiver data.

  Parameters
  ----------
  ax : Any
    matplotlib.axes

  xa : NDArray
    x-ordinates of sampled field; native athdf structure or flat interpolated.

  xb : NDArray
    y-ordinates of sampled field; native athdf structure or flat interpolated.

  field_data_a : NDArray
    Sampled field component; native athdf structure or flat interpolated.

  field_data_b : NDArray
    Sampled field component; native athdf structure or flat interpolated.

  relative_cutoff : float=0.05
    Set to zero data below threshold (computed from field 2-norm).

  factor_downsample : int=2
    Factor to down-sample input data (avoid plotting too many arrows).

  quiver_kwargs : Optional[dict]=None
    Additional kwargs to ax.quiver
  """

  if quiver_kwargs is None:
    quiver_kwargs = {}

  # passed quiver_kwargs should supercede the function defaults
  have_quiver_kwargs = {}
  have_quiver_kwargs.update(quiver_kwargs)

  xyz_I = [xa, xb]
  data_quiver = [field_data_a.copy(), field_data_b.copy()]

  # down-sample factor for quiver
  ds = factor_downsample

  xyz_I_ = [xyz_[..., ::ds] for xyz_ in xyz_I]
  data_quiver_ = [dq[..., ::ds, ::ds] for dq in data_quiver]

  data_quiver_Norm2 = sqrt(sum([dq ** 2 for dq in data_quiver_], axis=0))
  data_quiver_ = [dq / data_quiver_Norm2.max() for dq in data_quiver_]

  for dq in data_quiver_:
    dq[data_quiver_Norm2<=relative_cutoff] = 0

  if data_quiver[0].ndim == 3:
    # Data partitioned by MeshBlock
    x,y = broadcast_arrays(*[xyz_I_[0][:,:,None],
                            xyz_I_[1][:,None,:]])
    ax.quiver(*(x.flatten(), y.flatten()),
              *(data_quiver_[0].flatten(),
                data_quiver_[1].flatten()),
              **have_quiver_kwargs)

  else:
    # Have flat (interpolated) data (meshgrid needs specific idx remap)
    ax.quiver(*meshgrid(*xyz_I_, indexing="ij"), *data_quiver_,
              **have_quiver_kwargs)


def plot_2d_MeshBlock_structure(ax          : Any,
                                filename    : str,
                                NGHOST      : int=None,
                                max_level   : Optional[int]=20,
                                color       : Any="k",
                                linewidth   : Any=0.8,
                                plot_kwargs : Optional[dict]=None) -> None:
  """
  Plot two-dimensional MeshBlock structure based on a filename.

  Parameters
  ----------
  ax : Any
    matplotlib.axes

  filename : str
    Filename to parse.

  NGHOST : int=None
    Number of ghost zones.

  max_level : Optional[int]=20
    Maximum level to plot.

  color : Any="k"
    Color to use for grid.

  linewidth : Any=0.8
    Width to use for grid.

  plot_kwargs : Optional[dict]=None
    Additional kwargs to ax.plot
  """
  data = hdf5_get_vars(filename,
                       return_sliced=False,
                       physical_only=True,
                       NGHOST=NGHOST)

  if len(data["xf_labels"]) != 2:
    raise ValueError(f"Dataset {filename} dimensionality is not 2.")

  if plot_kwargs is None:
    plot_kwargs = {}

  plot_kwargs.update({"color": color, "linewidth": linewidth})

  X, Y = data["xf"]
  Levels = data["Levels"]


  for xyl in zip(X, Y, Levels):
    x, y, L = xyl
    if L <= max_level:
      x_1, x_2 = x[0], x[-1]
      y_1, y_2 = y[0], y[-1]

      ax.plot((x_1, x_1), (y_1, y_2), **plot_kwargs)
      ax.plot((x_2, x_2), (y_1, y_2), **plot_kwargs)
      ax.plot((x_1, x_2), (y_1, y_1), **plot_kwargs)
      ax.plot((x_1, x_2), (y_2, y_2), **plot_kwargs)


#
# :D
#
