#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Various data processing functions for GR-Athena++.
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from functools import lru_cache
from typing import TYPE_CHECKING, Optional, Set, Tuple, Union
import re as _re

from sys import exit

# pylint: disable=redefined-builtin
from numpy import (array, argsort, cumsum, float64, ndarray, min, max,
                   squeeze, transpose)
from numpy.typing import NDArray

# package imports
from simtroller.core.primitives import (dict_nested_set,
                                        list_like,
                                        list_split_chunks,
                                        sequence_generator_flatten,
                                        tuple_like)
from simtroller.core.numerical import ndarray_dim_order_exchange
from simtroller.core.io import (hdf5_file_get_attr,
                                hdf5_file_get_dataset_names,
                                hdf5_dataset_get,
                                csv_load,
                                dir_list,
                                dir_filter,
                                raw_load,
                                path_is_extant,
                                path_to_absolute)

# locals
_NAMES_GRID_VARIABLES = {
  "Levels", "LogicalLocations",
  "x1f", "x2f", "x3f",
  "x1v", "x2v", "x3v"
}

# TODO: complete me
_VARIABLE_LABEL_TO_SAMPLING = {
  "rho":       ("x1v", "x2v", "x3v"),
  "press":     ("x1v", "x2v", "x3v"),
  "vel1":      ("x1v", "x2v", "x3v"),
  "vel2":      ("x1v", "x2v", "x3v"),
  "vel3":      ("x1v", "x2v", "x3v"),
  "dens":      ("x1v", "x2v", "x3v"),
  "Etot":      ("x1v", "x2v", "x3v"),
  "mom1":      ("x1v", "x2v", "x3v"),
  "mom2":      ("x1v", "x2v", "x3v"),
  "mom3":      ("x1v", "x2v", "x3v"),
  # magnetic fields
  "Bcc1":      ("x1v", "x2v", "x3v"),
  "Bcc2":      ("x1v", "x2v", "x3v"),
  "Bcc3":      ("x1v", "x2v", "x3v"),
  # ADM
  "adm.gxx":   ("x1f", "x2f", "x3f"),
  "adm.gxy":   ("x1f", "x2f", "x3f"),
  "adm.gxz":   ("x1f", "x2f", "x3f"),
  "adm.gyy":   ("x1f", "x2f", "x3f"),
  "adm.gyz":   ("x1f", "x2f", "x3f"),
  "adm.gzz":   ("x1f", "x2f", "x3f"),
  "adm.Kxx":   ("x1f", "x2f", "x3f"),
  "adm.Kxy":   ("x1f", "x2f", "x3f"),
  "adm.Kxz":   ("x1f", "x2f", "x3f"),
  "adm.Kyy":   ("x1f", "x2f", "x3f"),
  "adm.Kyz":   ("x1f", "x2f", "x3f"),
  "adm.Kzz":   ("x1f", "x2f", "x3f"),
  "adm.psi4":  ("x1f", "x2f", "x3f"),
  # Z4c
  "z4c.chi":   ("x1f", "x2f", "x3f"),
  "z4c.gxx":   ("x1f", "x2f", "x3f"),
  "z4c.gxy":   ("x1f", "x2f", "x3f"),
  "z4c.gxz":   ("x1f", "x2f", "x3f"),
  "z4c.gyy":   ("x1f", "x2f", "x3f"),
  "z4c.gyz":   ("x1f", "x2f", "x3f"),
  "z4c.gzz":   ("x1f", "x2f", "x3f"),
  "z4c.Khat":  ("x1f", "x2f", "x3f"),
  "z4c.Axx":   ("x1f", "x2f", "x3f"),
  "z4c.Axy":   ("x1f", "x2f", "x3f"),
  "z4c.Axz":   ("x1f", "x2f", "x3f"),
  "z4c.Ayy":   ("x1f", "x2f", "x3f"),
  "z4c.Ayz":   ("x1f", "x2f", "x3f"),
  "z4c.Azz":   ("x1f", "x2f", "x3f"),
  "z4c.Gamx":  ("x1f", "x2f", "x3f"),
  "z4c.Gamy":  ("x1f", "x2f", "x3f"),
  "z4c.Gamz":  ("x1f", "x2f", "x3f"),
  "z4c.Theta": ("x1f", "x2f", "x3f"),
  "z4c.alpha": ("x1f", "x2f", "x3f"),
  "z4c.betax": ("x1f", "x2f", "x3f"),
  "z4c.betay": ("x1f", "x2f", "x3f"),
  "z4c.betaz": ("x1f", "x2f", "x3f"),
  # con
  "con.C":     ("x1f", "x2f", "x3f"),
  "con.H":     ("x1f", "x2f", "x3f"),
  "con.M":     ("x1f", "x2f", "x3f"),
  "con.Z":     ("x1f", "x2f", "x3f"),
  "con.Mx":    ("x1f", "x2f", "x3f"),
  "con.My":    ("x1f", "x2f", "x3f"),
  "con.Mz":    ("x1f", "x2f", "x3f"),
  # wave
  "wExact":    ("x1f", "x2f", "x3f"),
  "wU":        ("x1f", "x2f", "x3f"),
  "wError":    ("x1f", "x2f", "x3f"),
  # weyl
  "weyl.rpsi4": ("x1v", "x2v", "x3v"),
  "weyl.ipsi4": ("x1v", "x2v", "x3v"),
}
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

def coordinates_identify_slice(x1=None, x2=None, x3=None, as_indices=False):
  """
  Convenience function to identify the variety of slice based on input
  x1, x2, x3.

  Parameters
  ----------
  x1 = None :
    Optional first variable.

  x2 = None :
    Optional second variable.

  x3 = None :
    Optional third variable.

  as_indices = False:
    Control whether return is indicial: e.g. (0, 1, 2) or "x1x2x3"

  Returns
  -------
  (ndim, coordinate combination)
  """
  sz_cut = 2  # if we have fewer points than this then we are on a slice

  # extract MeshBlock sampling for each direction
  N_mb_wg_x1 = x1.shape[1] if x1 is not None else 0
  N_mb_wg_x2 = x2.shape[1] if x2 is not None else 0
  N_mb_wg_x3 = x3.shape[1] if x3 is not None else 0

  # infer slice dimensionality
  is_sl_x1 = N_mb_wg_x1 > sz_cut
  is_sl_x2 = N_mb_wg_x2 > sz_cut
  is_sl_x3 = N_mb_wg_x3 > sz_cut

  sl_dim = is_sl_x1 + is_sl_x2 + is_sl_x3 + 0

  if as_indices:
    variety = []
    if is_sl_x1:
      variety.append(0)
    if is_sl_x2:
      variety.append(1)
    if is_sl_x3:
      variety.append(2)
    variety = tuple(variety)

  else:
    variety = is_sl_x1 * "x1" + is_sl_x2 * "x2" + is_sl_x3 * "x3"

  return (sl_dim, variety)

def coordinates_get_dx_extrema(x=None):
  """
  Given input grid extract min(dx) and max(dx)
  """
  x_diff = x[:, 1] - x[:, 0]
  return min(x_diff), max(x_diff)

def hdf5_get_dataset_names(filename           : str,
                           exclude_grid_names : bool=True) -> Set[str]:
  """
  GR-Athena++ athdf stores variables in 'prim', 'hydro' etc.
  Get what is available in a given file while optionally partitioning out grid
  names.

  Parameters
  ----------
  filename : str
    Filename to parse.

  exclude_grid_names : bool = True
    Take set difference between result and grid variable names.

  Returns
  -------
  Resulting names found.
  """
  result = set(hdf5_file_get_dataset_names(filename))

  if exclude_grid_names:
    result = result.difference(_NAMES_GRID_VARIABLES)

  return result

@lru_cache(maxsize=None, typed=True)
def variable_label_to_sampling_tuple(variable_label):
  """
  Given a variable label return the natural sampling tuple.
  """
  if not isinstance(variable_label, str):
    variable_label = variable_label.decode("ascii")

  try:
    return _VARIABLE_LABEL_TO_SAMPLING[variable_label]
  except KeyError as exc:
    raise NotImplementedError(
      f"Unknown variable_label={variable_label}") from exc

@lru_cache(maxsize=None, typed=True)
def slicer_physical(sampling_tuple, NGHOST=None, ix_var_slice=None):
  """
  Given a sampling tuple and NGHOST number then construct slicers to physical
  nodes. Optionally take a subset (removing singleton axes).

  Parameters
  ----------
  sampling_tuple
    Variable labels to use. The case ("x1f", "x2f", "x3f") is treated as VC
    and the slicer for field data is adjusted accordingly.

  NGHOST = None
    Number of ghost zones.

  ix_var_slice = None
    Optionally specify non-singleton axes.

  Returns
  -------
  var_slice, field_slice
  """
  if NGHOST is None:
    raise ValueError("NGHOST must be provided")

  # deal with VC cases
  if sampling_tuple == ("x1f", "x2f", "x3f"):
    offset = 1
  else:
    offset = 0

  # pylint: disable=invalid-unary-operand-type
  field_slice = (slice(NGHOST, -(NGHOST - offset)),
                 slice(NGHOST, -(NGHOST - offset)),
                 slice(NGHOST, -(NGHOST - offset)))

  var_slice = (slice(NGHOST, -NGHOST),
               slice(NGHOST, -NGHOST),
               slice(NGHOST, -NGHOST))

  if ix_var_slice is not None:
    var_slice = [var_slice[ix] for ix in ix_var_slice]
    field_slice = [field_slice[ix] for ix in ix_var_slice]

  # prepend block slice
  field_slice = tuple(sequence_generator_flatten((slice(None), field_slice)))

  return var_slice, field_slice

def hdf5_get_field(filename, variable_label, physical_only=True, NGHOST=None,
  sampling_tuple=None):
  """
  Parameters
  ----------
  filename
    hdf5 file to process.

  variable_label
    Label of field data to extract.

  physical_only = True
    Control whether ghost zones are clipped.

  NGHOST = None
    Control number of ghost zones that are clipped when physical_only is True.

  sampling_tuple = None
    Optional direct specification of sampling tuple. If None then inferred
    based on variable_label.

  Returns
  -------
  var_data, field_data

  Notes
  -----
  Data is reordered internally for "C" order.
  """
  if sampling_tuple is None:
    # get the sampling of the chosen variable
    sampling_tuple = variable_label_to_sampling_tuple(variable_label)

  # TODO: clean this up; should work for generic passed sampling_tuple
  # argument labels to coordinates_identify_slice
    xn =[]
    for ix, xlv in enumerate(sampling_tuple):
      s_ix = str(ix+1)
      if s_ix in xlv:
        xn.append("x" + s_ix)
  else:
    xn = ("x1", "x2", "x3")

  # get the type of slicing
  x = {arg: hdf5_dataset_get(filename=filename, dataset_name=name,
                              load_to_memory=False)
       for name, arg in zip(sampling_tuple, xn)}

  _, ix_var_slice = coordinates_identify_slice(
    **x, as_indices=True
  )

  # jump through some hoops to extract variable by label ----------------------
  DatasetNames = hdf5_file_get_attr(filename, "DatasetNames")
  NumVariables = hdf5_file_get_attr(filename, "NumVariables")
  VariableNames = hdf5_file_get_attr(filename, "VariableNames")

  # locate correct dataset for the required variable
  ix_vl_flat = VariableNames.tolist().index(variable_label.encode())

  # ugly, but directly extract salient DatasetName:
  cnv = cumsum(NumVariables)
  ccn = 0
  for dix, dnv in enumerate(zip(DatasetNames, NumVariables)):
    dn, nv = dnv
    if cnv[dix] > ix_vl_flat:
      ix_field = ix_vl_flat - ccn
      name = dn
      break
    ccn += nv

  # get the variable index in the identified set
  # ix_field = ctot_vals[ix-1] - ix_vl_flat
  # name = dname

  # # get the variable index
  # VariableNames = hdf5_file_get_attr(filename, "VariableNames")
  # ix_field = VariableNames.tolist().index(variable_label.encode())

  # # should only be one name per data-set
  # name, *_ = hdf5_get_dataset_names(filename)

  # only load specific field
  data_field = array(hdf5_dataset_get(filename, dataset_name=name,
                                      load_to_memory=False)[ix_field])

  # squeeze singleton axes and reorder
  data_field = ndarray_dim_order_exchange(squeeze(data_field),
                                          num_skip_dim=1)

  # if we have a slice we should take a subset of specification
  vars_slice = tuple(sampling_tuple[ix] for ix in ix_var_slice)
  vars_data = [
    hdf5_dataset_get(filename, dataset_name=var) for var in vars_slice
  ]

  # cut to only physical nodes
  if physical_only:
    var_slice, field_slice = slicer_physical(sampling_tuple, NGHOST=NGHOST,
                                             ix_var_slice=ix_var_slice)

    vars_data = [vd[:, vs] for vd, vs in zip(vars_data, var_slice)]
    try:
      data_field = data_field[field_slice]
    except IndexError:
      # ensure we have the block axis
      data_field = data_field[field_slice[1:]][None, ...]

  return vars_data, data_field

def hdf5_get_vars(filename      : str,
                  return_sliced : bool=True,
                  physical_only : bool=False,
                  NGHOST        : Optional[int]=None) -> dict:
  """
  Extract grid variables from a filename.
  If a variable is sliced away it is dropped.

  Parameters
  ----------
  filename : str
    Filename to parse.

  return_sliced : bool=True
    When variables are sliced control whether sliced axes are also returned.

  physical_only : bool=False,
    Control whether salient variables are sliced to physical values.

  NGHOST : Optional[int]=None
    If physical_only is enabled then how many ghosts zones will be cut.

  Returns
  -------
  dict
    Keys:
    {"dim", "xf_labels", "xv_labels", "Levels", "xf", "xv"}.
    If return_sliced then additionally we have keys:
    {"fxf_labels", "fxv_labels", "fxf", "fxv"}
  """
  xf = ("x1f", "x2f", "x3f")
  xv = ("x1v", "x2v", "x3v")
  xn = ("x1", "x2", "x3")

  interrogate_x = {arg: hdf5_dataset_get(filename, dataset_name=name,
                                         load_to_memory=False)
                                         for name, arg in zip(xf, xn)}

  dim, ix_var_slice = coordinates_identify_slice(
    **interrogate_x, as_indices=True
  )

  # salient vars
  s_xf = tuple(xf[ix] for ix in ix_var_slice)
  s_xv = tuple(xv[ix] for ix in ix_var_slice)

  if physical_only:
    # pylint: disable=invalid-unary-operand-type
    if NGHOST is None:
      raise ValueError("NGHOST must be provided for physical_only.")
    data_xf = [hdf5_dataset_get(filename, dataset_name=ax_xf)[:,NGHOST:-NGHOST]
               for ax_xf in s_xf]
    data_xv = [hdf5_dataset_get(filename, dataset_name=ax_xv)[:,NGHOST:-NGHOST]
               for ax_xv in s_xv]
  else:
    data_xf = [hdf5_dataset_get(filename, dataset_name=ax_xf) for ax_xf in s_xf]
    data_xv = [hdf5_dataset_get(filename, dataset_name=ax_xv) for ax_xv in s_xv]

  levels = hdf5_dataset_get(filename, dataset_name="Levels",
                            load_to_memory=True)

  data = {
    "dim": dim,
    "Levels": levels,
    "xf_labels": s_xf,
    "xv_labels": s_xv,
    "xf": data_xf,
    "xv": data_xv
  }

  # fixed vars
  if return_sliced:
    f_xf = set(xf).difference(set(s_xf))
    f_xv = set(xv).difference(set(s_xv))

    data_fxf = [
      hdf5_dataset_get(filename, dataset_name=ax_xf)
      for ax_xf in f_xf
    ]
    data_fxv = [
      hdf5_dataset_get(filename, dataset_name=ax_xf)
      for ax_xf in f_xv
    ]

    data["fxf_labels"] = f_xf
    data["fxv_labels"] = f_xv
    data["fxf"] = data_fxf
    data["fxv"] = data_fxv

  return data

def parameter_file_to_dict(filename : str) -> dict:
  """
  Given a GR-Athena++ parameter file, parse and return a dictionary
  representation.

  Parameters
  ----------
  filename : str
    Filename to parse.

  Returns
  -------
  par_dict : dict
    Parsed file.
  """
  raw_par = raw_load(filename)
  par_split = []
  for line in raw_par.split("\n"):
    cur_line = line.replace("\t", " ")
    cur_line = cur_line.replace(" ", "")

    if "#" in cur_line:
      cur_line = cur_line[:cur_line.find("#")]

    if len(cur_line) > 0:
      par_split.append(cur_line)

  par_dict = {}
  cur_section = "untitled"
  for par in par_split:
    if (par[0] == "<") and (par[-1] == ">"):
      cur_section = par[1:-1]
    else:
      ix_eq = par.find("=")
      var = par[:ix_eq]
      value = par[(ix_eq + 1):]

      dict_nested_set([cur_section, var],
        par_dict, value, update=True)

  return par_dict

def parameter_dict_get_extraction_radii(par_dict : dict, max_radii : int=9):
  """
  Given a processed parameter file return (sorted) extraction radii.

  Parameters
  ----------
  par_dict : dict
    Processed parameters as got from:
      simtroller.extensions.gra.parameter_file_to_dict

  max_radii : int=9
    Maximum number of radii to return.
  """
  var_radii = []
  radii = []
  for var, value in par_dict["z4c"].items():
    var_radii

  # key is clipped down to number
  radii = {int(var[18:]): value for var, value in par_dict["z4c"].items()
          if "extraction_radius_" in var}
  ix_sorted = argsort(array(list(radii.keys())))

  radii_sorted = []

  for ix in ix_sorted[:min((max_radii, len(radii.keys())))]:
    radii_sorted.append(float64(radii[ix]))
  return array(radii_sorted)

def hst_load(filename : Union[str, list, tuple],
             loadtxt_kwargs : Optional[dict]={}) -> Tuple[Tuple, NDArray]:
  """
  Convenience wrapper for GR-Athena++ history files.

  Utilizes core.io.csv_load and also extracts # row with variable labels.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename of csv data to load.

  loadtxt_kwargs : Optional[dict]={}
    Additional kwargs to pass to numpy.loadtxt

  Returns
  -------
  labels and data contained in file.
  """
  fn_abs = path_to_absolute(filename)
  data = csv_load(fn_abs, **loadtxt_kwargs)

  # labels appear on second line
  with open(fn_abs, "r") as fh:
    next(fh)
    info_col = fh.readline()

  # TODO: replace this ugly label extraction logic;
  #       currently should deal with space-filled labels
  to_replace = ("#", " ", "\n")
  for tr in to_replace:
    info_col = info_col.replace(tr, "")

  lbl_col = tuple([el.split("[")[0] for el in info_col.split("=")[1:]])

  return lbl_col, data

class scrape_dir_athdf(object):
  # Scrape information from directory containing athdf
  #
  # - characterize differing outN
  #   - DatasetNames
  #   - VariableNames
  #   - dimensionality & slicing direction
  #   - extract dt for each (requires at least two files)
  #   - map range in t to range in dumped iteration
  #
  # Implementation details are hidden, may change and use should be avoided

  def __init__(self, dir_data : str, N_B : Union[int, float, list, tuple]):
    self.dir_data = dir_data
    self.N_B = N_B

    self._fns = self._parse_fns()
    self._out_sets, self._fns_sets = self._parse_fns_out()

    self._out_to_ix = {
      out: ix for out, ix in zip(self._out_sets, range(len(self._out_sets)))
    }

    self._fns_stem = [
      self._parse_fn_stem(out) for out in self._out_sets
    ]

    self._out_DatasetNames = [
      self._parse_out_attr(out, "DatasetNames") for out in self._out_sets
    ]

    self._out_VariableNames = [
      self._parse_out_attr(out, "VariableNames") for out in self._out_sets
    ]

    self._out_NumVariables = [
      self._parse_out_attr(out, "NumVariables") for out in self._out_sets
    ]

    self._miniter_str = [
      self._parse_min_iter(out) for out in self._out_sets
    ]

    self._miniter_int = [
      int(val) for val in self._miniter_str
    ]

    self._maxiter_str = [
      self._parse_max_iter(out) for out in self._out_sets
    ]

    self._maxiter_int = [
      int(val) for val in self._maxiter_str
    ]

    self._out_dt = [
      self._parse_dt(out) for out in self._out_sets
    ]

    self._var_map = self._parse_to_var_key()

    self._out_ng = self._parse_dim_ng()

  def _parse_fns(self):
    # parse relevant data dir for relevant files (sorted)
    dir_sim_list = dir_list(self.dir_data, recursive=False)
    fns_athdf = dir_filter(dir_sim_list,
                           mode="directories",
                           extensions=[".athdf"])

    return sorted(fns_athdf)

  def _parse_fns_out(self):
    # filter fns_athdf splitting by "outN"
    # return sorted list of "out" together with dict of split fns

    out_known = set()
    reg_out = _re.compile(r"out\d+")

    fns_by_out = {}

    for fn in self._fns:
      out_str = reg_out.search(fn).group()
      out_known.add(out_str)

      if out_str in fns_by_out:
        fns_by_out[out_str].append(fn)
      else:
        fns_by_out[out_str] = [fn, ]

    for key, val in fns_by_out.items():
      fns_by_out[key] = sorted(val)

    # Last iter each "out" set may still be being written.
    # To check:
    # Attempt to extract "Time"; if fail, drop last iter
    for out in out_known:
      try:
        hdf5_file_get_attr(fns_by_out[out][-1], "Time")
      except OSError:
        fns_by_out[out].pop()

    if len(out_known) == 0:
      raise RuntimeError(f"No athdf files found in {self.dir_data}")

    list_out_types = sorted(list(out_known))

    return list_out_types, fns_by_out

  def _generator_fns_by_out(self, out : str, as_reversed : bool=False):
    # get generator for filenames by out

    if as_reversed:
      return (fn for fn in self._fns_sets[out][::-1] if out in fn)

    return (fn for fn in self._fns_sets[out] if out in fn)

  def _parse_fn_stem(self, out : str):
    # get filename stem (i.e. everything before .outN)
    gen_fns = self._generator_fns_by_out(out, as_reversed=False)
    return next(gen_fns).split(out)[0][:-1]

  def _parse_out_attr(self, out : str, attr : str):
    # extract attribute from hdf5; convert to ascii where needed
    fn = self._fns_sets[out][0]
    Attrs = hdf5_file_get_attr(fn, attr)

    try:
      return tuple(var.decode("ascii") for var in Attrs)
    except:
      return tuple(var for var in Attrs)

  def _parse_min_iter(self, out : str):
    # extract minimum iteration
    fn = next(self._generator_fns_by_out(out, as_reversed=False))
    tail_fn = _re.search(r"\d+.athdf", fn).group()

    # return as string
    return tail_fn.split(".")[0]

  def _parse_max_iter(self, out : str):
    # extract maximum iteration
    fn = next(self._generator_fns_by_out(out, as_reversed=True))
    tail_fn = _re.search(r"\d+.athdf", fn).group()

    # return as string
    return tail_fn.split(".")[0]

  def _parse_dt(self, out : str):
    # given multiple output files, infer dt for output set

    maxiter = self._maxiter_int[self._out_to_ix[out]]
    if maxiter > 1:
      return (self.get_iter_time(out, maxiter) -
              self.get_iter_time(out, maxiter-1))

    print(f"Warning, dt extraction for {out} failed; need more datasets")

    return 0

  def _sanitize_arg_as_int_triplet(
    self, triplet : Union[int, float, list, tuple]):
    # ensure scalar becomes listlike triplet
    NDIM = 3
    if isinstance(triplet, (int, float)):
      triplet = [triplet, ] * NDIM

    if (len(triplet) == 1):
      triplet = [triplet[0], ] * NDIM

    elif (len(triplet) != NDIM):
      raise ValueError(f"Triplet {triplet} malformed.")

    return tuple(int(val) for val in triplet)

  def get_iter_fn(self, out : str, iterate : int=0):
    # Given set and iteration, return filename
    #
    # athdf iterations are with 5 digits
    str_iter = f"{int(iterate):05}"

    fn =  ".".join((self._fns_stem[self._out_to_ix[out]],
                    out,
                    str_iter,
                    "athdf"))

    path_is_extant(fn, fail_on_missing=True)

    return fn

  def get_data_spec_iter(self, out : str, iterate : int):
    # Given out and iterate extract:
    # - overall shape
    # - number of variables
    # - number of MeshBlocks
    # - dimensionality
    # - xn slicing
    fn = self.get_iter_fn(out, iterate)

    # N.B. only 1 per athdf
    dataset_names = self._out_DatasetNames[self._out_to_ix[out]]

    dataset = hdf5_dataset_get(fn, dataset_names[0], load_to_memory=False)

    shape = dataset.shape
    num_vars, num_MeshBlocks = shape[0], shape[1]

    # x3,x2,x1 -> x1,x2,x3
    NDIM = 3
    dim_xyz = shape[-NDIM:][::-1]

    slicing = tuple([f"x{ix}" for ix in range(1,NDIM+1) if dim_xyz[ix-1] > 1])

    return num_vars, num_MeshBlocks, dim_xyz, slicing

  def get_iter_time(self, out : str, iterate : int=0):
    # Given set and iteration, lookup time
    fn = self.get_iter_fn(out, iterate)
    return hdf5_file_get_attr(fn, "Time")

  def _get_ghosts(self, dim_xyz, N_B : Union[int, float, list, tuple]):

    dg = []
    have_ghosts = True

    for d, N in zip(dim_xyz, N_B):
      if d > 1:
        cur_dg = abs(d - N) // 2  # dataset ax - sampling on ax
        have_ghosts = have_ghosts and (cur_dg > 1)
        dg.append(cur_dg)
      else:
        dg.append(0)

    return have_ghosts, max(dg)

  def _parse_to_var_key(self):
    # create keys for variable lookup
    map = {}

    for oux, out in enumerate(self._out_sets):
      maxiter = self._maxiter_int[oux]

      _, _, dim_xyz, slicing = self.get_data_spec_iter(out, maxiter)

      # N.B. can have multiple datasets per athdf
      dataset_names = self._out_DatasetNames[oux]
      # Split flattened variable list to correct dataset
      vars_split = list_split_chunks(self._out_VariableNames[oux],
                                     self._out_NumVariables[oux])

      have_ghosts, dg = self._get_ghosts(dim_xyz, self.N_B)

      for var in self._out_VariableNames[oux]:
        key = (var, slicing, have_ghosts)

        # find index of variable in correct dataset
        for vix, vs in enumerate(vars_split):
          if var in vs:
            var_index = vs.index(var)
            dataset_index = vix
            dataset_name = dataset_names[dataset_index]

        map[key] = (out, var_index, dg, dataset_name)

    return map

  def _parse_dim_ng(self):
    # get dimxyz and ghosts for the output types
    info = []

    for oux, out in enumerate(self._out_sets):
      maxiter = self._maxiter_int[oux]

      _, _, dim_xyz, slicing = self.get_data_spec_iter(out, maxiter)

      have_ghosts, dg = self._get_ghosts(dim_xyz, self.N_B)

      info.append((dim_xyz, dg))

    return tuple(info)

  @lru_cache(maxsize=None, typed=True)
  def _parse_var_info(self,
                      var         : str,
                      sampling    : Union[str, list, tuple],
                      with_ghosts : bool=True):
    # infer required key, return variable information

    if isinstance(sampling, str):
      sampling = (sampling, )

    # ensure we also have a slicing (x1,...) and not a sampling (x1f,...)
    slicing = tuple([val[:2] for val in sampling])

    var_key = (var, slicing, with_ghosts)

    try:
      var_info = self._var_map[var_key]
    except KeyError:
      print(f"Could not find data corresponding to key: {var_key}.")
      print("Known keys (var, slicing, with_ghosts):")
      for key in self._var_map.keys():
        print(key)
      exit()

    return sampling, var_info    # pdb.set_trace()


  def get_var_info(self,
                   var         : str,
                   sampling    : Union[str, list, tuple],
                   with_ghosts : bool = True):
    # Given variable and sampling specification extract information.
    # Returned: (out, vix, dg)
    sampling, var_info = self._parse_var_info(var,
                                              sampling,
                                              with_ghosts=with_ghosts)
    return var_info

  def get_grid(self,
               out         : str,
               sampling    : Union[str, list, tuple],
               iterate     : int=0,
               strip_dg    : int=0):
    # Use "out" and extract grid associated with sampling from a target iterate
    # Optionally strip ghosts
    sampling = tuple_like(sampling)
    fn_data = self.get_iter_fn(out, iterate)

    xyz = [hdf5_dataset_get(fn_data, grid_var, load_to_memory=True)
           for grid_var in sampling]

    # slice away ghosts if desired
    if strip_dg > 0:
      dim_xyz, ng = self._out_ng[self._out_to_ix[out]]

      var_sl, _ = self.slicer_physical(ng, sampling, dg=ng-strip_dg)
      xyz = tuple(
        gr[sl] for gr, sl in zip(xyz, var_sl)
      )

    return xyz

  def get_grid_levels(self,
                      out    : str,
                      iterate: int=0) -> ndarray:
    # return levels that MeshBlocks live on
    fn_data = self.get_iter_fn(out, iterate)
    return hdf5_dataset_get(fn_data, "Levels")

  def get_var(self,
              var         : str,
              sampling    : Union[str, list, tuple],
              iterate     : int=0,
              with_ghosts : bool=True,
              strip_dg    : int=0):
    # Extract variable data
    #
    # Returns:
    # Squeezed field_data

    sampling, var_info = self._parse_var_info(var,
                                              sampling,
                                              with_ghosts=with_ghosts)
    out, var_index, dg, dataset_name = var_info

    fn_data = self.get_iter_fn(out, iterate)

    dataset = hdf5_dataset_get(fn_data, dataset_name, load_to_memory=True)

    # xyz = [hdf5_dataset_get(fn_data, grid_var, load_to_memory=True)
    #        for grid_var in sampling]

    # get salient field and shift to (fixed dim storage):
    # ix_MB, ix_x, ix_y, ...
    field_data = transpose(dataset[var_index], (0, 3, 2, 1)).squeeze()

    if strip_dg > 0:

      dim_xyz, ng = self._out_ng[self._out_to_ix[out]]

      _, fcn_sl = self.slicer_physical(ng, sampling, dg=ng-strip_dg)

      # _, fcn_sl = self.slicer_physical(strip_dg, sampling)

      try:
        field_data = field_data[fcn_sl]
      except IndexError:
        # When data-set consists of a single block need reslice
        field_data = field_data[fcn_sl[1:]][None,...]

    return field_data

  @lru_cache(maxsize=None, typed=True)
  def get_iter_range(self,
                     var         : str,
                     sampling    : Union[str, list, tuple],
                     with_ghosts : bool=True):
    # given desired field and sampling, return known iterate range

    sampling, var_info = self._parse_var_info(var,
                                              sampling,
                                              with_ghosts=with_ghosts)

    out, var_index, dg, dataset_name = var_info

    maxiter = self._maxiter_int[self._out_to_ix[out]]
    miniter = self._miniter_int[self._out_to_ix[out]]

    return miniter, maxiter

  def get_iter_from_time(self,
                         var         : str,
                         sampling    : Union[str, list, tuple],
                         Time        : float,
                         with_ghosts : bool=True) -> int:
    # Map input time to iterate

    miniter, maxiter = self.get_iter_range(var, sampling, with_ghosts)
    sampling, var_info = self._parse_var_info(var,
                                              sampling,
                                              with_ghosts=with_ghosts)

    out, vix, dg = var_info
    maxtime = self.get_iter_time(out, maxiter)

    dt = self._out_dt[self._out_to_ix[out]]

    # N_iter = int(maxtime // dt)
    N_iter_Time = int(Time // dt)

    # closest time that we have iterate for
    iter_Time = min([max([miniter, N_iter_Time]), maxiter])

    return iter_Time

  @lru_cache(maxsize=None, typed=True)
  def slicer_physical(self,
                      ng,
                      sampling,
                      dg=None):
    # Given a number of ghosts and sampling, construct slicer
    # reducing to physical nodes only (i.e. no ghosts)
    var_sl = []
    fcn_sl = [slice(None), ]

    if ng > 0:
      for gix, grid_var in enumerate(sampling):
        # face-centered field data needs an offset
        offset = 1 if "f" in grid_var else 0
        # ix = int(grid_var[1])-1
        il, iu = ng, self.N_B[gix]+ng+offset
        if dg is not None:
          il, iu = il-dg, iu+dg
        nog_sl = slice(il, iu, None)
        var_sl.append((slice(None), nog_sl))
        fcn_sl.append(nog_sl)

    else:
      for grid_var in sampling:
        var_sl.append((slice(None), slice(None)))
        fcn_sl.append(slice(None))

    return tuple(var_sl), tuple(fcn_sl)

  def debug_data_keys(self):
    # return the variable map for debugging / sanity checks
    return self._var_map

#
# :D
#
