"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functionality for GR-Athena++.
"""
from simtroller.extensions.gra.data_processing import (
  coordinates_identify_slice,
  coordinates_get_dx_extrema,
  hdf5_get_dataset_names,
  hdf5_get_field,
  hdf5_get_vars,
  slicer_physical,
  variable_label_to_sampling_tuple,
  parameter_file_to_dict,
  parameter_dict_get_extraction_radii,
  hst_load,
  scrape_dir_athdf)

from simtroller.extensions.gra.data_processing_scalars import (
  scalars_trackers_process,
  scalars_trackers_interpolate,
  scalars_waveforms_ix_map,
  scalars_waveforms_process,
  scalars_waveforms_interpolate,
  scalars_waveforms_raw_to_complex)

from simtroller.extensions.gra.interpolation import (
  interpolate_1d_MeshBlock_field,
  interpolate_2d_MeshBlock_field,
  interpolate_3d_MeshBlock_field,
  clip_1d_MeshBlock_field_to_range,
  clip_2d_MeshBlock_field_to_range,
  clip_3d_MeshBlock_field_to_range,
  clip_1d_MeshBlock_to_range,
  clip_2d_MeshBlock_to_range,
  clip_3d_MeshBlock_to_range)

from simtroller.extensions.gra.plotting_2d import (
  plot_2d_interpolated,
  plot_2d_raw,
  plot_2d_quiver,
  plot_2d_MeshBlock_structure)

from simtroller.extensions.gra.plotting_nd import (
  plot_nd_MeshBlock_structure)

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("data_processing"))
exec(_import_clear("data_processing_scalars"))
exec(_import_clear("interpolation"))
exec(_import_clear("plotting_2d"))
# =============================================================================

#
# :D
#
