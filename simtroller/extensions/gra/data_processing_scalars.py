#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Data processing for GR-Athena++ related to scalar output
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from functools import lru_cache
from typing import TYPE_CHECKING, Any, Optional, Set, Union

# pylint: disable=redefined-builtin
from numpy import (array, concatenate, hstack, vstack)
from numpy.typing import NDArray

# package imports
from simtroller.core import (parallel_pool_apply_async, )
from simtroller.core.io import (csv_load, )
from simtroller.core.numerical import (scalar_data_interpolate_scipy,
                                       grid_1d_interpolate,
                                       scalar_data_deduplicate_sort, )
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

def scalars_trackers_process(filename    : Union[str, list, tuple],
                             deduplicate : bool = True) -> NDArray:
  """
  Given a tracker filename process and optionally deduplicate.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename to parse.

  deduplicate : bool = True
    Control whether to deduplicate entries.

  Returns
  -------
  trackers : NDArray
    Parsed tracker file
  """
  trackers = csv_load(filename)

  if deduplicate:
    return scalar_data_deduplicate_sort(trackers,
                                        column=1,
                                        return_indices=False)[:, 1:]

  # drop iteration idx
  return trackers[:, 1:]

def scalars_trackers_interpolate(tracker : NDArray,
                                 T_I     : NDArray,
                                 method  : str="FH",
                                 d       : int=10) -> NDArray:
  """
  Interpolate tracker data.

  Parameters
  ----------
  tracker : NDArray
    Tracker data to interpolate.

  T_I : NDArray
    Target (temporal) grid to interpolate to.

  method : str="FH"
    Must be an element of {"FH". "BT", "linear", "cubic"}

  d : int=10
    For method "FH" control the order.

  Returns
  -------
  Interpolated data

  See also
  --------
  simtroller.core.numerical.scalar_data_interpolate_scipy
  simtroller.core.numerical.grid_1d_interpolate
  """
  if method in {"FH", "BT"}:
    t_T, t_x, t_y, t_z = tracker.T

    t_x_I = grid_1d_interpolate(t_T, t_x, T_I, method=method, d=d)
    t_y_I = grid_1d_interpolate(t_T, t_y, T_I, method=method, d=d)
    t_z_I = grid_1d_interpolate(t_T, t_z, T_I, method=method, d=d)

    return hstack([T_I[:, None],
                   t_x_I[:, None], t_y_I[:, None], t_z_I[:, None]])

  t_T, t_x, t_y, t_z = tracker.T

  t_x_I = scalar_data_interpolate_scipy(t_T, t_x, T_I, kind=method, axis=-1,
                                        assume_sorted=True)
  t_y_I = scalar_data_interpolate_scipy(t_T, t_y, T_I, kind=method, axis=-1,
                                        assume_sorted=True)
  t_z_I = scalar_data_interpolate_scipy(t_T, t_z, T_I, kind=method, axis=-1,
                                        assume_sorted=True)

  return hstack([T_I[:, None],
                 t_x_I[:, None], t_y_I[:, None], t_z_I[:, None]])

def scalars_waveforms_process(filename    : Union[str, list, tuple],
                              deduplicate : bool = True) -> NDArray:
  """
  Given a waveform filename process and optionally deduplicate.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename to parse.

  deduplicate : bool = True
    Control whether to deduplicate entries.

  Returns
  -------
  waveforms : NDArray
    Parsed waveform file
  """
  waveforms = csv_load(filename)

  if deduplicate:
    return scalar_data_deduplicate_sort(waveforms,
                                        column=1,
                                        return_indices=False)[:, 1:]

  # drop iteration idx
  return waveforms[:, 1:]

@lru_cache(maxsize=None, typed=True)
def scalars_waveforms_ix_map(l : int,
                             m : int,
                             is_real_part : bool=True,
                             parsed_offset : bool=True) -> int:
  """
  Convenience function for mapping (l, m) mode to array offset.

  Parameters
  ----------
  l : int
    l number

  m : int
    m number

  is_real_part : bool=True
    Should the index point to the real or imaginary part

  parsed_offset : bool=True
    Control whether this index is applicable for parsed waveform file:
    (2,-2) -> 1 or offset with (2,-2) -> 0.

  Returns
  -------
  Index : int
  """
  prev_l = l - 1
  prev_sz = -3 + 2 * prev_l + prev_l ** 2

  cur_ix = prev_sz + l + m

  if parsed_offset:
    # 1 for time, real part @ 2 * cur_ix + 1 if im required.
    re_im_offset = 0 if is_real_part else 1
    cur_ix = 1 + 2 * cur_ix + re_im_offset

  return cur_ix

def scalars_waveforms_interpolate(waveforms     : NDArray,
                                  T_I           : NDArray,
                                  method        : str="FH",
                                  d             : int=10,
                                  parallel_pool : Any=None) -> NDArray:
  """
  Interpolate waveform data.

  Parameters
  ----------
  waveforms : NDArray
    Waveform data to interpolate.

  T_I : NDArray
    Target (temporal) grid to interpolate to.

  method : str="FH"
    Must be an element of {"FH". "BT", "linear", "cubic"}

  d : int=10
    For method "FH" control the order.

  parallel_pool
    Parallel pool as generated by simtroller.core.parallel_pool_start

  Returns
  -------
  Interpolated data

  See also
  --------
  simtroller.core.numerical.scalar_data_interpolate_scipy
  simtroller.core.numerical.grid_1d_interpolate
  """
  if method in {"FH", "BT"}:
    interpolant = lambda x, f, X, method=method, d=d: grid_1d_interpolate(
      x, f, X, method=method, d=d
    )
  else:
    interpolant = lambda x, f, X, method=method: scalar_data_interpolate_scipy(
      x, f, X, kind=method, axis=-1, assume_sorted=True
    )

  # split
  T, rpsi = waveforms[:, 0], waveforms[:, 1:]

  if parallel_pool is not None:
    parallel_work = [
      parallel_pool_apply_async(
        parallel_pool,
        grid_1d_interpolate,
        (T, rpsi[:, ix], T_I),
        kwds={"method": method, "d": d},
        method="dill")
      for ix in range(rpsi.shape[1])
    ]

    rpsi_I = [job.get() for ix, job in enumerate(parallel_work)]

  else:
    rpsi_I = [interpolant(T, rpsi[:, ix], T_I)
              for ix in range(rpsi.shape[1])]

  return hstack((T_I[:, None], vstack(rpsi_I).T))

def scalars_waveforms_raw_to_complex(raw_psi : NDArray) -> NDArray:
  """
  Convenience function for converting extracted psi data to complex form.

  For use with:
  simtroller.extensions.gra.scalars_waveforms_process

  Parameters
  ----------
  raw_psi : NDArray
    Output of above function.

  Returns
  -------
  complexified form of the above.
  """
  if isinstance(raw_psi, (list, tuple)):
    raw_psi = array(raw_psi)

  return concatenate((
    raw_psi[..., :, 0][..., None],
    raw_psi[..., :, 1::2] + 1j * raw_psi[..., :, 1::2]
  ), axis=-1)

#
# :D
#
