#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Plotting in nd for GR-Athena++ data.
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from functools import lru_cache
from typing import TYPE_CHECKING, Any, Optional, Set

from matplotlib.pyplot import (colorbar, get_cmap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# 3d
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d


# pylint: disable=redefined-builtin
from numpy import (inf, meshgrid)
from numpy.typing import NDArray

# package imports
from simtroller.core.primitives import sequence_generator_flatten
from simtroller.extensions.gra import hdf5_get_vars

###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

# TODO: typing could be improved for input plotting classes

def plot_nd_MeshBlock_structure(ax         : Any,
                                *xyz        : tuple[NDArray,...],
                                color       : Any="k",
                                linewidth   : Any=0.8,
                                plot_kwargs : Optional[dict]=None) -> None:
  """
  Plot 1, 2, or 3d MeshBlock structure.

  Parameters
  ----------
  ax : Any
    matplotlib.axes-like

  *xyz : tuple[NDArray,...]
    Data to parse. Singleton axes ignored during dim reduction.

  color : Any="k"
    Color to use for grid.

  linewidth : Any=0.8
    Width to use for grid.

  plot_kwargs : Optional[dict]=None
    Additional kwargs to ax.plot
  """

  # remove singleton
  xyz_ = tuple(
    gr for gr in xyz if gr.size > 1
  )

  dim = len(xyz_)

  if plot_kwargs is None:
    plot_kwargs = {}

  # passed plot_kwargs should supercede the function defaults
  have_plot_kwargs = {"color": color, "linewidth": linewidth}
  have_plot_kwargs.update(plot_kwargs)

  line_segments = set()

  if dim == 3:
    x, y, z = xyz_

    for x_, y_, z_ in zip(x,y,z):
      x_1, x_2 = x_[0], x_[-1]
      y_1, y_2 = y_[0], y_[-1]
      z_1, z_2 = z_[0], z_[-1]

      for x__ in (x_1, x_2):
        for y__ in (y_1, y_2):
          ln_s = ((x__, x__), (y__, y__), (z_1, z_2))
          if ln_s not in line_segments:
            line_segments.add(ln_s)
            ax.plot3D(*ln_s, **have_plot_kwargs)

      for x__ in (x_1, x_2):
        for z__ in (z_1, z_2):
          ln_s = ((x__, x__), (y_1, y_2), (z__, z__))
          if ln_s not in line_segments:
            line_segments.add(ln_s)
            ax.plot3D(*ln_s, **have_plot_kwargs)

      for y__ in (y_1, y_2):
        for z__ in (z_1, z_2):
          ln_s = ((x_1, x_2), (y__, y__), (z__, z__))
          if ln_s not in line_segments:
            line_segments.add(ln_s)
            ax.plot3D(*ln_s, **have_plot_kwargs)
  elif dim == 2:
    x, y = xyz_

    for x_, y_ in zip(x,y):
      x_1, x_2 = x_[0], x_[-1]
      y_1, y_2 = y_[0], y_[-1]

      for x__ in (x_1, x_2):
        ln_s = ((x__, x__), (y_1, y_2))
        if ln_s not in line_segments:
          line_segments.add(ln_s)
          ax.plot(*ln_s, **have_plot_kwargs)

      for y__ in (y_1, y_2):
        ln_s = ((x_1, x_2), (y__, y__))
        if ln_s not in line_segments:
          line_segments.add(ln_s)
          ax.plot(*ln_s, **have_plot_kwargs)
  elif dim == 1:
    x, = xyz_

    for x_ in x:
      x_1, x_2 = x_[0], x_[-1]

      if x_1 not in line_segments:
        line_segments.add(x_1)
        ax.axvline(x_1, **have_plot_kwargs)

      if x_2 not in line_segments:
        line_segments.add(x_2)
        ax.axvline(x_2, **have_plot_kwargs)
  else:
    raise ValueError(f"dim={dim} cannot be processed")


#
# :D
#
