#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functions for collating waveforms.
"""
###############################################################################
# python imports
from typing import TYPE_CHECKING, Optional
from numpy import ndarray

# pylint: disable=redefined-builtin
from numpy import (zeros, zeros_like)

# package imports
from simtroller.core.numba import JITI
# from simtroller.core.numerical import (grid_1d_interpolate,
#                                        grid_2d_interpolate,
#                                        grid_3d_interpolate,
#                                        ndarray_get_sorted_argmin,
#                                        interval_is_intersection_empty)
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals


# ...

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  # function = JITI(function)
  pass

#
# :D
#
