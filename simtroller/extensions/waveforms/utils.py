#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Waveform utilities
"""
###############################################################################
# python imports
from typing import TYPE_CHECKING, Optional
from numpy import ndarray

# pylint: disable=redefined-builtin
from numpy import (abs, exp, log, piecewise, zeros, zeros_like)

# package imports
from simtroller.core.numba import JITI
# from simtroller.core.numerical import (grid_1d_interpolate,
#                                        grid_2d_interpolate,
#                                        grid_3d_interpolate,
#                                        ndarray_get_sorted_argmin,
#                                        interval_is_intersection_empty)
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals


def map_t_to_u(t, r, M):
  '''
  Map time to retarded time.
  '''
  rstar = r + 2 * M * log(abs(r / (2 * M) - 1))
  return t - rstar

def map_u_to_t(u, r, M):
  '''
  Map retarded time to time.
  '''
  rstar = r + 2 * M * log(abs(r / (2 * M) - 1))
  return u + rstar

def map_q_to_nu(q):
  """
  Map mass ratio q to symmetric mass ratio nu
  """
  return q / ((1. + q) ** 2)

def map_window_exp(x, delta=0):
  '''
  Double side, exponential windowing function on [0,1]
  '''
  return piecewise(x,
          [(x == 0),
            (x > 0) & (x < delta),
            (x >= delta) & (x <= 1.0 - delta),
            (x > 1.0 - delta) & (x < 1),
            (x == 1)],
          [lambda x: 0.0,
            lambda x: exp(-1.0/(1.0 - ((x-delta)/delta)**2) + 1.0),
            lambda x: 1.0,
            lambda x: exp(-1.0/(1.0 - ((x-(1.0-delta))/delta)**2) + 1.0),
            lambda x: 0.0]
          )

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  # function = JITI(function)
  pass

#
# :D
#
