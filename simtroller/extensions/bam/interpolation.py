#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Various interpolation functions for BAM.
"""
###############################################################################
# python imports
from typing import TYPE_CHECKING, Optional
from numpy import ndarray
from scipy.interpolate import (griddata, )

# pylint: disable=redefined-builtin
from numpy import (broadcast_arrays, empty, nan, )

# package imports
from simtroller.core.numba import JITI
from simtroller.core.numerical import (grid_2d_interpolate_to_ordinates,
                                       grid_2d_interpolate_uniform_scipy)
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

def interpolate_2d_Box_in_Box_field(data_dict   : dict,
                                    x_I         : ndarray,
                                    y_I         : ndarray,
                                    d           : int=4,
                                    F_I         : Optional[ndarray]=None,
                                    method      : str="FH") -> ndarray:
  """
  Given a two dimensional field over BAM box in box grids perform interpolation.

  Parameters
  ----------
  data_dict : dict
    Dictionary with structure {'level_label': (XY : ndarray, field : ndarray)}.
    An underlying order is assumed of increasing resolution.

  x_I : ndarray
    Grid to interpolate to.

  y_I : ndarray
    Grid to interpolate to.

  d : int=4
    Order of interpolator (for method "FH).

  F_I : Optional[ndarray]=None
    Optionally pass pre-allocated array for output.

  method : str="FH"
    Method to use. Must be element of {"BT", "FH"}.

  Returns
  -------
  Interpolated function data over target grid.

  Notes
  -----
  Uses simtroller.core.numerical.grid_2d_interpolate
  """
  data_dtype = next(iter(data_dict.values()))[-1].dtype

  if F_I is None:
    F_I = empty((x_I.size, y_I.size), dtype=data_dtype)

  F_I[:] = nan

  xx_I, yy_I = broadcast_arrays(x_I[:, None], y_I[None, :])

  # use logical masking to process with fine then coarser data
  lmask = empty((x_I.size, y_I.size), dtype=bool)
  lmask[:] = True

  # iterate from finest to coarsest
  for data_level in reversed(data_dict.values()):
    XY, field = data_level
    X, Y = XY

    mask_x = (X[0] <= xx_I) & (xx_I <= X[-1])
    mask_y = (Y[0] <= yy_I) & (yy_I <= Y[-1])
    mask = mask_x & mask_y & lmask

    # interpolate to relevant points
    if method in {"linear", "cubic"}:
      # interpolate to relevant points
      XX, YY = broadcast_arrays(X[:, None], Y[None, :])
      idata = griddata(
        (XX.flatten(), YY.flatten()), field.flatten(),
        (xx_I[mask], yy_I[mask]), method=method
      )

    else:
      idata = grid_2d_interpolate_to_ordinates(
        X, Y, field, xx_I[mask], yy_I[mask],
        d=d, method=method)

    F_I[mask] = idata

    # flag processed
    lmask[mask] = False

  return F_I

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  # func = JITI(func)
  pass

#
# :D
#
