#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Various data processing functions for GR-Athena++.
"""
###############################################################################
# python imports
# pylint: disable=unused-import
# from functools import lru_cache
from typing import TYPE_CHECKING, Optional, Set

# pylint: disable=redefined-builtin
from numpy import (array, arange, ndarray, max, min, meshgrid, squeeze, unique,
                   empty_like, float64, int64, inf, empty)
from pandas import read_csv

# package imports
from simtroller.core.primitives import sequence_generator_flatten
from simtroller.core.numerical import ndarray_dim_order_exchange

###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

def coordinates_identify_slice(x=None, y=None, z=None, as_indices=False):
  """
  Convenience function to identify the variety of slice based on input
  x, y, z.

  Parameters
  ----------
  x = None :
    Optional first variable.

  y = None :
    Optional second variable.

  z = None :
    Optional third variable.

  as_indices = False:
    Control whether return is indicial: e.g. (0, 1, 2) or "xyz"

  Returns
  -------
  (ndim, coordinate combination)
  """
  sz_cut = 1  # if we have fewer points than this then we are on a slice

  # extract MeshBlock sampling for each direction
  N_mb_wg_x1 = unique(x).size if x is not None else 0
  N_mb_wg_x2 = unique(y).size if y is not None else 0
  N_mb_wg_x3 = unique(z).size if z is not None else 0

  # infer slice dimensionality
  is_sl_x1 = N_mb_wg_x1 > sz_cut
  is_sl_x2 = N_mb_wg_x2 > sz_cut
  is_sl_x3 = N_mb_wg_x3 > sz_cut

  sl_dim = is_sl_x1 + is_sl_x2 + is_sl_x3 + 0

  if as_indices:
    variety = []
    if is_sl_x1:
      variety.append(0)
    if is_sl_x2:
      variety.append(1)
    if is_sl_x3:
      variety.append(2)
    variety = tuple(variety)

  else:
    variety = is_sl_x1 * "x" + is_sl_x2 * "y" + is_sl_x3 * "z"

  return (sl_dim, variety)

def csv_bam_load_times(filename, return_linenos=True):
  """
  Given BAM csv file structure follows:
  "Time = $DUMP_TIME"
  x y z F
    .
    .
    .
  x y z F

  "Time = $NEXT_TIME"
    .
    .
    .

  Extract the value of each 'Time' tag together with line-number.

  Parameters
  ----------
  filename : str
    Filename to process.

  return_linenos : bool=True
    Control whether the line number is returned.

  Returns
  -------
  Extracted line numbers (optionally) and times.
  """
  ix_T, T = [], []

  with open(filename, "r") as fh:
    for ix, line in enumerate(fh):
      if line.startswith('"Time'):
        line = line.replace(" ", "")
        line = line[6:-2]
        ix_T.append(ix)
        T.append(float64(line))

  if return_linenos:
    return array(ix_T, dtype=int64), array(T)

  return array(T)

def csv_bam_canonicalize(data, return_salient_xyz=True):
  """
  Given BAM data that has been loaded with 'csv_bam_load' then canonicalize for
  'C' ordering. Optionally return relevant slicing coordinates.

  Parameters
  ----------
  data
    BAM data that has been read according to 'csv_bam_load'.

  return_salient_xyz : bool=True
    Control whether relevant slicing coordinates are returned.

  Returns
  -------
  Data with canonicalized 'C' ordering.
  """
  u_x, u_y, u_z = [unique(data[:, ix]) for ix in range(3)]

  sl_dim, variety = coordinates_identify_slice(x=u_x, y=u_y, z=u_z,
                                               as_indices=True)

  salient_xyz = [(u_x, u_y, u_z)[ix] for ix in variety]
  field_slicer = [var.size for var in salient_xyz[::-1]]
  field_F = data[:, -1].reshape(field_slicer)
  field_C = ndarray_dim_order_exchange(field_F, num_skip_dim=0)

  if return_salient_xyz:
    return salient_xyz, field_C

  return field_C

def csv_bam_load(filename, reprocess_to_canonical=True):
  """
  Given BAM data that is saved as CSV load it. Optionally reprocess to
  canonical 'C' ordered form.

  Parameters
  ----------
  filename : str
    Filename to process.

  reprocess_to_canonical : bool=True
    Control whether reprocessing to canonical form is performed according to
    'csv_bam_canonicalize'.

  Returns
  -------
  Processed data
  """
  # use Pandas to process (faster than numpy.loadtxt)
  df = read_csv(filename,
                sep=" ",
                comment='"',
                names=["X", "Y", "Z", "V"],
                dtype=float64,
                skipinitialspace=True)
  data = array(df)

  # now split according to size of each data chunk
  ix_T, T = csv_bam_load_times(filename, return_linenos=True)

  sx_T = ix_T.copy()
  sx_T[1:] -= 2 * arange(1, sx_T.size)
  ex_T = empty_like(sx_T)
  ex_T[:-1] = sx_T[1:]
  ex_T[-1] = data.size

  # slice the data
  rdata = {}
  for sx, ex, cT in zip(sx_T, ex_T, T):
    rdata[cT] = data[sx:ex]

  if reprocess_to_canonical:
    for cT, data in rdata.items():
      xyz, field = csv_bam_canonicalize(data, return_salient_xyz=True)
      rdata[cT] = (xyz, field)

  return rdata

def coordinates_get_dx_extrema(data_dict : dict):
  """
  Given input grid extract min(dx) and max(dx).

  Parameters
  ----------
  data_dict : dict
    Dictionary with structure {'level_label': (XY : ndarray, field : ndarray)}.

  Returns
  -------
  min, max : ndarray
  """
  num_ords = len(next(iter(data_dict.values()))[0])

  mm_dx = empty((num_ords, 2), dtype=float64)
  mm_dx[:, 0] = inf
  mm_dx[:, 1] = -inf

  for fn, data in data_dict.items():
    XYZ, field = data

    for ix, ord in enumerate(XYZ):
      ord_diff = ord[1] - ord[0]
      if ord_diff < mm_dx[ix, 0]:
        mm_dx[ix, 0] = ord_diff
      if ord_diff > mm_dx[ix, 1]:
        mm_dx[ix, 1] = ord_diff

  return mm_dx

#
# :D
#
