"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functionality for BAM.
"""
from simtroller.extensions.bam.data_processing import (
  coordinates_identify_slice,
  csv_bam_canonicalize,
  csv_bam_load,
  csv_bam_load_times,
  coordinates_get_dx_extrema)

from simtroller.extensions.bam.data_processing_scalars import (
  scalars_trackers_process,
  scalars_trackers_interpolate)

from simtroller.extensions.bam.interpolation import (
  interpolate_2d_Box_in_Box_field, )

from simtroller.extensions.bam.plotting_2d import (
  plot_2d_raw,
  plot_2d_box_in_box_structure)

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("data_processing"))
exec(_import_clear("data_processing_scalars"))
exec(_import_clear("interpolation"))
exec(_import_clear("plotting_2d"))
# =============================================================================

#
# :D
#
