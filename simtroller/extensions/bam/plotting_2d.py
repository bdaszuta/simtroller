#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Plotting in 2d for BAM data.
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from functools import lru_cache
from typing import TYPE_CHECKING, Any, Optional, Set

from matplotlib.pyplot import (colorbar, get_cmap)
from mpl_toolkits.axes_grid1 import make_axes_locatable

# pylint: disable=redefined-builtin
from numpy import (inf, meshgrid)
from numpy.typing import NDArray

# package imports
from simtroller.core.primitives import sequence_generator_flatten

###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

# TODO: typing could be improved for input plotting classes

def plot_2d_raw(ax            : Any,
                data_dict     : dict,
                cmap          : str="inferno",
                vmin          : Optional[float]=None,
                vmax          : Optional[float]=None,
                show_colorbar : bool=True,
                plot_kwargs   : Optional[dict]=None,
                lambda_apply  : Optional[Any]=None) -> None:
  """
  Plot 2d raw data.

  Parameters
  ----------
  ax : Any
    matplotlib.axes

  data_dict : dict
    BAM data to use.

  cmap : str="inferno"
    Color map to use.

  vmin : Optional[float]=None
    Minimum value for color map.

  vmax : Optional[float]=None
    Maximum value for color map.

  show_colorbar : bool=True
    Control whether colorbar is added.

  plot_kwargs : Optional[dict]=None
    Additional kwargs to ax.plot

  lambda_apply : Optional[Any]=None
    Optionally apply this function to data prior to plotting
  """
  if lambda_apply is None:
    lambda_apply = lambda v: v


  if plot_kwargs is None:
    plot_kwargs = {}

  selected_cmap = get_cmap(cmap)

  if vmin is None:
    vmin = inf

    for fn, data in data_dict.items():
      xyz, field = data
      fmin = lambda_apply(field).min()
      vmin = fmin if fmin < vmin else vmin

  if vmax is None:
    vmax = -inf

    for fn, data in data_dict.items():
      xyz, field = data
      fmax = lambda_apply(field).max()
      vmax = fmax if fmax > vmax else vmax

  for fn, data in data_dict.items():
    xyz, field = data

    psm = ax.pcolormesh(
      *tuple(sequence_generator_flatten(
        (meshgrid(*xyz, indexing="ij"), lambda_apply(field)))),
      cmap=selected_cmap,
      vmin=vmin,
      vmax=vmax,
      **plot_kwargs
    )

  if show_colorbar:
    # add an axis for colorbar [se #44682146]
    divider_im = make_axes_locatable(ax)
    cax = divider_im.append_axes("right", size="5%", pad=0.02)
    _ = colorbar(psm, ax=ax, cax=cax)

def plot_2d_box_in_box_structure(ax          : Any,
                                 data_dict   : dict,
                                 NGHOST      : int=None,
                                 color       : Any="k",
                                 linewidth   : Any=0.8,
                                 plot_kwargs : Optional[dict]=None) -> None:
  """
  Plot two-dimensional box in box structure.

  Parameters
  ----------
  ax : Any
    matplotlib.axes

  data_dict : dict
    BAM data to use.

  NGHOST : int=None
    Number of ghost zones.

  color : Any="k"
    Color to use for grid.

  linewidth : Any=0.8
    Width to use for grid.

  plot_kwargs : Optional[dict]=None
    Additional kwargs to ax.plot
  """
  if plot_kwargs is None:
    plot_kwargs = {}

  plot_kwargs.update({"color": color, "linewidth": linewidth})

  for fn, data in data_dict.items():
    xyz, field = data

    X, Y = xyz

    x_1, x_2 = X.min(), X.max()
    y_1, y_2 = Y.min(), Y.max()

    ax.plot((x_1, x_1), (y_1, y_2), **plot_kwargs)
    ax.plot((x_2, x_2), (y_1, y_2), **plot_kwargs)
    ax.plot((x_1, x_2), (y_1, y_1), **plot_kwargs)
    ax.plot((x_1, x_2), (y_2, y_2), **plot_kwargs)


#
# :D
#
