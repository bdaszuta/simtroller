#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Data processing for BAM related to scalar output
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from typing import TYPE_CHECKING, Optional, Set, Union

# pylint: disable=redefined-builtin
from numpy import (abs, argmin, hstack, roll, inf, isfinite)
from numpy.typing import NDArray

# package imports
from simtroller.core.io import (csv_load, )
from simtroller.core.numerical import (scalar_data_deduplicate_sort, )
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals

def scalars_trackers_process(filename    : Union[str, list, tuple],
                             deduplicate : bool=True,
                             T_cut       : float=inf) -> NDArray:
  """
  Given a tracker filename process and optionally deduplicate.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename to parse.

  deduplicate : bool = True
    Control whether to deduplicate entries.

  T_cut : float=inf
    Control whether output should be cut after some time.

  Returns
  -------
  trackers : NDArray
    Parsed tracker file
  """
  trackers = csv_load(filename, loadtxt_kwargs={"skiprows": 1})

  if deduplicate:
    trackers = scalar_data_deduplicate_sort(trackers,
                                            column=trackers.shape[1] - 1,
                                            return_indices=False)

  # roll time to first column
  trackers = roll(trackers, 1, axis=1)

  if isfinite(T_cut):
    ix_T_cut = argmin(abs(T_cut - trackers[:,0]))
    trackers = trackers[:(ix_T_cut + 1), ...]

  return trackers

def scalars_trackers_interpolate(tracker : NDArray,
                                 T_I     : NDArray,
                                 method  : str="FH",
                                 d       : int=10) -> NDArray:
  """
  Interpolate tracker data. Assumes BAM output structure for two trackers.

  Parameters
  ----------
  tracker : NDArray
    Tracker data to interpolate.

  T_I : NDArray
    Target (temporal) grid to interpolate to.

  method  : str="FH"
    Must be an element of {"FH". "BT", "linear", "cubic"}

  d : int=10
    For method "FH" control the order.

  Returns
  -------
  Interpolated data

  See also
  --------
  simtroller.core.numerical.scalar_data_interpolate_scipy
  simtroller.core.numerical.grid_1d_interpolate
  simtroller.extensions.bam.scalars_trackers_process
  """
  # just use the GRA method
  from simtroller.extensions.gra import (scalars_trackers_interpolate, )

  # assumes BAM output for two trackers
  T = tracker[:, 0]
  tracker_A = tracker[:, 1:4]
  tracker_B = tracker[:, 4:7]

  res_A = scalars_trackers_interpolate(hstack((T[:, None], tracker_A)),
    T_I, method=method, d=d)

  res_B = scalars_trackers_interpolate(hstack((T[:, None], tracker_B)),
    T_I, method=method, d=d)

  return res_A, res_B

#
# :D
#
