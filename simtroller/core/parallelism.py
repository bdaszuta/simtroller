#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Perform execution in parallel using multiprocessing.
"""
###############################################################################
# python imports
import multiprocessing as _mp

from simtroller.core.serialization import (object_serialize,
                                           object_serialized_call)
from simtroller.core.shell.interact import shell_run_script
###############################################################################

def parallel_pool_start(num_processes=None):
  """
  Spin up a pool of processes for parallelism.

  Parameters
  ----------
  num_processes = None
    Number of processes to spawn. If `None` then cpu_count // 2 is utilized.

  Returns
  -------
  multiproccessing.Pool
  """
  if num_processes is None:
    num_processes = _mp.cpu_count() // 2
  return _mp.Pool(processes=num_processes)

def parallel_pool_apply_async(parallel_pool,
                              function,
                              args,
                              kwds=None,
                              method="pickle"):
  """
  Apply function using a parallel pool asynchronously. Internal serialization
  can be controlled. This is useful for lambda functions that do not work
  with pickle.

  Parameters
  ----------
  parallel_pool
    Parallel pool to use.

  function
    Function to apply.

  args
    Sequence of arguments to supply to function.

  kwds=None
    Dictionary of kwargs to supply to function.

  method="pickle"
    Method to use for serialization.
    Must be an element of {None, "pickle", "dill"}.

  Returns
  -------
  multiprocessing.pool.ApplyResult
  """
  if kwds is None:
    kwds = {}

  if method is None or method == "pickle":
    return parallel_pool.apply_async(function, args=args, kwds=kwds)

  if method == "dill":
    to_serialize = (function, args, kwds)
    payload = object_serialize(to_serialize, method="dill")

    return parallel_pool.apply_async(object_serialized_call, (payload,),
                                     kwds={"method": "dill"})

  raise ValueError(f"Unknown 'method' {method}.")

def parallel_pool_shell_run_scripts_async(parallel_pool,
                                          scripts,
                                          script_args=None,
                                          blocking=True,
                                          shell="bash",
                                          method="pickle"):
  """
  Convenience function to parallelize asynchronous script execution.

  Parameters
  ----------
  parallel_pool
    Parallel pool to use.

  scripts
    Sequence of scripts to execute.

  script_args
    An optional string or sequence of strings (must be same len as scripts) to
    pass as arguments during execution.

  blocking=True
    Control whether result is waited upon.

  method="pickle"
    Method to use for serialization.
    Must be an element of {None, "pickle", "dill"}.

  Returns
  -------
  Sequence of futures or result of script execution.

  See also
  --------
  simtroller.core.parallel_pool_apply_async
  simtroller.core.shell.shell_run_script
  """
  if script_args is None or isinstance(script_args, str):
    par_futures = [
      parallel_pool_apply_async(parallel_pool,
                                shell_run_script,
                                (script, ),
                                kwds={"script_args": script_args},
                                method=method)
      for script in scripts
    ]
  else:
    par_futures = [
      parallel_pool_apply_async(parallel_pool,
                                shell_run_script,
                                (script, ),
                                kwds={"script_args": script_args[ix_scr]},
                                method=method)
      for ix_scr, script in enumerate(scripts)
    ]

  if blocking:
    return [par_future.get() for par_future in par_futures]

  return par_futures

#
# :D
#
