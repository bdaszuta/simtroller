#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Better serialization (i.e. for lambda functions)
"""
###############################################################################
# python imports
import pickle as _pi
from typing import (Any, TYPE_CHECKING, Union, # pylint: disable=unused-import
                    List, Sequence)
import dill as _di
###############################################################################

def object_serialize(object_in, method="dill"):
  """
  Serialize an object.

  Parameters
  ----------
  object_in
    Object to serialize.

  method = "dill"
    Method to use. Must be an element of {"dill", "pickle"}.

  Returns
  -------
  Serialized object.
  """
  if method == "dill":
    return _di.dumps(object_in)

  if method == "pickle":
    return _pi.dumps(object_in)

  raise ValueError(f"The method {method} is unknown.")

def object_deserialize(object_in, method="dill"):
  """
  Deserialize an object.

  Parameters
  ----------
  object_in
    Object to deserialize.

  method = "dill"
    Method to use. Must be an element of {"dill", "pickle"}.

  Returns
  -------
  Deserialized object.
  """
  if method == "dill":
    return _di.loads(object_in)

  if method == "pickle":
    return _pi.loads(object_in)

  raise ValueError(f"The method {method} is unknown.")

def object_serialized_call(serialized : bytes, method : str = "dill") -> Any:
  """
  Given a serialized object deserialize and call.

  Parameters
  ----------
  serialized : bytes
    Serialized function.

    Should be of the form:
      (function_handle, args : Sequence, kwds : dict)

  method : str = "dill"
    Serialization library used.

  Returns
  -------
  Result of function call.
  """
  fun, args, kwargs = object_deserialize(serialized, method=method)
  return fun(*args, **kwargs)

#
# :D
#
