#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Utilities for numba library.
"""
###############################################################################
# python imports
from functools import partial
from typing import Any, TYPE_CHECKING, Union # pylint: disable=unused-import
from numba import (jit, )

###############################################################################
# numba settings and convenience function
###############################################################################

# keyword arguments that are fed to each JIT decorator.
SETTINGS_JITI = {'nopython': True,
                 'nogil': True,
                 'cache': True,
                 'inline': 'always',
                 'error_model': 'numpy',
                 'boundscheck': False}

SETTINGS_JIT = {'nopython': True,
                'nogil': True,
                'cache': True,
                'inline': 'never',
                'error_model': 'numpy',
                'boundscheck': False}

SETTINGS_JITI_NC = {'nopython': True,
                    'nogil': True,
                    'cache': False,
                    'inline': 'always',
                    'error_model': 'numpy',
                    'boundscheck': False}

SETTINGS_JIT_NC = {'nopython': True,
                   'nogil': True,
                   'cache': False,
                   'inline': 'never',
                   'error_model': 'numpy',
                   'boundscheck': False}

JITI = partial(jit, **SETTINGS_JITI)
JITI_NC = partial(jit, **SETTINGS_JITI_NC)
JIT = partial(jit, **SETTINGS_JIT)
JIT_NC = partial(jit, **SETTINGS_JIT_NC)

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  # fcn = JITI(fcn)
  pass

#
# :D
#
