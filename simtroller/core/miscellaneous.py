#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Miscellanous functions that do not fit elsewhere.
"""
###############################################################################
# python imports
import time as _t
import hashlib as _hl
###############################################################################

def time_get_current(as_tag=True):
  '''
  Get current time, optionally generate as tag.
  '''
  current_time = _t.time()
  if as_tag:
    current_time = int(current_time)
  return current_time

def time_sleep(seconds):
  """
  Delay execution for a given number of seconds
  """
  _t.sleep(seconds)

def hash_generate(obj):
  '''
  Hash that maintains consistency across sessions without env. flags.

  If input object is a flat dictionary then keys are sorted;
  key/val pairs are converted to string and finally hashed.
  '''
  if isinstance(obj, dict):
    to_hash = ''
    keys = list(obj.keys())
    str_keys = [str(k) for k in keys]
    skeys = [k for _, k in sorted(zip(str_keys, keys))]

    for srtk in skeys:
      to_hash += str(srtk) + str(obj[srtk])

    obj = to_hash

  return _hl.sha256(str(obj).encode('ascii')).hexdigest()


def _import_clear(name):
  to_del_cmd = "import sys as _sys\n"
  to_del_cmd += "if not hasattr(_sys, 'ps1'):\n"
  to_del_cmd += "  {name} = False\n"
  to_del_cmd += "del {name}"

  return to_del_cmd.format(name=name)

#
# :D
#
