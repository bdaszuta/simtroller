#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Manipulation of arrays
"""
###############################################################################
# python imports
from typing import Any, TYPE_CHECKING, Union # pylint: disable=unused-import
import numpy as _np

# package imports
from simtroller.core.primitives.prim_common import sequence_generator_flatten
###############################################################################

def ndarray_dim_order_exchange(arr=None, num_skip_dim=0):
  """
  Interchange underlying ordering of input array.

  Useful for quick switch between C and Fortran order.

  Parameters
  ----------
  arr
    Numpy array to change ordering of.

  num_skip_dim = 0
    Number of dimensions to skip (reordering performed on remaining).

  Returns
  -------
  Appropriately transposed array.

  Usage
  -----
  >>> from numpy import empty
  >>> from simtroller.core.numerical import ndarray_dim_order_exchange
  >>> A = empty((4, 33, 3, 4, 5))
  >>> ndarray_dim_order_exchange(A).shape
      (5, 4, 3, 33, 4)
  >>> ndarray_dim_order_exchange(A, num_skip_dim=2).shape
      (4, 33, 5, 4, 3)
  """
  if arr.ndim < num_skip_dim:
    raise ValueError(f"num_skip_dim = {num_skip_dim} > arr.ndim = {arr.ndim}")

  transpose_indices = (range(num_skip_dim),
                       reversed(range(num_skip_dim, arr.ndim)))

  transpose_indices = tuple(
    tuple(element) for element in transpose_indices
  )
  tar_indices = tuple(sequence_generator_flatten(transpose_indices))

  return _np.transpose(arr, tar_indices)

#
# :D
#
