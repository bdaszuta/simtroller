"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functionality related to numerical methods goes here.
"""
from simtroller.core.numerical.array_manipulation import (
  ndarray_dim_order_exchange
)

from simtroller.core.numerical.find_extrema import (
  find_local_extrema_1d,
  find_local_extrema_2d
)

from simtroller.core.numerical.interpolation import (
  scalar_data_interpolate_scipy,
  grid_1d_interpolate_uniform_scipy,
  grid_2d_interpolate_uniform_scipy,
  grid_3d_interpolate_uniform_scipy,
  grid_1d_interpolate_scipy,
  grid_2d_interpolate_scipy,
  grid_3d_interpolate_scipy,
  grid_1d_interpolate,
  grid_2d_interpolate,
  grid_3d_interpolate,
  grid_2d_interpolate_to_ordinates,
  grid_3d_interpolate_to_ordinates
)

from simtroller.core.numerical.miscellaneous import (
  ndarray_get_sorted_argmin,
  interval_is_intersection_empty,
  scalar_data_deduplicate_sort
)

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("array_manipulation"))
exec(_import_clear("find_extrema"))
exec(_import_clear("interpolation"))
exec(_import_clear("miscellaneous"))
# =============================================================================

#
# :D
#
