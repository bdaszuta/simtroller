#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Extrema utilities.
"""
###############################################################################
# python imports
from typing import TYPE_CHECKING, Tuple
from numpy.typing import (NDArray, )

# package imports
from simtroller.core.numba import JITI
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=redefined-builtin
# pylint: disable=too-many-arguments
# pylint: disable=too-many-branches
# pylint: disable=too-many-locals
# pylint: disable=too-many-nested-blocks

def find_local_extrema_1d(x_I : NDArray,
                          F_I : NDArray,
                          x_0 : float,
                          r_0 : float,
                          extrema_type : int=+1) -> int:
  """
  Given a contiguous block of 1d data find indices of local extrema bounded by
  an interval.

  Parameters
  ----------
  x_I : NDArray
    x-ordinate for sampled F_I.

  F_I : NDArray
    Sampled function.

  x_0 : float
    x-ordinate of bracketing interval.

  r_0 : float
    Radius of bracketing interval.

  extrema_type : int=+1
    Control whether minima (-1) or maxima (+1) is searched for.

  Returns
  -------
  ix_max : int
  """

  # internally we always look for a maximum; this is done by masking
  # with extrema_type
  old_max = (extrema_type * F_I[0])

  ix_max = 0
  for ix, x in enumerate(x_I):
    if (x - x_0) ** 2 < r_0 ** 2:
      if (extrema_type * F_I[ix]) > old_max:
        old_max = extrema_type * F_I[ix]
        ix_max = ix

  return ix_max

def find_local_extrema_2d(x_I : NDArray,
                          y_I : NDArray,
                          F_I : NDArray,
                          x_0 : float,
                          y_0 : float,
                          r_0 : float,
                          extrema_type : int=+1) -> Tuple[int, int]:
  """
  Given a contiguous block of 2d data find indices of local extrema bounded by
  a disc.

  Parameters
  ----------
  x_I : NDArray
    x-ordinate for sampled F_I.

  y_I : NDArray
    y-ordinate for sampled F_I.

  F_I : NDArray
    Sampled function.

  x_0 : float
    x-ordinate of bracketing disc.

  y_0 : float
    y-ordinate of bracketing disc.

  r_0 : float
    Radius of bracketing disc.

  extrema_type : int=+1
    Control whether minima (-1) or maxima (+1) is searched for.

  Returns
  -------
  (ix_max, jx_max) : Tuple[int, int]
  """

  # internally we always look for a maximum; this is done by masking
  # with extrema_type
  old_max = (extrema_type * F_I[0, 0])

  # xx_I, yy_I = np.broadcast_arrays(x_I[:, None], y_I[None :])
  # disc_mask = (xx_I - x_0) ** 2 + (yy_I - y_0) ** 2 < r_0 ** 2
  # ix_loc = np.argmax(F_I[disc_mask])
  # return np.array([xx_I[disc_mask][ix_loc], yy_I[disc_mask][ix_loc]])

  ix_max, jx_max = 0, 0
  for jx, y in enumerate(y_I):
    for ix, x in enumerate(x_I):
      if (x - x_0) ** 2 + (y - y_0) ** 2 < r_0 ** 2:
        if (extrema_type * F_I[ix, jx]) > old_max:
          old_max = extrema_type * F_I[ix, jx]
          ix_max, jx_max = ix, jx

  return ix_max, jx_max

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  find_local_extrema_1d = JITI(find_local_extrema_1d)
  find_local_extrema_2d = JITI(find_local_extrema_2d)

#
# :D
#
