#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Various interpolation functions.
"""
###############################################################################
# python imports
from typing import TYPE_CHECKING, Optional, Tuple
from numpy import (broadcast_arrays, empty, float64,
                   linspace, ndarray, power, unique, zeros_like, zeros)

from scipy.interpolate import (griddata, interp1d, interpn)

# package imports
from simtroller.core.numba import JITI
###############################################################################

# pylint: disable=invalid-name
# pylint: disable=redefined-builtin
# pylint: disable=too-many-arguments
# pylint: disable=too-many-branches
# pylint: disable=too-many-locals
# pylint: disable=too-many-nested-blocks

def scalar_data_interpolate_scipy(xx, yy, X,
                                  axis=-1,
                                  kind='cubic',
                                  assume_sorted=True,
                                  **interp1d_options):
  """
  Wrapper for scipy.interpolate.interp1d.

  Parameters
  ----------
  xx
    Sampled independent variable.

  yy
    Sampled dependent variable.

  X
    Target interpolated grid.

  axis=-1
    Axis to perform interpolation over.

  assume_sorted=True
    Control whether it can be assumed that input data is sorted.

  **interp1d_options
    kwargs to pass onto interp1d.

  Returns
  -------
  Interpolated data
  """
  interp = interp1d(xx, yy, kind=kind, assume_sorted=assume_sorted,
    axis=axis, **interp1d_options)
  return interp(X)

def grid_1d_interpolate_uniform_scipy(
  x         : ndarray,
  grid_data : ndarray,
  N_x       : int=256,
  ab_x      : Optional[Tuple]=None,
  method    : str="cubic") -> Tuple[ndarray, ndarray]:
  """
  Interpolate one dimensional data to a uniform grid.

  Parameters
  ----------
  x : ndarray
    Input grid associated with grid data.

  grid_data : ndarray
    Grid data to interpolate.

  N_x : int=256
    Number of points to use for interpolation.

  ab_x : Optional[Tuple]=None
    End-points of interpolated interval. Taken as (min(x), max(x)) if
    unspecified.

  method : str="cubic"
    Method to use. Must be an element of {"linear", "cubic"}.

  Returns
  -------
  (x_I, gd_I) : Tuple[ndarray, ndarray]

  Notes
  -----
  Internally this uses scipy.interpolate.griddata
  """
  x_F_nonunique = x.flatten()

  x_F, x_F_unique_ix = unique(x_F_nonunique, return_index=True)
  gd_F = (grid_data.flatten())[x_F_unique_ix]

  if ab_x is None:
    a_x, b_x = x_F.min(), x_F.max()
  elif isinstance(ab_x, tuple) and len(ab_x) == 1:
    a_x, b_x = ab_x[0], ab_x[0]
  else:
    a_x, b_x = ab_x

  x_I = linspace(a_x, b_x, num=N_x)

  if method in {"cubic", "linear"}:
    i_data = griddata((x_F, ), gd_F, (x_I, ), method=method)

    return x_I, i_data

  raise NotImplementedError(f"method={method} unknown.")

def grid_2d_interpolate_uniform_scipy(
  x         : ndarray,
  y         : ndarray,
  grid_data : ndarray,
  N_x       : int=256,
  N_y       : int=256,
  ab_x      : Optional[Tuple]=None,
  ab_y      : Optional[Tuple]=None,
  method    : str="cubic") -> Tuple[ndarray, ndarray, ndarray]:
  """
  Interpolate two dimensional data to a uniform grid.

  Parameters
  ----------
  x : ndarray
    Input grid associated with grid data.

  y : ndarray
    Input grid associated with grid data.

  grid_data : ndarray
    Grid data to interpolate.

  N_x : int=256
    Number of points to use for interpolation.

  N_y : int=256
    Number of points to use for interpolation.

  ab_x : Optional[Tuple]=None
    End-points of interpolated interval. Taken as (min(x), max(x)) if
    unspecified.

  ab_y : Optional[Tuple]=None
    End-points of interpolated interval. Taken as (min(y), max(y)) if
    unspecified.

  method : str="cubic"
    Method to use. Must be an element of {"linear", "cubic"}.

  Returns
  -------
  (x_I, y_I, gd_I) : Tuple[ndarray, ndarray, ndarray]

  Notes
  -----
  Internally this uses scipy.interpolate.griddata
  """
  x_E, y_E = broadcast_arrays(x[:, :, None], y[:, None, :])
  x_F, y_F = x_E.flatten(), y_E.flatten()

  grid_data_F = grid_data.flatten()

  if ab_x is None:
    a_x, b_x = x_F.min(), x_F.max()
  elif isinstance(ab_x, tuple) and len(ab_x) == 1:
    a_x, b_x = ab_x[0], ab_x[0]
  else:
    a_x, b_x = ab_x

  if ab_y is None:
    a_y, b_y = y_F.min(), y_F.max()
  elif isinstance(ab_y, tuple) and len(ab_y) == 1:
    a_y, b_y = ab_y[0], ab_y[0]
  else:
    a_y, b_y = ab_y

  x_I, y_I = linspace(a_x, b_x, num=N_x), linspace(a_y, b_y, num=N_y)
  xx_I, yy_I = broadcast_arrays(x_I[:,None], y_I[None,:])
  xx_I_F, yy_I_F = xx_I.flatten(), yy_I.flatten()

  if method in {"cubic", "linear"}:
    i_data = griddata((x_F, y_F), grid_data_F, (xx_I_F, yy_I_F),
                      method=method).reshape((N_x, N_y))

    return xx_I, yy_I, i_data

  raise NotImplementedError(f"method={method} unknown.")

def grid_3d_interpolate_uniform_scipy(
  x         : ndarray,
  y         : ndarray,
  z         : ndarray,
  grid_data : ndarray,
  N_x       : int=256,
  N_y       : int=256,
  N_z       : int=256,
  ab_x      : Optional[Tuple]=None,
  ab_y      : Optional[Tuple]=None,
  ab_z      : Optional[Tuple]=None,
  method    : str="cubic") -> Tuple[ndarray, ndarray, ndarray,ndarray]:
  """
  Interpolate two dimensional data to a uniform grid.

  Parameters
  ----------
  x : ndarray
    Input grid associated with grid data.

  y : ndarray
    Input grid associated with grid data.

  z : ndarray
    Input grid associated with grid data.

  grid_data : ndarray
    Grid data to interpolate.

  N_x : int=256
    Number of points to use for interpolation.

  N_y : int=256
    Number of points to use for interpolation.

  N_z : int=256
    Number of points to use for interpolation.

  ab_x : Optional[Tuple]=None
    End-points of interpolated interval. Taken as (min(x), max(x)) if
    unspecified.

  ab_y : Optional[Tuple]=None
    End-points of interpolated interval. Taken as (min(y), max(y)) if
    unspecified.

  ab_z : Optional[Tuple]=None
    End-points of interpolated interval. Taken as (min(z), max(z)) if
    unspecified.

  method : str="cubic"
    Method to use. Must be an element of {"linear", "cubic"}.

  Returns
  -------
  (x_I, y_I, z_I, gd_I) : Tuple[ndarray, ndarray, ndarray, ndarray]

  Notes
  -----
  Internally this uses scipy.interpolate.griddata
  """
  x_E, y_E, z_E = broadcast_arrays(x[:, :, None, None],
                                   y[:, None, :, None],
                                   z[:, None, None, :])
  x_F, y_F, z_F = x_E.flatten(), y_E.flatten(), z_E.flatten()

  grid_data_F = grid_data.flatten()

  if ab_x is None:
    a_x, b_x = x_F.min(), x_F.max()
  elif isinstance(ab_x, tuple) and len(ab_x) == 1:
    a_x, b_x = ab_x[0], ab_x[0]
  else:
    a_x, b_x = ab_x

  if ab_y is None:
    a_y, b_y = y_F.min(), y_F.max()
  elif isinstance(ab_y, tuple) and len(ab_y) == 1:
    a_y, b_y = ab_y[0], ab_y[0]
  else:
    a_y, b_y = ab_y

  if ab_z is None:
    a_z, b_z = z_F.min(), z_F.max()
  elif isinstance(ab_z, tuple) and len(ab_z) == 1:
    a_z, b_z = ab_z[0], ab_z[0]
  else:
    a_z, b_z = ab_z

  x_I = linspace(a_x, b_x, num=N_x)
  y_I = linspace(a_y, b_y, num=N_y)
  z_I = linspace(a_z, b_z, num=N_z)

  xx_I, yy_I, zz_I = broadcast_arrays(x_I[:, None, None],
                                      y_I[None, :, None],
                                      z_I[None, None, :])

  xx_I_F, yy_I_F, zz_I_F = xx_I.flatten(), yy_I.flatten(), zz_I.flatten()

  if method in {"cubic", "linear"}:
    i_data = griddata((x_F, y_F, z_F), grid_data_F, (xx_I_F, yy_I_F, zz_I_F),
                      method=method).reshape((N_x, N_y, N_z))

    return xx_I, yy_I, zz_I, i_data

  raise NotImplementedError(f"method={method} unknown.")

# Alternative input specification ---------------------------------------------

def grid_1d_interpolate_scipy(
  X            : ndarray,
  grid_data    : ndarray,
  X_I          : ndarray,
  method       : str="linear",
  method_scipy : str="interpn") -> ndarray:
  """
  Interpolate one dimensional data to a provided grid.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  grid_data : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  method : str="linear"
    Method to use. Depends on "method_scipy".
    For "interpn" must be an element of {"linear", "nearest"}.
    For "griddata" must be an element of {"linear", "cubic", "nearest"}.

  method_scipy : str="interpn"
    Scipy method to use. Must be an element of {"interpn", "griddata"}.

  Returns
  -------
  gd_I : ndarray
    Result of interpolation.

  Notes
  -----
  Internally this uses scipy.interpolate
  """
  xx_I_F = X_I.flatten()

  N_x = X_I.size

  if method_scipy == "interpn":
    if method not in {"linear", "nearest"}:
      raise NotImplementedError(f"method={method} unknown.")

    I_F = interpn((X, ), grid_data, (xx_I_F, ),
                  method=method).reshape((N_x, ))

    return I_F

  elif method_scipy == "griddata":
    if method not in {"linear", "cubic", "nearest"}:
      raise NotImplementedError(f"method={method} unknown.")

    x_F = X.flatten()

    grid_data_F = grid_data.flatten()

    i_data = griddata((x_F, ), grid_data_F, (xx_I_F, ),
                      method=method).reshape((N_x, ))

    return i_data

  else:
    raise NotImplementedError(f"method_scipy={method_scipy} unknown.")

def grid_2d_interpolate_scipy(
  X            : ndarray,
  Y            : ndarray,
  grid_data    : ndarray,
  X_I          : ndarray,
  Y_I          : ndarray,
  method       : str="linear",
  method_scipy : str="interpn") -> ndarray:
  """
  Interpolate two dimensional data to a provided grid.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  Y : ndarray
    Input grid associated with grid data.

  grid_data : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  Y_I : ndarray
    Grid to interpolate over.

  method : str="linear"
    Method to use. Depends on "method_scipy".
    For "interpn" must be an element of {"linear", "nearest"}.
    For "griddata" must be an element of {"linear", "cubic", "nearest"}.

  method_scipy : str="interpn"
    Scipy method to use. Must be an element of {"interpn", "griddata"}.

  Returns
  -------
  gd_I : ndarray
    Result of interpolation.

  Notes
  -----
  Internally this uses scipy.interpolate
  """
  xx_I, yy_I = broadcast_arrays(X_I[:, None], Y_I[None, :])

  xx_I_F = xx_I.flatten()
  yy_I_F = yy_I.flatten()

  N_x = X_I.size
  N_y = Y_I.size

  if method_scipy == "interpn":
    if method not in {"linear", "nearest"}:
      raise NotImplementedError(f"method={method} unknown.")

    I_F = interpn((X, Y), grid_data, (xx_I_F, yy_I_F),
                  method=method).reshape((N_x, N_y))

    return I_F

  elif method_scipy == "griddata":
    if method not in {"linear", "cubic", "nearest"}:
      raise NotImplementedError(f"method={method} unknown.")

    x_E, y_E = broadcast_arrays(X[:, None], Y[None, :])
    x_F, y_F = x_E.flatten(), y_E.flatten()

    grid_data_F = grid_data.flatten()

    i_data = griddata((x_F, y_F), grid_data_F, (xx_I_F, yy_I_F),
                      method=method).reshape((N_x, N_y))

    return i_data

  else:
    raise NotImplementedError(f"method_scipy={method_scipy} unknown.")

def grid_3d_interpolate_scipy(
  X            : ndarray,
  Y            : ndarray,
  Z            : ndarray,
  grid_data    : ndarray,
  X_I          : ndarray,
  Y_I          : ndarray,
  Z_I          : ndarray,
  method       : str="linear",
  method_scipy : str="interpn") -> ndarray:
  """
  Interpolate three dimensional data to a provided grid.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  Y : ndarray
    Input grid associated with grid data.

  Z : ndarray
    Input grid associated with grid data.

  grid_data : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  Y_I : ndarray
    Grid to interpolate over.

  Z_I : ndarray
    Grid to interpolate over.

  method : str="linear"
    Method to use. Depends on "method_scipy".
    For "interpn" must be an element of {"linear", "nearest"}.
    For "griddata" must be an element of {"linear", "cubic", "nearest"}.

  method_scipy : str="interpn"
    Scipy method to use. Must be an element of {"interpn", "griddata"}.

  Returns
  -------
  gd_I : ndarray
    Result of interpolation.

  Notes
  -----
  Internally this uses scipy.interpolate
  """
  xx_I, yy_I, zz_I = broadcast_arrays(X_I[:, None, None],
                                      Y_I[None, :, None],
                                      Z_I[None, None, :])

  xx_I_F = xx_I.flatten()
  yy_I_F = yy_I.flatten()
  zz_I_F = zz_I.flatten()

  N_x = X_I.size
  N_y = Y_I.size
  N_z = Z_I.size

  if method_scipy == "interpn":
    if method not in {"linear", "nearest"}:
      raise NotImplementedError(f"method={method} unknown.")

    I_F = interpn((X, Y, Z), grid_data, (xx_I_F, yy_I_F, zz_I_F),
                  method=method).reshape((N_x, N_y, N_z))

    return I_F

  elif method_scipy == "griddata":
    if method not in {"linear", "cubic", "nearest"}:
      raise NotImplementedError(f"method={method} unknown.")

    x_E, y_E, z_E = broadcast_arrays(X[:, None, None],
                                    Y[None, :, None],
                                    Z[None, None, :])
    x_F, y_F, z_F = x_E.flatten(), y_E.flatten(), z_E.flatten()

    grid_data_F = grid_data.flatten()

    i_data = griddata((x_F, y_F, z_F), grid_data_F, (xx_I_F, yy_I_F, zz_I_F),
                      method=method).reshape((N_x, N_y, N_z))

    return i_data

  else:
    raise NotImplementedError(f"method_scipy={method_scipy} unknown.")

# Barycentric impl. details ---------------------------------------------------
def _set_J_FH(alpha, d, n):
  return range(max(alpha - d, 0), min(alpha, n - d) + 1)

def _wei_bary_FH(k, d, n, X):
  wei = 0
  for i in _set_J_FH(k, d, n):
    wei_pr = power(-1, i)
    for j in range(i, i + d + 1):
      cur_val = 1 / (X[k] - X[j]) if j != k else 1
      wei_pr *= cur_val

    wei += wei_pr
  return wei

def _wei_bary_BT(k, n, X):
  wei = 1
  for j in range(n + 1):
    wei *= (X[k] - X[j]) if j != k else 1
  return 1 / wei

def grid_1d_interpolate(X      : ndarray,
                        F      : ndarray,
                        X_I    : ndarray,
                        d      : int=4,
                        F_I    : Optional[ndarray]=None,
                        method : str="FH") -> ndarray:
  """
  Interpolate one dimensional data. Assumes input grid values are in ascending
  order.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  F : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  d : int = 4
    Order of interpolation (for use with "FH" method).

  F_I : Optional[ndarray]=None
    Optionally pass output target.

  method : str="FH"
    Method to use. For "BT" [1] is utilized whereas "FH" uses [2].

  Returns
  ------
  F_I : ndarray
    Result of interpolation.

  Notes
  -----
  Weights are dynamically computed.
  Function uses JIT.

  References
  ----------
  [1]: Berrut, J.P. and Trefethen, L.N., 2004.
       Barycentric lagrange interpolation. SIAM review, 46(3), pp.501-517.
  [2]: Floater, M.S. and Hormann, K., 2007.
       Barycentric rational interpolation with no poles and high rates of
       approximation. Numerische Mathematik, 107(2), pp.315-331.
  """
  n = X.size - 1

  # prepare weights
  wei = empty((n + 1), dtype=float64)

  if method == "FH":
    for k in range(0, n + 1):
      wei[k] = _wei_bary_FH(k, d, n, X)
  elif method == "BT":
    for k in range(0, n + 1):
      wei[k] = _wei_bary_BT(k, n, X)
  else:
    raise ValueError("method unknown.")

  # wei = [_wei_bary_FH(k, d, n, X) for k in range(0, n + 1)]

  if F_I is None:
    F_I = zeros_like(X_I)

  for I in range(0, X_I.size):
    num, den = 0, 0
    for i in range(0, n + 1):
      diff = X_I[I] - X[i]
      # slight perturbation is fine but exact float eq. causes problems
      if diff != 0:
        fac = wei[i] / diff
        num += fac * F[i]
        den += fac
      else:
        num = F[i]
        den = 1
        break
    F_I[I] = num / den
  return F_I

def grid_2d_interpolate(X      : ndarray,
                        Y      : ndarray,
                        F      : ndarray,
                        X_I    : ndarray,
                        Y_I    : ndarray,
                        d      : int=4,
                        F_I    : Optional[ndarray]=None,
                        method : str="FH") -> ndarray:
  """
  Interpolate two dimensional data. Assumes input grid values are in ascending
  order.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  Y : ndarray
    Input grid associated with grid data.

  F : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  Y_I : ndarray
    Grid to interpolate over.

  d : int = 4
    Order of interpolation (for use with "FH" method).

  F_I : Optional[ndarray]=None
    Optionally pass output target.

  method : str="FH"
    Method to use. For "BT" [1] is utilized whereas "FH" uses [2].

  Returns
  ------
  F_I : ndarray
    Result of interpolation.

  Notes
  -----
  Weights are dynamically computed.
  Function uses JIT.

  References
  ----------
  [1]: Berrut, J.P. and Trefethen, L.N., 2004.
       Barycentric lagrange interpolation. SIAM review, 46(3), pp.501-517.
  [2]: Floater, M.S. and Hormann, K., 2007.
       Barycentric rational interpolation with no poles and high rates of
       approximation. Numerische Mathematik, 107(2), pp.315-331.
  """
  n_x = X.size - 1
  n_y = Y.size - 1

  # prepare weights
  wei_x = empty((n_x + 1), dtype=float64)
  wei_y = empty((n_y + 1), dtype=float64)

  if method == "FH":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_FH(k, d, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_FH(k, d, n_y, Y)

  elif method == "BT":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_BT(k, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_BT(k, n_y, Y)

  else:
    raise ValueError("method unknown.")

  if F_I is None:
    F_I = zeros((X_I.size, Y_I.size), dtype=F.dtype)

  # for storing regularized weights
  r_wei_x = zeros_like(wei_x, dtype=X.dtype)
  r_wei_y = zeros_like(wei_y, dtype=Y.dtype)

  for I in range(0, X_I.size):

    # regularize x weights
    for i in range(0, n_x + 1):
      diff_x = X_I[I] - X[i]
      if diff_x != 0:
        r_wei_x[i] = wei_x[i] / diff_x
      else:
        for i_r in range(0, n_x + 1):
          r_wei_x[i_r] = 0

        r_wei_x[i] = 1
        break

    for J in range(0, Y_I.size):
      num, den = 0, 0

      # regularize y weights
      for j in range(0, n_y + 1):
        diff_y = Y_I[J] - Y[j]
        if diff_y != 0:
          r_wei_y[j] = wei_y[j] / diff_y
        else:
          for j_r in range(0, n_y + 1):
            r_wei_y[j_r] = 0

          r_wei_y[j] = 1
          break

      for i in range(0, n_x + 1):
        for j in range(0, n_y + 1):
          fac = r_wei_x[i] * r_wei_y[j]
          num += fac * F[i,j]
          den += fac

      # logic without regularization
      # for i in range(0, n_x + 1):
      #   diff_x = X_I[I] - X[i]
      #   fac_x = wei_x[i] / diff_x

      #   for j in range(0, n_y + 1):
      #     diff_y = Y_I[J] - Y[j]

      #     # (x, y) & (x_I, y_I) all distinct
      #     fac_y = wei_y[j] / diff_y

      #     num += fac_x * fac_y * F[i,j]
      #     den += fac_x * fac_y

      F_I[I, J] = num / den

  return F_I

def grid_2d_interpolate_to_ordinates(X      : ndarray,
                                     Y      : ndarray,
                                     F      : ndarray,
                                     X_I    : ndarray,
                                     Y_I    : ndarray,
                                     d      : int=4,
                                     F_I    : Optional[ndarray]=None,
                                     method : str="FH") -> ndarray:
  """
  Interpolate two dimensional data. Assumes input grid values are in ascending
  order. Target co-ordinates should be of the same size.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  Y : ndarray
    Input grid associated with grid data.

  F : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Ordinate to interpolate over.

  Y_I : ndarray
    Ordinate to interpolate over.

  d : int = 4
    Order of interpolation (for use with "FH" method).

  F_I : Optional[ndarray]=None
    Optionally pass output target.

  method : str="FH"
    Method to use. For "BT" [1] is utilized whereas "FH" uses [2].

  as_target_ordinates : bool=

  Returns
  ------
  F_I : ndarray
    Result of interpolation.

  Notes
  -----
  Weights are dynamically computed.
  Function uses JIT.

  References
  ----------
  [1]: Berrut, J.P. and Trefethen, L.N., 2004.
       Barycentric lagrange interpolation. SIAM review, 46(3), pp.501-517.
  [2]: Floater, M.S. and Hormann, K., 2007.
       Barycentric rational interpolation with no poles and high rates of
       approximation. Numerische Mathematik, 107(2), pp.315-331.
  """
  n_x = X.size - 1
  n_y = Y.size - 1

  # prepare weights
  wei_x = empty((n_x + 1), dtype=float64)
  wei_y = empty((n_y + 1), dtype=float64)

  if method == "FH":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_FH(k, d, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_FH(k, d, n_y, Y)

  elif method == "BT":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_BT(k, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_BT(k, n_y, Y)

  else:
    raise ValueError("method unknown.")

  if F_I is None:
    F_I = zeros((X_I.size, ), dtype=F.dtype)

  # for storing regularized weights
  r_wei_x = zeros_like(wei_x, dtype=X.dtype)
  r_wei_y = zeros_like(wei_y, dtype=Y.dtype)

  for I in range(0, X_I.size):

    # regularize x weights
    for i in range(0, n_x + 1):
      diff_x = X_I[I] - X[i]
      if diff_x != 0:
        r_wei_x[i] = wei_x[i] / diff_x
      else:
        for i_r in range(0, n_x + 1):
          r_wei_x[i_r] = 0

        r_wei_x[i] = 1
        break

    num, den = 0, 0

    # regularize y weights
    for j in range(0, n_y + 1):
      diff_y = Y_I[I] - Y[j]
      if diff_y != 0:
        r_wei_y[j] = wei_y[j] / diff_y
      else:
        for j_r in range(0, n_y + 1):
          r_wei_y[j_r] = 0

        r_wei_y[j] = 1
        break

    for i in range(0, n_x + 1):
      for j in range(0, n_y + 1):
        fac = r_wei_x[i] * r_wei_y[j]
        num += fac * F[i,j]
        den += fac

    F_I[I] = num / den

  return F_I

def grid_3d_interpolate(X      : ndarray,
                        Y      : ndarray,
                        Z      : ndarray,
                        F      : ndarray,
                        X_I    : ndarray,
                        Y_I    : ndarray,
                        Z_I    : ndarray,
                        d      : int=4,
                        F_I    : Optional[ndarray]=None,
                        method : str="FH") -> ndarray:
  """
  Interpolate three dimensional data. Assumes input grid values are in
  ascending order.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  Y : ndarray
    Input grid associated with grid data.

  Z : ndarray
    Input grid associated with grid data.

  F : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  Y_I : ndarray
    Grid to interpolate over.

  Z_I : ndarray
    Grid to interpolate over.

  d : int = 4
    Order of interpolation (for use with "FH" method).

  F_I : Optional[ndarray]=None
    Optionally pass output target.

  method : str="FH"
    Method to use. For "BT" [1] is utilized whereas "FH" uses [2].

  Returns
  ------
  F_I : ndarray
    Result of interpolation.

  Notes
  -----
  Weights are dynamically computed.
  Function uses JIT.

  References
  ----------
  [1]: Berrut, J.P. and Trefethen, L.N., 2004.
       Barycentric lagrange interpolation. SIAM review, 46(3), pp.501-517.
  [2]: Floater, M.S. and Hormann, K., 2007.
       Barycentric rational interpolation with no poles and high rates of
       approximation. Numerische Mathematik, 107(2), pp.315-331.
  """

  n_x = X.size - 1
  n_y = Y.size - 1
  n_z = Z.size - 1

  # prepare weights
  wei_x = empty((n_x + 1), dtype=float64)
  wei_y = empty((n_y + 1), dtype=float64)
  wei_z = empty((n_z + 1), dtype=float64)

  if method == "FH":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_FH(k, d, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_FH(k, d, n_y, Y)
    for k in range(0, n_z + 1):
      wei_z[k] = _wei_bary_FH(k, d, n_z, Z)

  elif method == "BT":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_BT(k, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_BT(k, n_y, Y)
    for k in range(0, n_z + 1):
      wei_z[k] = _wei_bary_BT(k, n_z, Z)

  else:
    raise ValueError("method unknown.")

  if F_I is None:
    F_I = zeros((X_I.size, Y_I.size, Z_I.size), dtype=F.dtype)

  # for storing regularized weights
  r_wei_x = zeros_like(wei_x, dtype=X.dtype)
  r_wei_y = zeros_like(wei_y, dtype=Y.dtype)
  r_wei_z = zeros_like(wei_z, dtype=Z.dtype)

  for I in range(0, X_I.size):

    # regularize x weights
    for i in range(0, n_x + 1):
      diff_x = X_I[I] - X[i]
      if diff_x != 0:
        r_wei_x[i] = wei_x[i] / diff_x
      else:
        for i_r in range(0, n_x + 1):
          r_wei_x[i_r] = 0

        r_wei_x[i] = 1
        break

    for J in range(0, Y_I.size):

      # regularize y weights
      for j in range(0, n_y + 1):
        diff_y = Y_I[J] - Y[j]
        if diff_y != 0:
          r_wei_y[j] = wei_y[j] / diff_y
        else:
          for j_r in range(0, n_y + 1):
            r_wei_y[j_r] = 0

          r_wei_y[j] = 1
          break

      for K in range(0, Z_I.size):
        num, den = 0, 0

        # regularize z weights
        for k in range(0, n_z + 1):
          diff_z = Z_I[K] - Z[k]
          if diff_z != 0:
            r_wei_z[k] = wei_z[k] / diff_z
          else:
            for k_r in range(0, n_z + 1):
              r_wei_z[k_r] = 0

            r_wei_z[k] = 1
            break

        for i in range(0, n_x + 1):
          for j in range(0, n_y + 1):
            for k in range(0, n_z + 1):
              fac = r_wei_x[i] * r_wei_y[j] * r_wei_z[k]
              num += fac * F[i,j,k]
              den += fac

        F_I[I, J, K] = num / den

  return F_I

def grid_3d_interpolate_to_ordinates(X      : ndarray,
                                     Y      : ndarray,
                                     Z      : ndarray,
                                     F      : ndarray,
                                     X_I    : ndarray,
                                     Y_I    : ndarray,
                                     Z_I    : ndarray,
                                     d      : int=4,
                                     F_I    : Optional[ndarray]=None,
                                     method : str="FH") -> ndarray:
  """
  Interpolate three dimensional data. Assumes input grid values are in
  ascending order. Target co-ordinates should be of the same size.

  Parameters
  ----------
  X : ndarray
    Input grid associated with grid data.

  Y : ndarray
    Input grid associated with grid data.

  Z : ndarray
    Input grid associated with grid data.

  F : ndarray
    Grid data to interpolate.

  X_I : ndarray
    Grid to interpolate over.

  Y_I : ndarray
    Grid to interpolate over.

  Z_I : ndarray
    Grid to interpolate over.

  d : int = 4
    Order of interpolation (for use with "FH" method).

  F_I : Optional[ndarray]=None
    Optionally pass output target.

  method : str="FH"
    Method to use. For "BT" [1] is utilized whereas "FH" uses [2].

  Returns
  ------
  F_I : ndarray
    Result of interpolation.

  Notes
  -----
  Weights are dynamically computed.
  Function uses JIT.

  References
  ----------
  [1]: Berrut, J.P. and Trefethen, L.N., 2004.
       Barycentric lagrange interpolation. SIAM review, 46(3), pp.501-517.
  [2]: Floater, M.S. and Hormann, K., 2007.
       Barycentric rational interpolation with no poles and high rates of
       approximation. Numerische Mathematik, 107(2), pp.315-331.
  """

  n_x = X.size - 1
  n_y = Y.size - 1
  n_z = Z.size - 1

  # prepare weights
  wei_x = empty((n_x + 1), dtype=float64)
  wei_y = empty((n_y + 1), dtype=float64)
  wei_z = empty((n_z + 1), dtype=float64)

  if method == "FH":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_FH(k, d, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_FH(k, d, n_y, Y)
    for k in range(0, n_z + 1):
      wei_z[k] = _wei_bary_FH(k, d, n_z, Z)

  elif method == "BT":
    for k in range(0, n_x + 1):
      wei_x[k] = _wei_bary_BT(k, n_x, X)
    for k in range(0, n_y + 1):
      wei_y[k] = _wei_bary_BT(k, n_y, Y)
    for k in range(0, n_z + 1):
      wei_z[k] = _wei_bary_BT(k, n_z, Z)

  else:
    raise ValueError("method unknown.")

  if F_I is None:
    F_I = zeros((X_I.size, ), dtype=F.dtype)

  # for storing regularized weights
  r_wei_x = zeros_like(wei_x, dtype=X.dtype)
  r_wei_y = zeros_like(wei_y, dtype=Y.dtype)
  r_wei_z = zeros_like(wei_z, dtype=Z.dtype)

  for I in range(0, X_I.size):

    # regularize x weights
    for i in range(0, n_x + 1):
      diff_x = X_I[I] - X[i]
      if diff_x != 0:
        r_wei_x[i] = wei_x[i] / diff_x
      else:
        for i_r in range(0, n_x + 1):
          r_wei_x[i_r] = 0

        r_wei_x[i] = 1
        break

    # regularize y weights
    for j in range(0, n_y + 1):
      diff_y = Y_I[I] - Y[j]
      if diff_y != 0:
        r_wei_y[j] = wei_y[j] / diff_y
      else:
        for j_r in range(0, n_y + 1):
          r_wei_y[j_r] = 0

        r_wei_y[j] = 1
        break

    num, den = 0, 0

    # regularize z weights
    for k in range(0, n_z + 1):
      diff_z = Z_I[I] - Z[k]
      if diff_z != 0:
        r_wei_z[k] = wei_z[k] / diff_z
      else:
        for k_r in range(0, n_z + 1):
          r_wei_z[k_r] = 0

        r_wei_z[k] = 1
        break

    for i in range(0, n_x + 1):
      for j in range(0, n_y + 1):
        for k in range(0, n_z + 1):
          fac = r_wei_x[i] * r_wei_y[j] * r_wei_z[k]
          num += fac * F[i,j,k]
          den += fac

    F_I[I] = num / den

  return F_I

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  _set_J_FH = JITI(_set_J_FH)
  _wei_bary_FH = JITI(_wei_bary_FH)
  _wei_bary_BT = JITI(_wei_bary_BT)
  grid_1d_interpolate = JITI(grid_1d_interpolate)
  grid_2d_interpolate = JITI(grid_2d_interpolate)
  grid_3d_interpolate = JITI(grid_3d_interpolate)
  grid_2d_interpolate_to_ordinates = JITI(grid_2d_interpolate_to_ordinates)
  grid_3d_interpolate_to_ordinates = JITI(grid_3d_interpolate_to_ordinates)

#
# :D
#
