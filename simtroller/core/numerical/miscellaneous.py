#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Miscellaneous functionality
"""
###############################################################################
# python imports
 # pylint: disable=unused-import
from typing import Any, TYPE_CHECKING, Union, Tuple
from numpy import (abs, ndarray, unique) # pylint: disable=redefined-builtin
from numpy.typing import NDArray

# package imports
from simtroller.core.numba import JITI
###############################################################################

# pylint: disable=invalid-name

def ndarray_get_sorted_argmin(val : Any, arr : ndarray) -> int:
  """
  Given a value and a sorted input array return index 'ix' such that:
  abs(arr[ix] - value) is minimized.

  Parameters
  ----------
  val : Any
    Value to minimize against.

  arr : ndarray
    Array to search (must be sorted).

  Returns
  -------
  ix : int
    Found index.
  """
  ix = 0
  cval = abs(arr[ix]-val)  # do not miss first entry
  for ix in range(1, arr.size):
    ccval = abs(arr[ix] - val)
    if ccval < cval:
      cval = ccval
    else:
      return ix - 1       # current too large, take last
  return ix

def interval_is_intersection_empty(I_a : int, I_b : int,
                                   J_a : int, J_b : int) -> bool:
  """
  Given two intervals [I_a, I_b] and [J_a, J_b] check if intersection is empty.
  """
  # [I_a, I_b]
  # [J_a, J_b]
  return ((J_a > I_b) or (I_a > J_b))

def scalar_data_deduplicate_sort(
  data           : NDArray,
  column         : int=0,
  return_indices : bool=False) -> Any:
  """
  Given N x M data sort and deduplicate along axis = 0. Column for sorting max
  be selected.

  Parameters
  ----------
  data : NDArray
    Data to deduplicate / sort.

  column : int = 0
    Column to use for deduplication / sorting.

  return_indices : bool = False
    Control whether unique / sorting indices are returned.

  Returns
  -------
  data or optionally (data, indices)
  """
  to_parse = data[:, column]
  _, ix_parsed = unique(to_parse, return_index=True)

  if return_indices:
    return (data[ix_parsed, :], ix_parsed)

  return data[ix_parsed, :]

###############################################################################
# JIT if not type-checking as required
###############################################################################

if TYPE_CHECKING:
  # pylint: disable=undefined-variable
  reveal_locals()
else:
  ndarray_get_sorted_argmin = JITI(ndarray_get_sorted_argmin)
  interval_is_intersection_empty = JITI(interval_is_intersection_empty)

#
# :D
#
