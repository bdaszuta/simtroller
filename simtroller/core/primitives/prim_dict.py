#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Q.O.L. for manipulation of python dictionaries.
"""
from typing import Any, TYPE_CHECKING, Optional # pylint: disable=unused-import

from simtroller.core.primitives.prim_str import str_delimiter_replace

def dict_flatten_one_level(to_flatten : dict) -> dict:
  '''
  Flattens nested dictionary of dictionaries by one level.

  Parameters
  ----------
  to_flatten : dict
    Possible nested dictionary to flatten. Idempotent if flat.

  Returns
  -------
  to_return : dict
    Dictionary flattened by one level. Keys that nest are removed.

  Usage
  -----
  >>> from simtroller.core.primitives import dict_flatten_one_level
  >>> dict_nested = {"a": {"b":3, "d": {"e": 6}}, "c": 4}
  >>> dict_flatten_one_level(dict_nested)
      {'b': 3, 'd': {'e': 6}, 'c': 4}
  '''
  to_return = {}
  for key, val in to_flatten.items():
    if isinstance(val, dict):
      for nkey, nval in val.items():
        to_return[nkey] = nval
    else:
      to_return[key] = val
  return to_return

def dict_default_get(key                 : Any,
                     dictionary          : dict,
                     default             : Any = None,
                     populate_on_missing : bool = True) -> Any:
  '''
  Retrieve value from dictionary. If missing return specified default.

  Parameters
  ----------
  key : Any
    Key to extract.

  dictionary : dict
    Extract from provided dictionary.

  default : Any = None
    Default value to return when key is missing.

  populate_on_missing : bool = True
    Populate missing key with default.

  Returns
  -------
  value : Any
    Value associated with key.

  Usage
  -----
  >>> from simtroller.core.primitives import dict_default_get
  >>> dictionary = {"a": 2, "c": 4}
  >>> dict_default_get("a", dictionary, default=7)
      2
  >>> dict_default_get("b", dictionary, default=7)
      7
  '''
  try:
    val = dictionary[key]
  except KeyError:
    val = default
    if populate_on_missing:
      dictionary[key] = val

  return val

def dict_nested_set(key           : Any,
                    populate_dict : dict,
                    value         : Any,
                    update        : bool = False) -> None:
  '''
  Set value in dictionary. The key may be a list which entails DoD nesting.

  Parameters
  ----------
  key : Any
    Key to populate. May be a sequence (dictionary of dictionaries).

  populate_dict : dict
    To populate.

  value : Any
    Value to populate with.

  update : bool = False
    Control whether dictionary update is performed at key or a replacement.

  Usage
  -----
  >>> from simtroller.core.primitives import dict_nested_set
  >>> dictionary = {'a': 2, 'c': 4}
  >>> dict_nested_set(['b', 'd'], dictionary, 7)
  >>> dictionary
      {'a': 2, 'c': 4, 'b': {'d': 7}}
  '''
  if isinstance(key, (list, tuple)):
    if len(key) > 1:
      if (key[0] not in populate_dict.keys()
          or not isinstance(populate_dict[key[0]], dict)):
        populate_dict[key[0]] = {}
      dict_nested_set(key[1:], populate_dict[key[0]], value, update=update)
    else:
      if update:
        try:
          populate_dict[key[0]].update(value)
        except:
          populate_dict[key[0]] = value
      else:
        populate_dict[key[0]] = value
  else:
    if update:
      try:
        populate_dict[key].update(value)
      except:
        populate_dict[key] = value
    else:
      populate_dict[key] = value

def dict_nested_get(key        : Any,
                    dictionary : dict,
                    default    : Any = None) -> Any:
  '''
  Return value from dictionary. The key may be a list which entails DoD nesting.

  Parameters
  ----------
  key : Any
    Key to populate. May be a sequence (dictionary of dictionaries).

  dictionary : dict
    Dictionary to parse.

  default : Any = None
    Value to return on missing key.

  Returns
  -------
  value : Any
    Value associated with key.

  Usage
  -----
  >>> from simtroller.core.primitives import dict_nested_get
  >>> dictionary = {'a': 2, 'c': 4, 'v': {'d': 3, 'e': 9}}
  >>> dict_nested_get('c', dictionary, default=7)
      4
  >>> dict_nested_get(['v', 'e'], dictionary, default=7)
      9
  >>> dict_nested_get(['v', 'e', 'f'], dictionary, default=7)
      7
  >>> dict_nested_get(['z', 'w', 'f'], dictionary, default=7)
      7
  '''
  cur_key = key
  if isinstance(key, (list, tuple)):
    if len(key) > 1:
      try:
        return dict_nested_get(key[1:], dictionary[key[0]], default=default)
      except KeyError:
        return default
    else:
      cur_key = key[0]

  try:
    val = dictionary[cur_key]
  except:
    val = default
  return val

class dict_with_rules(dict):
  """
  Provide a dictionary that automatically replaces values based on string
  search.

  Only use this if you require such rule functionality as it will be slower
  than the native Python dict primitive.
  """
  def __init__(self, *args, delimiters=("[[", "]]"), **kwargs):
    dict.__init__(self, *args, **kwargs)

    self.delimiters = delimiters
    self.key_parsers = []

    self.update({})

  def __setitem__(self, key, val, update_rules=True):
    dict.__setitem__(self, key, val)
    if update_rules:
      self.update({})

  def update(self, *args, **kwargs):
    for k, v in dict(*args, **kwargs).items():
      self.__setitem__(k, v, update_rules=False)

    replace_result = eval(
      str_delimiter_replace(repr(self), self, delimiters=self.delimiters)
    )

    # run key parsers if they exist
    for k, v in replace_result.items():
      for key_matcher, function_apply in self.key_parsers:
        if key_matcher(k):
          v = function_apply(v)
      self.__setitem__(k, v, update_rules=False)

  def set_key_parser(self, key_matcher, function_apply):
    """
    Set internal key_matcher function which if True will apply a function to
    value of an associated key.
    """
    self.key_parsers.append([key_matcher, function_apply])
    self.update({})

#
# :D
#
