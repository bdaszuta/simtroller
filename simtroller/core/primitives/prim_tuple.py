#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Q.O.L. for manipulation of python tuple.
"""
from typing import Any, TYPE_CHECKING, Sequence # pylint: disable=unused-import

def tuple_default_get(idx         : int,
                      input_tuple : tuple,
                      default     : Any = None) -> Any:
  '''
  Retrieve value from tuple. If missing return specified default.

  Parameters
  ----------
  idx : int
    Index to extract at.

  input_tuple : tuple
    Extract from provided tuple.

  default : Any = None
    Default value to return when idx is missing.

  Returns
  -------
  value : Any
    Value associated with idx.

  Usage
  -----
  >>> from simtroller.core.primitives import tuple_default_get
  >>> values = (1, 2, 3)
  >>> tuple_default_get(0, values, default=7)
      1
  >>> tuple_default_get(8, values, default=7)
      7
  '''
  try:
    ret = input_tuple[idx]
  except IndexError:
    ret = default
  return ret

def tuple_like(input_arg : Any = None) -> tuple:
  '''
  Ensure input becomes tuple-like. Input strings get nested.

  Parameters
  ----------
  input_arg : Any = None
    Object to parse.

  Returns
  -------
  as_tuple_like : tuple
    Result of conversion (as required - idempotent operation if not).

  Usage
  -----
  >>> from simtroller.core.primitives import tuple_like
  >>> tuple_like('test')
      ('test',)
  >>> tuple_like([1, 2, 3])
      (1, 2, 3)
  >>> tuple_like((1, 2, 3))
      (1, 2, 3)
  >>> tuple_like({1, 2, 3})
      (1, 2, 3)
  '''
  if isinstance(input_arg, tuple):
    return input_arg

  if isinstance(input_arg, (list, set)):
    return tuple(input_arg)

  if isinstance(input_arg, str):
    return (input_arg, )

  if input_arg is None:
    return tuple()

  raise ValueError("'input_arg' cannot be converted")

#
# :D
#
