#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Q.O.L. for manipulation of python lists.
"""
from itertools import islice

from typing import Any, TYPE_CHECKING, Sequence # pylint: disable=unused-import
from typing import Union

def list_default_get(idx        : int,
                     input_list : list,
                     default    : Any = None) -> Any:
  '''
  Retrieve value from list. If missing return specified default.

  Parameters
  ----------
  idx : int
    Index to extract at.

  input_list : list
    Extract from provided list.

  default : Any = None
    Default value to return when idx is missing.

  Returns
  -------
  value : Any
    Value associated with idx.

  Usage
  -----
  >>> from simtroller.core.primitives import list_default_get
  >>> values = [1, 2, 3]
  >>> list_default_get(0, values, default=7)
      1
  >>> list_default_get(8, values, default=7)
      7
  '''
  try:
    ret = input_list[idx]
  except IndexError:
    ret = default
  return ret

def list_like(input_arg : Any = None) -> list:
  '''
  Ensure input becomes list-like. Input strings get nested.

  Parameters
  ----------
  input_arg : Any = None
    Object to parse.

  Returns
  -------
  as_list_like : list
    Result of conversion (as required - idempotent operation if not).

  Usage
  -----
  >>> from simtroller.core.primitives import list_like
  >>> list_like('test')
      ['test']
  >>> list_like([1, 2, 3])
      [1, 2, 3]
  >>> list_like((1, 2, 3))
      [1, 2, 3]
  >>> list_like({1, 2, 3})
      [1, 2, 3]
  '''
  if isinstance(input_arg, list):
    return input_arg

  if isinstance(input_arg, (tuple, set)):
    return list(input_arg)

  if isinstance(input_arg, str):
    return [input_arg, ]

  if input_arg is None:
    return []

  raise ValueError("'input_arg' cannot be converted")

def list_split_chunks(to_split : list,
                      chunks : Union[list, tuple]) -> list:
  '''
  Split an input list to list of list based on provided chunks.

  Parameters
  ----------
  to_split : list
    List to split.

  chunks : Union[list, tuple]
    Specification of chunks.

  Returns
  -------
  split : list
    Result of splitting.

  Usage
  -----
  >>> from simtroller.core.primitives import list_split_chunks
  >>> list_like([1, 2, 3, 4], [4,])
      [[1, 2, 3, 4]]
  >>> list_like([1, 2, 3, 4], [1, 3])
      [[1,], [2, 3, 4]]
  '''
  it = iter(to_split)
  return [list(islice(it, c)) for c in chunks]

#
# :D
#
