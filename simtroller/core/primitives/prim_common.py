#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Common functions for primitives.
"""
from typing import Any, TYPE_CHECKING, Sequence # pylint: disable=unused-import

# TODO: Typing
def sequence_generator_flatten(to_flatten):
  '''
  Generator for flattening a nested sequence of sequences.

  Parameters
  ----------
  to_flatten : sequence
    Sequence to flatten.

  Returns
  -------
  Generator that flattens nested sequence.

  Usage
  -----
  >>> from simtroller.core.primitives import sequence_generator_flatten
  >>> list(sequence_generator_flatten([1,2,3]))
      [1, 2, 3]
  >>> list(sequence_generator_flatten([1, [[2, 4], 3]]))
      [1, 2, 4, 3]
  '''
  for element in to_flatten:
    if isinstance(element, (list, tuple)):
      yield from sequence_generator_flatten(element)
    else:
      yield element

#
# :D
#
