"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functionality for parsing primitive python types.
"""
from simtroller.core.primitives.prim_common import (
  sequence_generator_flatten,
)

from simtroller.core.primitives.prim_dict import (
  dict_flatten_one_level,
  dict_default_get,
  dict_nested_set,
  dict_nested_get,
  dict_with_rules
)

from simtroller.core.primitives.prim_list import (
  list_default_get,
  list_like,
  list_split_chunks,
)

from simtroller.core.primitives.prim_str import (
  str_delimiter_extract,
  str_delimiter_replace,
  str_filename_sanitize,
  str_flat_list_like_parser,
)

from simtroller.core.primitives.prim_tuple import (
  tuple_default_get,
  tuple_like,
)

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("prim_common"))
exec(_import_clear("prim_dict"))
exec(_import_clear("prim_list"))
exec(_import_clear("prim_str"))
exec(_import_clear("prim_tuple"))
# =============================================================================

#
# :D
#
