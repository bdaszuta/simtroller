#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Q.O.L. for manipulation of python strings.
"""
import re as _re

from typing import Any, TYPE_CHECKING, Sequence # pylint: disable=unused-import
from typing import List, Type # pylint: disable=unused-import

def _impl_reg_parse_delims(delim):
  sdelim = str(delim)
  reps = {
    "[": "\\[",
    "]": "\\]",
    "(": "\\(",
    ")": "\\)",
    "{": "\\{",
    "}": "\\}",
  }

  for key, val in reps.items():
    sdelim = sdelim.replace(key, val)
  return sdelim

def str_delimiter_extract(
  string      : str,
  delimiters  : Sequence[str] = ("[[", "]]"),
  to_sort     : bool = True
) -> List[str]:
  '''
  Extract vars between delimiters.

  Parameters
  ----------
  string : str
    String to extract from.

  delimiters : Sequence[str] = ("[[", "]]")
    Delimiters to use for search (a matching test is perform internally).

  to_sort : bool = False
    Control whether the result is sorted.

  Returns
  -------
  result : list
    An (optionally sorted) list of matches.

  Usage
  -----
  >>> from simtroller.core.primitives import str_delimiter_extract
  >>> to_parse = """
  ...   z = [[Z_VAL]]
  ...   y = [[3]]
  ... """
  >>> str_delimiter_extract(to_parse)
      ['3', 'Z_VAL']

  Notes
  -----
  `repr` should allow this to be used on other structures.
  '''
  string = repr(string)
  if string.count(delimiters[0]) != string.count(delimiters[1]):
    raise ValueError("Input has mismatched delimiters.")

  # res = _re.findall(r"\[\[(.*?)\]\]", string)
  res = _re.findall(_impl_reg_parse_delims(delimiters[0]) +
                    "(.*?)" +
                    _impl_reg_parse_delims(delimiters[1]), string)

  result = list(set(res))
  if to_sort:
    result = sorted(result)

  return result

def str_delimiter_replace(
  template    : str,
  rule_dict   : dict,
  delimiters  : Sequence[str] = ("[[", "]]"),
  fixed_point : bool = True
) -> str:
  '''
  Perform replacements in a given string based on dictionary.

  Parameters
  ----------
  template : str
    String to replace in.

  rule_dict : dict
    Dictionary where keys are searched for and values replace.

  delimiters : Sequence[str] = ("[[", "]]")
    Delimiters to use for search (a matching test is perform internally).

  fixed_point : bool = True
    Continue to perform replacements on results until no change.

  Usage
  -----
  >>> from simtroller.core.primitives import str_delimiter_replace
  >>> to_parse = """
  ...   z = [[Z_VAL]]
  ...   y = [[y]]
  ... """
  >>> rule_dict = {"Z_VAL": 2, "y": 7}
  >>> str_delimiter_replace(to_parse, rule_dict)
      '\\n  z = 2\\n  y = 7\\n'

  Notes
  -----
  `repr` should allow this to be used on other structures.
  '''

  ori = rep = repr(template)

  if rep.count(delimiters[0]) != rep.count(delimiters[1]):
    raise ValueError("Input has mismatched delimiters.")

  str_find = delimiters[0] + "{find}" + delimiters[1]

  for find, replace in rule_dict.items():
    rep = rep.replace(str_find.format(find=str(find)), str(replace))

  if ori == rep:  # avoid infinite recursion if no progress made
    fixed_point = False


  # pylint: disable=eval-used
  result = eval(rep)

  if fixed_point and delimiters[0] in result:
    result = str_delimiter_replace(
      template=result,
      rule_dict=rule_dict,
      delimiters=delimiters,
      fixed_point=True)

  return result

def str_filename_sanitize(
  string : str,
  special_replace : str = "_") -> str:
  '''
  Sanitize input string for filename.
  '''
  replacements = (
    ":", " ", ",", "="
  )
  for replacement in replacements:
    string = string.replace(replacement, special_replace)

  return string

def _delete_characters(parse, characters=tuple()):
  # removes characters from string
  for char in characters:
    parse = parse.replace(char, "")
  return parse

def str_flat_list_like_parser(parse      : str,
                              delimiter  : str=",",
                              sanitize   : bool=True,
                              convert_to : Type=str) -> tuple:
  """
  Given string with list-likes parse & optionally convert elements.

  Possible cases:

  Source str  -> Target lists (individual a,b,c,d elements converted)
  [a,b],c     -> [[a,b], c]
  a,[b,c]     -> [a, [b,c]]
  a,c         -> [a, c]
  [a,b],[c,d] -> [[a,b], [c,d]]
  [a,b,c],d   -> [[a,b,c], d]

  Parameters
  ----------
  parse : str
    String to parse

  delimiter : str=","
    Delimiter separating elements

  sanitize : bool=True
    Drop empty (spaces) and clip repeated delimiters.

  convert_to : Type=str
    Specify conversion of individual elements

  Returns
  -------
  Parsed structure as described above.
  """

  # sanitize: drop empty & clip repeated delimiters
  if sanitize:
    while " " in parse:
      parse = parse.replace(" ", "")

    dd = delimiter + delimiter
    while dd in parse:
      parse = parse.replace(dd, delimiter)

  parse = parse.split(",")

  # have possible sublists at depth 1
  # reassemble these, drop brackets, split again

  parsed = []
  sublist = []

  for el in parse:
    if "[" in el:
      sublist.append(el)
    elif (len(sublist) > 0):
      sublist.append(el)
      if "]" in el:
        # sublist finished, strip
        sublist = [convert_to(el.replace("[", "").replace("]", ""))
                  for el in sublist]
        parsed.append(tuple(sublist))
        sublist = []
    else:
      parsed.append(convert_to(el))

  return tuple(parsed)

#
# :D
#
