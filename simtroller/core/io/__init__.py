"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functionality related to input / output goes here.
"""
from simtroller.core.io.pathing import (
  path_is_extant,
  path_to_absolute,
  path_cwd_get,
  path_cwd_set,
  path_strip,
  path_is_newer,
  path_split_extension,
  dir_list,
  dir_filter
)

from simtroller.core.io.basic import (
  dir_generate,
  dir_ensure_extant,
  json_load,
  json_dump,
  pickle_load,
  pickle_dump,
  raw_load,
  raw_dump,
  file_ensure_extant,
  csv_load,
  csv_dump,
  tpl_parse
)

from simtroller.core.io.hdf5 import (
  hdf5_file_get_attr,
  hdf5_filenames_filter,
  hdf5_filenames_apply_parser,
  hdf5_dataset_get,
  hdf5_file_get_dataset_names
)

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("pathing"))
exec(_import_clear("basic"))
exec(_import_clear("hdf5"))
# =============================================================================

#
# :D
#
