#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Deal with file system pathing.
"""
###############################################################################
# python imports
from pathlib import Path
import os as _os

from typing import (Any, TYPE_CHECKING, Union, # pylint: disable=unused-import
                    List, Sequence)

# package imports
from simtroller.core.primitives import (list_like, sequence_generator_flatten)
from simtroller.core.shell import (print_signal, print_warning, )
###############################################################################

def path_is_extant(to_check        : Any,
                   fail_on_missing : bool = False,
                   try_absolute    : bool = True) -> bool:
  '''
  Check if file or path exists. Optionally terminate on missing.

  Parameters
  ----------
  to_check : Any
    String or list describing path or file to check.

  fail_on_missing : bool = False
    If not found fail raising ValueError.

  try_absolute : bool = True
    Try to convert input path to absolute if existence fails.

  Returns
  -------
  exists : bool
    Status of existence.
  '''
  list_to_check = list_like(to_check)
  to_check = list(sequence_generator_flatten(list_to_check))
  to_check = _os.path.join(*to_check)

  exists = _os.path.exists(to_check)

  if not exists and try_absolute:
    path_abs = path_to_absolute(to_check, verify=False)
    exists = _os.path.exists(path_abs)

  if not exists and fail_on_missing:
    print_warning(f"Current working dir: {_os.getcwd()}")
    raise ValueError(f"'{to_check}' does not exist.")

  return exists

def path_to_absolute(to_convert : Any,
                     verify     : bool = True) -> str:
  '''
  Convert input (potentially relative) path to absolute.

  Parameters
  ----------
  to_convert : Any
    String or list describing path or file to convert.
    Nested lists are allowed.

  verify : bool = True
    If converted input not found fail raising ValueError.

  Returns
  -------
  result : str
    Converted path.

  Notes
  -----
  Can be used to extract shell environment variables.

  Leading '/' on specification ensure absolute rather than relative path.
  '''
  # work for str / list input
  to_convert = list(sequence_generator_flatten(list_like(to_convert)))
  to_convert = [str(tc) for tc in to_convert]
  candidate = _os.path.join(*to_convert)

  # expand any user-specified variables (i.e $HOME)
  candidate = _os.path.expandvars(candidate)

  # consider as relative
  candidater = _os.path.abspath(candidate)

  # consider listlike full path (needs prefix)
  candidatep = _os.path.join(_os.path.sep, candidate)

  if path_is_extant(candidater, fail_on_missing=False, try_absolute=False):
    candidate = candidater
  elif path_is_extant(candidatep, fail_on_missing=False, try_absolute=False):
    # consider as abs path listlike pass which needs prefix
    candidate = candidatep
  else:
    candidate = candidater

  if verify:
    path_is_extant(candidate, fail_on_missing=True, try_absolute=False)
  return candidate

def path_cwd_get() -> str:
  """
  Convenience function to return current working directory.
  """
  return path_to_absolute("", verify=False)

def path_cwd_set(directory : Union[str, list, tuple] = None,
                 signal    : bool = True) -> None:
  """
  Convenience function to set current working directory.
  """
  to_change = path_to_absolute(directory, verify=True)
  _os.chdir(to_change)
  if signal:
    print_signal("cwd:")
    print(path_cwd_get())

def path_strip(path : str, mode : str = "filename") -> str:
  """
  Given an absolute path to a file strip filename or directory structure.

  Parameters
  ----------
  path : str
    Specification of path.

  mode : str = "filename"
    Type of stripping: element of {"filename", "absolute_path"}

  Returns
  -------
  result : str
    Result of stripping

  Usage
  -----
  >>> from simtroller.core.io import path_strip
  >>> test_path = '/some/long/path/file.md'
  >>> path_strip(test_path, mode="filename")
      '/some/long/path'
  >>> path_strip(test_path, mode="absolute_path")
      'file.md'
  """
  if mode == "absolute_path":
    if _os.path.sep not in path:
      return path
    return _os.path.split(path)[1]

  if mode == "filename":
    return _os.path.split(path)[0]

  raise ValueError("Unknown 'mode' selected.")

def path_is_newer(path_new : Any, path_old : Any) -> bool:
  """
  Given two paths compare time of change.

  Parameters
  ----------
  path_new : Any
    Specification of path.

  path_old : Any
    Specification of path.

  Returns
  -------
  result : bool
    Result of comparison. Failure if either path is not valid.

  Usage
  -----
  >>> from simtroller.core.io import path_is_newer
  >>> from simtroller.shell.io import shell_interact
  >>> shell_interact(["touch", "file_old"])
  >>> shell_interact(["touch", "file_new"])
  >>> path_is_newer("file_new", "file_old")
      True
  """
  if (not path_is_extant(path_new)) or (not path_is_extant(path_old)):
    return False

  _path_old = path_to_absolute(path_old)
  _path_new = path_to_absolute(path_new)

  return _os.path.getctime(_path_new) > _os.path.getctime(_path_old)

def path_split_extension(path : Any) -> tuple[str]:
  """
  Given path to file, extract extension.

  Parameters
  ----------
  path : Any
    Specification of path (including filename with an extension).

  Returns
  -------
  result : tuple
    (The file extension, The path with extension stripped)
  """
  path = path_to_absolute(path)

  len_ext = path[::-1].find(".")
  path_no_ext = path[:-(len_ext+1)]
  ext = path[-len_ext:]

  result = (ext, path_no_ext)
  return result

def dir_list(directory   : Any = None,
             to_absolute : bool = True,
             recursive   : bool = False,
             to_sort     : bool = True) -> list:
  '''
  Get listing of directory.

  Parameters
  ----------
  directory : Any
    String or list describing path to list.

  to_absolute : bool = True
    Convert output to absolute paths.

  recursive : bool = False
    Control whether subdirectories are explored.

  to_sort : bool = True
    Control whether the result is sorted.

  Returns
  -------
  result : list
    The result of the query.
  '''
  directory = path_to_absolute(directory, verify=True)

  if not recursive:
    ldirectory = _os.listdir(directory)

    if to_absolute:
      ldirectory = [_os.path.join(directory, d)
                    for d in ldirectory]

  else:
    ldirectory = []
    for root, subdirs, files in _os.walk(directory):
      for subdir in subdirs:
        ldirectory.append(_os.path.join(root, subdir))

      for file in files:
        ldirectory.append(_os.path.join(root, file))

    if not to_absolute:
      ldirectory = [cur_dir.replace(directory + _os.path.sep, "")
                    for cur_dir in ldirectory]

  if to_sort:
    ldirectory = sorted(ldirectory)

  return ldirectory

def dir_filter(directories : List[str],
               mode        : str = "files",
               extensions  : Sequence[str] = None) -> List[str]:
  """
  Filter a directory listing.

  Parameters
  ----------
  directories : List[str]

  mode : str = 'files'
    Filter out 'files' or 'directories'.

  extensions : Sequence[str] = None
    If directories are filtered out, optionally only retain certain file
    extensions.

  Returns
  -------
  filtered : List[str]
    Result of filtering.
  """
  # for interactive usage
  if isinstance(directories, str):
    directories = [directories, ]

  for cur_dir in directories:
    if cur_dir[0] != _os.path.sep:
      raise ValueError("Absolute paths are required for filtering")

  if mode == "directories":
    dir_files = [obj for obj in directories
                 if _os.path.isfile(obj)]

    if extensions is not None:
      extensions = [ext if len(ext) > 0 and '.' in ext else '.' + ext
                    for ext in list_like(extensions)]
      parsed_extensions = set(extensions)

      dir_files = [f for f in dir_files
                  if _os.path.splitext(f)[1] in parsed_extensions]

    return dir_files

  if mode == "files":
    dirs = [obj for obj in directories if not _os.path.isfile(obj)]
    return dirs

  raise ValueError("Specified 'mode' unknown.")

#
# :D
#
