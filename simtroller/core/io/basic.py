#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Deal with file system pathing.
"""
###############################################################################
# python imports
# pylint: disable=unused-import
from typing import Any, TYPE_CHECKING, Union, Optional, Sequence
import pickle as _p
import json as _j
import lzma as _lzma
from numpy import (ndarray, loadtxt, savetxt)
from numpy.typing import NDArray

# package imports
from simtroller.core.shell import (print_signal,
                                   shell_interact)
from simtroller.core.io.pathing import (path_to_absolute,
                                        path_is_extant,
                                        path_strip)
from simtroller.core.primitives.prim_str import str_delimiter_replace
###############################################################################

def dir_generate(to_generate : Union[str, list, tuple],
                 verify      : bool = True,
                 signal      : bool = True):
  '''
  Generate directory structure (potentially nested) as required.

  Parameters
  ----------
  to_generate : Union[str, list, tuple]
    Specification of directory structure to generate.
    Warning: To enforce absolute paths use leading '/'.

  verify : bool = True
    Use `path_is_extant` for verification.

  signal : bool = True
    Print result of generation.

  Notes
  -----
  Leading '/' on specification ensure absolute rather than relative path.
  '''
  to_mk = path_to_absolute(to_generate,
                           verify=False) # may not exist - no verify!
  shell_interact(['mkdir', '-p', to_mk],
                 method='subprocess_run',
                 capture_output=False, suppress_output=True)

  if verify:
    path_is_extant(to_mk, fail_on_missing=True)

  if signal:
    print_signal("Directory structure generated:")
    print(to_mk)

def dir_ensure_extant(filename : Union[str, list, tuple],
                      no_strip : bool = True,
                      verify   : bool = True,
                      signal   : bool = True) -> None:
  """
  Given an absolute filename specification ensure that the nesting directory
  structure exists.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Specification of (absolute) filename with leading directory structure;
    filename is stripped.
    Warning: To enforce absolute paths use leading '/'.

  no_strip : bool = True
    Control whether the above filename stripping is performed.

  verify : bool = True
    Use `path_is_extant` for verification.

  signal : bool = True
    Print result of generation if required.
  """
  filename = path_to_absolute(filename, verify=False)

  # generate nested directory structure as required
  if not no_strip:
    dir_structure = path_strip(filename, mode="filename")
  else:
    dir_structure = filename

  if not path_is_extant(dir_structure, fail_on_missing=False):
    dir_generate(dir_structure, signal=signal, verify=verify)


def json_load(filename : Union[str, list, tuple],
              verify :   bool = True) -> dict:
  '''
  Convenience function for loading json file.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    File to load.

  verify : bool = True
    Verify file exists.

  Returns
  -------
  Dictionary representing JSON.
  '''
  filename = path_to_absolute(filename, verify=verify)
  with open(filename, 'r', encoding='utf-8') as filehandle:
    return _j.load(filehandle)

def json_dump(content  : dict = None,
              filename : Union[str, list, tuple] = None,
              verify   : bool = True,
              indent   : int = 2) -> None:
  '''
  Convenience function for dumping json file.

  Parameters
  ----------
  content : dict
    Dictionary of data to dump. May contain `NDArray` which will be converted
    with `tolist` bound method.

  filename : Union[str, list, tuple]
    File to dump to.

  verify : bool = True
    Verify dump success.

  indent : int = 2
    Default indentation to use for JSON.
  '''
  filename = path_to_absolute(filename, verify=False)

  # generate nested directory structure as required
  dir_ensure_extant(filename, no_strip=False, verify=True)

  # see SE: 26646362
  class NumpyEncoder(_j.JSONEncoder):
    """parse array for JSON"""
    def default(self, o): # pylint: disable=E0202
      if isinstance(o, ndarray):
        return o.tolist()
      return o

  with open(filename, 'w', encoding='utf-8') as filehandle:
    _j.dump(content, filehandle, cls=NumpyEncoder, indent=indent)

  if verify:
    path_is_extant(filename, fail_on_missing=True)

def pickle_load(filename :   Union[str, list, tuple] = None,
                verify :     bool = True,
                compression: Any = None) -> Any:
  '''
  Quick and dirty pickler: loading.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename of pickle to load.

  verify : bool = True
    Verify file exists.

  compression: Any : None,
    Control whether dump was compressed. Element of {None, "lzma"}.

  Returns
  -------
  Data contained in pickle.
  '''
  filename = path_to_absolute(filename, verify=verify)

  if compression is None:
    with open(filename, 'rb') as filehandle:
      return _p.load(filehandle)
  elif compression == "lzma":
    with _lzma.open(filename, 'rb') as filehandle:
      return _p.load(filehandle)
  else:
    raise NotImplementedError(f"Compression {compression} unknown.")

def pickle_dump(content  :   Any,
                filename :   Union[str, list, tuple] = None,
                verify   :   bool = True,
                compression: Any = None) -> None:
  '''
  Quick and dirty pickler: dumping.

  Parameters
  ----------
  content : any
    Data to dump.

  filename : Union[str, list, tuple]
    File to dump to.

  verify : bool = True
    Verify dump success.

  compression: Any : None,
    Control whether dump is compressed. Element of {None, "lzma"}.
  '''
  filename = path_to_absolute(filename, verify=False)

  # generate nested directory structure as required
  dir_ensure_extant(filename, no_strip=False, verify=True)

  if compression is None:
    with open(filename, 'wb') as filehandle:
      _p.dump(content, filehandle)
  elif compression == "lzma":
    with _lzma.open(filename, 'wb') as filehandle:
      _p.dump(content, filehandle)
  else:
    raise NotImplementedError(f"Compression {compression} unknown.")

  if verify:
    path_is_extant(filename, fail_on_missing=True)

def raw_load(filename : Union[str, list, tuple] = None,
             encoding : str = 'utf-8') -> str:
  '''
  Simple file load.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename of raw data to load.

  encoding : str = 'utf-8'
    Encoding to utilize.

  Returns
  -------
  Data contained in file.
  '''
  filename = path_to_absolute(filename, verify=True)
  with open(filename, 'r', encoding=encoding) as filehandle:
    return filehandle.read()

def raw_dump(content  : str = '',
             filename : Union[str, list, tuple] = None,
             encoding : str = 'utf-8',
             verify   : bool = True) -> None:
  '''
  Simple file dump.

  Parameters
  ----------
  content : str = ''
    Data to dump.

  filename : Union[str, list, tuple]
    File to dump to.

  encoding : str = 'utf-8'
    Encoding to utilize.

  verify : bool = True
    Verify dump success.
  '''
  filename = path_to_absolute(filename, verify=False)

  # generate nested directory structure as required
  dir_ensure_extant(filename, no_strip=False, verify=True)

  with open(filename, 'w', encoding=encoding) as filehandle:
    filehandle.write(content)
  if verify:
    path_is_extant(filename, fail_on_missing=True)

def file_ensure_extant(filename  : Union[str, list, tuple] = None,
                       overwrite : bool = False) -> None:
  '''
  Given a filename ensure that:
    - It exists
    - Optionally erase content but retain file.

  Parameters
  ----------
  filename
  '''
  if (not path_is_extant(filename)) or (overwrite):
    raw_dump(filename=filename, content='')

def csv_load(filename       : Union[str, list, tuple],
             loadtxt_kwargs : Optional[dict]=None) -> NDArray:
  '''
  Load column / comma separated data.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Filename of csv data to load.

  loadtxt_kwargs : Optional[dict]=None
    Additional kwargs to pass to numpy.loadtxt

  Returns
  -------
  Data contained in file.
  '''
  filename = path_to_absolute(filename, verify=True)

  if loadtxt_kwargs is None:
    loadtxt_kwargs = {}

  try:
    return loadtxt(filename, **loadtxt_kwargs)
  except ValueError:
    return loadtxt(filename, skiprows=1, **loadtxt_kwargs)

def csv_dump(content        : NDArray,
             filename       : Union[str, list, tuple],
             savetxt_kwargs : Optional[dict]=None) -> None:
  '''
  Save column / comma separated data.

  Parameters
  ----------
  content : NDArray
    Data to dump.

  filename : Union[str, list, tuple]
    Filename of csv data to save to.

  savetxt_kwargs : Optional[dict]=None
    Additional kwargs to pass to numpy.savetxt
  '''
  filename = path_to_absolute(filename, verify=False)

  # generate nested directory structure as required
  dir_ensure_extant(filename, no_strip=False, verify=True)

  if savetxt_kwargs is None:
    savetxt_kwargs = {}

  savetxt(filename, content, **savetxt_kwargs)

def tpl_parse(src_filename      : Union[str, list, tuple],
              tar_filename      : Union[str, list, tuple],
              replacement_rules : dict,
              delimiters        : Sequence[str] = ("[[", "]]")) -> str:
  """
  Convenience function for parsing templates populating with replacement rules
  and writing an output filename.

  Parameters
  ----------
  src_filename : Union[str, list, tuple]
    Filename of the template.

  tar_filename : Union[str, list, tuple]
    Output filename where the processed template is to be written.

  replacement_rules : dict
    Rules to apply for replacement.

  delimiters : Sequence[str] = ("[[", "]]")
    Delimiters to use for searching and replacing within the template file.

  Returns
  -------
  Parsed filename that was written.
  """
  raw_tpl = raw_load(src_filename)

  parsed_cmp_tpl = str_delimiter_replace(
    raw_tpl, replacement_rules, delimiters=delimiters
  )

  fn_res_cmp = path_to_absolute(tar_filename, verify=False)
  raw_dump(parsed_cmp_tpl, fn_res_cmp)

  return fn_res_cmp

#
# :D
#
