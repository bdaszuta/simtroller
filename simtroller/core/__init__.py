"""
 ,-*
(_)

@author: Boris Daszuta
@function: Collect core functionality of `simtroller` here.
"""
from simtroller.core.numba import (JITI, JITI_NC, JIT, JIT_NC)

from simtroller.core.miscellaneous import (time_get_current,
                                           hash_generate,
                                           time_sleep)

from simtroller.core.parallelism import (parallel_pool_start,
                                         parallel_pool_apply_async,
                                         parallel_pool_shell_run_scripts_async)

from simtroller.core.serialization import (object_serialize,
                                           object_deserialize,
                                           object_serialized_call)

from simtroller.core import primitives
from simtroller.core import io
from simtroller.core import numerical

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("numba"))
exec(_import_clear("miscellaneous"))
exec(_import_clear("parallelism"))
exec(_import_clear("serialization"))
# =============================================================================

#
# :D
#
