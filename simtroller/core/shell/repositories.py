#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Shell interaction.
"""
###############################################################################
# python imports

from subprocess import CalledProcessError

# pylint: disable=unused-import
from typing import Any, TYPE_CHECKING, Union, Optional

# package imports
from simtroller.core.shell.interact import rsync
from simtroller.core.shell.printing import print_signal
from simtroller.core.miscellaneous import hash_generate

###############################################################################

def repository_deploy(dir_source          : Union[str, list, tuple],
                      dir_target          : Union[str, list, tuple],
                      tag_hash_generate   : bool=True) -> dict:
  """
  Convenience function for deploying a repository

  Parameters
  ----------
  dir_source : Union[str, list, tuple]
    Directory to use as source of repository.

  dir_target : Union[str, list, tuple]
    Directory to use for target of deployment.

  tag_hash_generate : bool=True
    Control whether a filename in the form of a hash is generated.

  Returns
  -------
  info_rep : dict
    Recovered information about the repository. Empty if none found.

  Notes
  -----
  If target directory already exists then this function does not touch its
  contents.
  """
  from simtroller.core.io import (file_ensure_extant,
                                  path_is_extant,
                                  path_to_absolute)

  dir_rep = path_to_absolute(dir_source)

  if not path_is_extant(dir_rep):
    raise RuntimeError("Source repository does not exist.")

  try:
    # an error may occur if it is not a git repo.
    info_rep = repository_get_info(dir_rep)
  except CalledProcessError:
    info_rep = {}

  # deploy repository to target (strips git meta)
  if not path_is_extant(dir_target):
    rsync(dir_rep, dir_target)

    if tag_hash_generate:
      # make a tag based on repo. information in target
      if len(info_rep) > 0:
        tag_hash = info_rep["HEAD_hash"]
      else:
        tag_hash = hash_generate(info_rep)

      file_ensure_extant([dir_target, tag_hash])

  else:
    dir_target = path_to_absolute(dir_target)
    print_signal(f"Something already deployed at {dir_target}; ignoring..")

  return info_rep

def repository_get_info(repository_dir : Union[str, list, tuple]) -> dict:
  '''
  Scrape git repository information.

  Parameters
  ----------
  repository_dir : Union[str, list, tuple]
    Point to the repository of interest.

  Returns
  -------
  info : dict
    Information about current branch and hash of commit.
  '''
  # pylint: disable=import-outside-toplevel
  # pylint: disable=cyclic-import
  from simtroller.core.io import (path_to_absolute, path_cwd_get, path_cwd_set)
  from simtroller.core.shell.interact import (shell_interact, )

  rdir = path_to_absolute(repository_dir, verify=True)

  # TODO: make a context for this
  cur_dir = path_cwd_get()
  path_cwd_set(rdir, signal=False)

  git_cmd_res = shell_interact(['git', 'rev-parse', 'HEAD'],
                               method='subprocess_run',
                               capture_output=True)
  git_hash = git_cmd_res.replace('\n', '')

  git_cmd_res = shell_interact(['git', 'rev-parse', '--abbrev-ref', 'HEAD'],
                               method='subprocess_run',
                               capture_output=True)
  branch = git_cmd_res.replace('\n', '')

  path_cwd_set(cur_dir, signal=False)
  info = {'branch': branch, 'HEAD_hash': git_hash}
  return info

#
# :D
#
