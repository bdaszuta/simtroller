"""
 ,-*
(_)

@author: Boris Daszuta
@function: Functions for interacting with the shell / stdout.
"""
from simtroller.core.shell.printing import (
  print_warning,
  print_signal,
  print_info,
  print_color
)

from simtroller.core.shell.interact import (
  shell_interact,
  shell_run_script,
  rsync,
  shell_scrape_env,
)

from simtroller.core.shell.repositories import (
  repository_deploy,
  repository_get_info
)

# reduce import spam in interpreter: compat with pytest -----------------------
# pylint: disable=exec-used
from simtroller.core.miscellaneous import (_import_clear, )
exec(_import_clear("printing"))
exec(_import_clear("interact"))
exec(_import_clear("repositories"))
# =============================================================================

#
# :D
#
