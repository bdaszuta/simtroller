#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Shell interaction.
"""
###############################################################################
# python imports
import os as _os
import subprocess as _sub

from typing import (Any, TYPE_CHECKING, Union, # pylint: disable=unused-import
                    Optional, Sequence)

# package imports
from simtroller.core.primitives import (list_like, )
###############################################################################

def shell_interact(cmd             : Union[str, list, tuple],
                   method          : str='subprocess_run',
                   capture_output  : bool=True,
                   suppress_output : bool=True) -> Any:
  '''
  Interact with system via shell / subprocess commands.

  Parameters
  ----------
  cmd : Union[str, list, tuple]
    Command to execute.

  method : str = 'subprocess_run'
    One of {'subprocess_run', 'os_system'}.

  capture_output : bool = True
    If using 'subprocess_run' control if output is retained as return.

  suppress_output : bool = True
    If using 'os_system' control whether output is printed.

  Returns
  -------
  Output subject to above settings. In the case of 'os_system' status is
  returned.
  '''
  cmd = list_like(cmd)

  if method == 'subprocess_run':
    if capture_output:
      result = _sub.run(cmd, capture_output=True, check=True)
      return result.stdout.decode('ascii')

    return _sub.run(cmd, capture_output=False, check=True)

  if method == 'os_system':
    if suppress_output:
      cmd.extend([">/dev/null"])
    return _os.system(' '.join(cmd))

  raise ValueError("Unknown method selected.")

def shell_run_script(filename        : Union[str, list, tuple],
                     script_args     : Optional[Union[str,Sequence[str]]]=None,
                     suppress_output : bool=False,
                     shell           : str="bash") -> int:
  '''
  Run shell script with os.system.

  Parameters
  ----------
  filename : Union[str, list, tuple]
    Script to execute.

  script_args : Optional[Union[str,Sequence[str]]]=None
    Optionally pass script arguments.

  suppress_output : bool = True
    If control whether output is printed.

  Returns
  -------
  End status of script.
  '''
  # pylint: disable=import-outside-toplevel
  # pylint: disable=cyclic-import
  from simtroller.core.io import (path_to_absolute, )

  cmd = shell + " " + path_to_absolute(filename, verify=True)

  if script_args is not None:
    if isinstance(script_args, str):
      cmd = cmd + " " + script_args
    else:
      cmd = cmd + " ".join(script_args)

  if suppress_output:
    cmd += " >/dev/null"

  return _os.system(cmd)

def rsync(source      : Union[str, list, tuple]=None,
          destination : Union[str, list, tuple]=None,
          excludes    : Sequence[str]=(
            ".git/",
            "__pycache__/",
            "*.pyc",
            ".pytest_cache/",
            ".mypy_cache/",
          ),
          extra_args  : Sequence[str]=None,
          suppress_output : bool=False) -> None:
  '''
  Recursive copy of directory via rsync - optional excludes / arguments.

  Parameters
  ----------
  source : Union[str, list, tuple] = None
    Specification of source directory.

  destination : Union[str, list, tuple] = None
    Specification of destination directory.

  excludes : Sequence[str] = (
    ".git/",
    "__pycache__/",
    "*.pyc")
    Optional exclusions to apply.

  extra_args: Sequence[str] = None
    Additional arguments to pass to rsync.

  suppress_output : bool = False
    Control whether output is suppressed.
  '''
  # pylint: disable=import-outside-toplevel
  # pylint: disable=cyclic-import
  from simtroller.core.io import (path_to_absolute, dir_generate)

  source = path_to_absolute(source, verify=True)
  destination = path_to_absolute(destination, verify=False)

  dir_generate(destination, verify=True, signal=False)

  cmd = ['rsync', '-avr']

  if excludes is not None:
    excludes = list_like(excludes)

    for ex in excludes:
      cmd.append("--exclude=" + ex + " ")

  if extra_args is not None:
    extra_args = list_like(extra_args)

    for extra_arg in extra_args:
      cmd.append(extra_arg + " ")

  cmd.extend([_os.path.join(source, '*'),
              destination])

  shell_interact(cmd, method='os_system',
    capture_output=False, suppress_output=suppress_output)

def shell_scrape_env(filter=[]):
  """
  Convenience function for extraction of shell variables; optionally filter to
  a subset based on string content.

  TODO:
  Use regex as this is a bit brittle.
  """
  env = set(shell_interact("env").split("\n"))

  if len(filter) > 0:
    parsed = []
    for f_element in filter:
      for element in env:
        if f_element in element:
          parsed.append(element)

    env = set(parsed)

  to_ret = {}
  for element in env:
    var, defn = element.split("=")
    to_ret[var] = defn

  return to_ret

#
# :D
#
