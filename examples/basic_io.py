#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Show usage of basic input / output
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

dir_data = "/tmp/simtroller_test"

# ensure that we have the chosen directory structure (can also use e.g. list)
st.core.io.dir_generate(dir_data,
                        signal=True)

# as raw: ---------------------------------------------------------------------
data = "Test text"
st.core.io.raw_dump(content=data, filename=[dir_data, "as_raw"])
loaded_raw_data = st.core.io.raw_load(filename=[dir_data, "as_raw"])

assert data == loaded_raw_data

# overwrite with null
st.core.io.file_ensure_extant([dir_data, "as_raw"], overwrite=True)

assert st.core.io.raw_load(filename=[dir_data, "as_raw"]) == ''

# as pickle: ------------------------------------------------------------------
data_dict = {"key": 1}
st.core.io.pickle_dump(data_dict, filename=[dir_data, "as.pickle"])
loaded_pickle = st.core.io.pickle_load(filename=[dir_data, "as.pickle"])

assert data_dict == loaded_pickle

# as JSON (here we can dump numpy arrays also): -------------------------------
data_json = {
  "setting": 2,
  "block": {
    "alpha": 2,
    "beta": "val"
  },
  "data": np.eye(3, dtype=np.float64)
}
st.core.io.json_dump(data_json, filename=[dir_data, "data.json"])
loaded_json = st.core.io.json_load(filename=[dir_data, "data.json"])

# check the same (note conversion)
assert np.allclose(data_json["data"], np.eye(3, dtype=np.float64))
# check remaining
data_json.pop("data")
loaded_json.pop("data")

assert data_json == loaded_json

#
# :D
#
