#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ hdf5 files and interpolate.
"""
###############################################################################
# python imports
import numpy as np
import matplotlib.pyplot as plt

# package imports
import simtroller as st
###############################################################################

# specify directory containing simulation output in athdf

dir_base = "/mnt/nimbus/_Repositories/numerical_relativity"
dir_base += "/athena/gr_athena/development/outputs/z4c_c_op/"
tag_sim = "one_puncture"

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])

# -----------------------------------------------------------------------------
# inspect athdf to get the type of slices stored

pool = st.core.parallel_pool_start() # spin up a worker pool

# get slice variety for each hdf5
def get_slice_variety(filename,
                      x1_label="x1f",
                      x2_label="x2f",
                      x3_label="x3f"):
  x1 = st.core.io.hdf5_dataset_get(filename, dataset_name=x1_label)
  x2 = st.core.io.hdf5_dataset_get(filename, dataset_name=x2_label)
  x3 = st.core.io.hdf5_dataset_get(filename, dataset_name=x3_label)

  return st.extensions.gra.coordinates_identify_slice(x1=x1, x2=x2, x3=x3)

slice_variety = st.core.io.hdf5_filenames_apply_parser(
  fns_athdf, get_slice_variety, parallel_pool=pool)

Attributes = st.core.io.hdf5_file_get_attr(fns_athdf[0])
VariableNames = st.core.io.hdf5_file_get_attr(fns_athdf[0], "VariableNames")
Time = st.core.io.hdf5_file_get_attr(fns_athdf[0], "Time")

# -----------------------------------------------------------------------------
# interpolate in 1d

# find an example file
for ix, el in enumerate(slice_variety):
  if el == (1, "x1"):
    break

NGHOST = 4
fn = fns_athdf[ix]
adm_gxx = st.core.io.hdf5_dataset_get(fn, dataset_name="hydro")[0]
adm_gxx_C = np.squeeze(adm_gxx)[:, NGHOST:-(NGHOST-1)]
x1f = st.core.io.hdf5_dataset_get(fn, dataset_name="x1f")[:, NGHOST:-NGHOST]

# interpolate (scipy)
N_I = 1000
x_I, adm_gxx_I = st.core.numerical.grid_1d_interpolate_uniform_scipy(
  x1f, adm_gxx_C, N_I)

# interpolate (barycentric / per MeshBlock)
# adm_gxx_I = st.core.numerical.grid_1d_interpolate(
#   x1f[ix_MB], adm_gxx_C[ix_MB], x_I, d=d)

# interpolate (barycentric / globally MeshBlock-by-MeshBlock)
d = 6 # order of interpolation scheme
x_I = np.linspace(x1f.min(), x1f.max(), num=N_I)
F_I = st.extensions.gra.interpolate_1d_MeshBlock_field(x1f, adm_gxx_C,
                                                       x_I, d=d)


# comparison block
plt.plot(x1f.flatten(), adm_gxx_C.flatten(), "-or", markersize=1.1,
         label="MB")
plt.plot(x_I, adm_gxx_I, "-ob", markersize=1.1, label="I_scipy")
plt.plot(x_I, F_I, "-xg", markersize=1.1, label="I")
plt.legend()
plt.show()

# -----------------------------------------------------------------------------
# interpolate in 2d

# find an example file
for ix, el in enumerate(slice_variety):
  if el == (2, "x1x2"):
    break

fn = fns_athdf[ix]
adm_gxx = st.core.io.hdf5_dataset_get(fn, dataset_name="hydro")[0]

adm_gxx_C = st.core.numerical.ndarray_dim_order_exchange(
  np.squeeze(adm_gxx),
  num_skip_dim=1)[:, NGHOST:-(NGHOST-1), NGHOST:-(NGHOST-1)]

x1f = st.core.io.hdf5_dataset_get(fn, dataset_name="x1f")[:, NGHOST:-NGHOST]
x2f = st.core.io.hdf5_dataset_get(fn, dataset_name="x2f")[:, NGHOST:-NGHOST]

N_I = 256
x_I = np.linspace(x1f.min(), x1f.max(), num=N_I)
y_I = np.linspace(x2f.min(), x2f.max(), num=N_I // 2)

F_I = st.extensions.gra.interpolate_2d_MeshBlock_field(x1f, x2f, adm_gxx_C,
                                                       x_I, y_I, d=d)

plt.pcolormesh(
  *tuple(st.core.primitives.sequence_generator_flatten(
    (np.meshgrid(x_I, y_I, indexing="ij"), F_I)))
)
plt.show()

# -----------------------------------------------------------------------------
# interpolate in 3d

# find an example file
for ix, el in enumerate(slice_variety):
  if el == (3, "x1x2x3"):
    break

fn = fns_athdf[ix]
adm_gxx = st.core.io.hdf5_dataset_get(fn, dataset_name="hydro")[0]

adm_gxx_C = st.core.numerical.ndarray_dim_order_exchange(
  np.squeeze(adm_gxx),
  num_skip_dim=1)[:,
                  NGHOST:-(NGHOST-1),
                  NGHOST:-(NGHOST-1),
                  NGHOST:-(NGHOST-1)]

x1f = st.core.io.hdf5_dataset_get(fn, dataset_name="x1f")[:, NGHOST:-NGHOST]
x2f = st.core.io.hdf5_dataset_get(fn, dataset_name="x2f")[:, NGHOST:-NGHOST]
x3f = st.core.io.hdf5_dataset_get(fn, dataset_name="x3f")[:, NGHOST:-NGHOST]

x_I = np.linspace(x1f.min(), x1f.max(), num=64)
y_I = np.linspace(x2f.min(), x2f.max(), num=64)
z_I = np.linspace(x3f.min(), x3f.max(), num=32)

F_I = st.extensions.gra.interpolate_3d_MeshBlock_field(x1f, x2f, x3f, adm_gxx_C,
                                                       x_I, y_I, z_I, d=d)

# 3d data can be slow to process, we can use the parallel pool
def process_3d(x1f, x2f, x3f, F, x_I, y_I, z_I, d=d, chunk_z=10):
  pres = [st.core.parallel_pool_apply_async(
    pool,
    st.extensions.gra.interpolate_3d_MeshBlock_field,
    (x1f, x2f, x3f, F, x_I, y_I,
     z_I[i * chunk_z:((i+1) * chunk_z
                     if i < z_I.size // chunk_z - 1
                     else z_I.size)]),
    kwds={"d": d},
    method="dill") for i in range(z_I.size // chunk_z)]

  F_I = np.zeros((x_I.size, y_I.size, z_I.size), dtype=F.dtype)

  for i, res in enumerate(pres):
    F_I[:, :, i * chunk_z:((i+1) * chunk_z
              if i < z_I.size // chunk_z - 1
              else z_I.size)] = res.get()

  return F_I

pF_I = process_3d(x1f, x2f, x3f, adm_gxx_C, x_I, y_I, z_I, d=d, chunk_z=10)

assert np.allclose(F_I, pF_I)

#
# :D
#
