#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Compare implemented Barycentric interpolators performance
on Runge's prototypical example.
"""
###############################################################################
# python imports
import numpy as np
import matplotlib.pyplot as plt

# package imports
import simtroller as st
###############################################################################

a, b = -2.5, 2.5
N_c, N_f = 32, 512

x_c = np.linspace(a, b, num=N_c)
x_f = np.linspace(a, b, num=N_f)

f_c = 1 / (1 + x_c ** 2)
f_I_BT = st.core.numerical.grid_1d_interpolate(x_c, f_c, x_f, method="BT")
f_I_FH = st.core.numerical.grid_1d_interpolate(x_c, f_c, x_f, method="FH", d=6)

plt.plot(x_f, f_I_BT, "--or", markersize=0.4, label="BT")
plt.plot(x_f, f_I_FH, "--ok", markersize=0.4, label="FH")
plt.plot(x_c, f_c, "--ob", markersize=2.7, label="Coarse, sampled")

plt.legend()
plt.show()

#
# :D
#
