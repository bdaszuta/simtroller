#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Show usage of directory list / filters
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

# directory of package
dir_simtroller = st.package_path

dir_contents = st.core.io.dir_list(
  dir_simtroller, recursive=True
)

# filter out directories and retain python source
dir_files_only = st.core.io.dir_filter(
  dir_contents, mode="directories", extensions=[".py"]
)

# filter out files and retain only directories
dir_only = st.core.io.dir_filter(dir_contents, mode="files")


#
# :D
#
