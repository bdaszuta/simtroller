#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ hdf5 files containing CC data without ghosts.

Explanation:

The guts of the interpolation function goes as follows:
- Iterate over all MeshBlocks
  - Check whether the ordinate ranges defined by each meshblock has a non-zero
  - intersection with the target grid
  - If yes, do required interpolation for those points
  - Record in an auxiliary, mask array, to deal with any meshblock overlaps
    (to deal with ghosts / VC if we were in that situation)

Thus, if we have CC without ghosts then it can be the case that some points
will fall between the cell-centers of two adjacent Meshblocks (and hence the
ranges checked for intersection in the above), and therefore such points never
get interpolated; furthermore, in this latter, the multiplicity mask will be 0
and division by it will yield NAN.
"""
###############################################################################
# python imports
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from numpy import (abs, argsort, array, float64, linspace,
                   log10, logical_and, logical_not,
                   meshgrid, sign, sqrt)

# package imports
import simtroller as st
###############################################################################

st.extensions.plot_set_defaults()

def narray_from_range(ranges, N_I):
  # given range specification and num. of interp. samples, make grids

  gen_N_I = (N for N in N_I)

  ret = []
  for rix, ran in enumerate(ranges):
    if isinstance(ran, tuple):
      ret.append(linspace(*ran, num=next(gen_N_I)))
    else:
      ret.append(array([ran, ], dtype=float64))
  return tuple(ret)

###############################################################################
# specify directory containing simulation output in athdf and settings

dir_base = "${NR_DATA}/GR-Athena++/_debugging/"
tag_sim = "simtroller_interp"

# interpolation and axes window
var = "dens"
N_B = (16, 16)
sampling = ("x1v", "x2v")
NGHOST = None
range = None
iterate = 0
interpolate = True
N_I = (256, 256)

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])
fns_txt = st.core.io.dir_filter(dir_sim_list,
                                mode="directories",
                                extensions=[".txt"])


# extract general information from files
dir_data = st.core.io.path_to_absolute([dir_base, tag_sim])
sim_data_meta = st.extensions.gra.scrape_dir_athdf(
  dir_data, N_B
)

var_info = sim_data_meta.get_var_info(var, sampling, NGHOST is not None)

out, _, dg, _ = var_info
Time = sim_data_meta.get_iter_time(out, iterate=0)

# retain some ghosts as this allows better interpolation when required
strip_dg = 0 # number of ghosts to strip

xyz_s = sim_data_meta.get_grid(out,
                                sampling=sampling,
                                iterate=iterate,
                                strip_dg=strip_dg)

# raw data on the sampling
f_R = sim_data_meta.get_var(
  var,
  sampling,
  iterate=iterate,
  with_ghosts=NGHOST is not None,
  strip_dg=strip_dg
)

# sample (f)ace centered for grid structure generation
opt_sampling_f = tuple(smp[:2] + "f" for smp in sampling)

# grid structure (i.e. (f)ace-centered without ghosts)
xyz_G = sim_data_meta.get_grid(out,
                               sampling=opt_sampling_f,
                               iterate=iterate,
                               strip_dg=dg)

xyz_L = sim_data_meta.get_grid_levels(out, iterate=iterate)

# retain grid spacing extrema
dx_extrema = [st.extensions.gra.coordinates_get_dx_extrema(gr)
              for gr in xyz_G]

# -----------------------------------------------------------------------------
# interpolation (or not)
if interpolate:

  # infer range if not provided
  if range is None:
    range = tuple(
      (ax.min(), ax.max()) for ax in xyz_G
    )

  # perform interpolation as required by range
  xyz_I = narray_from_range(range, N_I)

  # slice away extraneous entry (VC dump fix)
  xyz_s_ = [xyz_[:,:f_R.shape[xix+1]] for xix, xyz_ in enumerate(xyz_s)]

  f_I = st.extensions.gra.interpolate_2d_MeshBlock_field(
    *xyz_s_, f_R,
    *xyz_I, method="FH"
  ).squeeze()

# Because we don't have ghosts, this injects NAN
#
# This can be fixed by low-order (d=1) extrapolation

def extend_ord(ord_, next=1):
  e_ord_ = np.zeros((ord_.shape[0], ord_.shape[1] + 2 * next),
                    dtype=ord_.dtype)
  e_ord_[:,next:-next] = ord_
  dord_ = (e_ord_[:,next+1] - e_ord_[:,next])[:,None]

  dordl_ = -(np.arange(1,next+1)[::-1])[None,:] * dord_ + ord_[:,0][:,None]
  dordr_ = (np.arange(1,next+1))[None,:] * dord_ + ord_[:,-1][:,None]

  e_ord_[:,:next]  = dordl_
  e_ord_[:,-next:] = dordr_
  return e_ord_

# extend to target
next = 1
e_xyz_s_ = [
  extend_ord(ord_, next) for ord_ in xyz_s_
]

# extrapolate on each MeshBlock (local)
ef_R = np.array(
  [
    st.core.numerical.grid_2d_interpolate(
      xyz_s_[0][ix], xyz_s_[1][ix],
      f_R[ix],
      e_xyz_s_[0][ix], e_xyz_s_[1][ix],
      method="FH",
      d=1,
    ) for ix in np.arange(f_R.shape[0])
  ]
)

# now try using extrapolated data for the global interp
ef_I = st.extensions.gra.interpolate_2d_MeshBlock_field(
  *e_xyz_s_, ef_R,
  *xyz_I, method="FH", d=4
).squeeze()

# inspect:
fig, ax = plt.subplots(1, 2, sharey=True)

extent = [xyz_I[0].min(), xyz_I[0].max(),
          xyz_I[1].min(), xyz_I[1].max()]

kw = {
  "origin": "lower",
  "extent": extent
}

ax[0].imshow(f_I, **kw)
ax[1].imshow(ef_I, **kw)

fig.tight_layout()
fig.subplots_adjust(wspace=0.)

plt.show()


#
# :D
#
