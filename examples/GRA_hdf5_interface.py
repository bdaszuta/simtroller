#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ hdf5 files.
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

# specify directory containing simulation output in athdf

dir_base = "/run/media/orbis/k-town/_Data/GR-Athena++/bns"
tag_sim = "d001_VLR_PLM_RK4_CFL0.50"

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])

# -----------------------------------------------------------------------------
# process multiple hdf5 files by attribute

def hdf5_has_early_time_rho(filename, T_cut=2):
  # composite condition: contains b"rho" variable and Time < 2

  has_rho = b"rho" in st.core.io.hdf5_file_get_attr(filename, "VariableNames")
  is_early = T_cut > st.core.io.hdf5_file_get_attr(filename, "Time")

  result = has_rho and is_early

  return result

# filtered athdf that satisfy condition
fns_early_time_rho = [
  filename for filename in fns_athdf if hdf5_has_early_time_rho(filename)
]

# -----------------------------------------------------------------------------
# process multiple hdf5 files by attribute: interfaces

pool = st.core.parallel_pool_start()

fns_early_time_rho_serial = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=hdf5_has_early_time_rho,
  condition_negate=True  # files that satisfy ^ are dropped so negate
)

# reuse the above pool and call interface

fns_early_time_rho_parallel = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=hdf5_has_early_time_rho,
  condition_negate=True, # files that satisfy ^ are dropped so negate
  parallel_pool=pool
)


# TODO: EXTEND

#
# :D
#
