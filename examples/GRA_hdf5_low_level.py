#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Show usage of core functionality to interact with GR-Athena++ data.

For simpler usage see GRA_hdf5_interface
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

# specify directory containing simulation output in athdf

dir_base = "/run/media/orbis/k-town/_Data/GR-Athena++/bns"
tag_sim = "d001_VLR_PLM_RK4_CFL0.50"

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])

# -----------------------------------------------------------------------------
# get attribute values from an hdf5
Attributes = st.core.io.hdf5_file_get_attr(fns_athdf[0])
VariableNames = st.core.io.hdf5_file_get_attr(fns_athdf[0], "VariableNames")
Time = st.core.io.hdf5_file_get_attr(fns_athdf[0], "Time")

# -----------------------------------------------------------------------------
# process multiple hdf5 files by attribute

def hdf5_has_early_time_rho(filename, T_cut=2, idx_filename=None):
  # composite condition: contains b"rho" variable and Time < 2

  has_rho = b"rho" in st.core.io.hdf5_file_get_attr(filename, "VariableNames")
  is_early = T_cut > st.core.io.hdf5_file_get_attr(filename, "Time")

  result = has_rho and is_early

  # for parallelism
  if idx_filename is not None:
    return idx_filename, result

  return result

# filtered athdf that satisfy condition
fns_early_time_rho = [
  filename for filename in fns_athdf if hdf5_has_early_time_rho(filename)
]

# -----------------------------------------------------------------------------
# process multiple hdf5 files by attribute: in parallel (1)
# This is the logic used within interfaces

pool = st.core.parallel_pool_start()

apply_async_tasks = [pool.apply_async(hdf5_has_early_time_rho, args=(filename,),
                                      kwds={"idx_filename": fidx})
                     for fidx, filename in enumerate(fns_athdf)]

aat_results = [apply_async.get() for apply_async in apply_async_tasks]
sorted_aat_results = sorted(aat_results,
                            key=lambda result_tuple: result_tuple[1])

# filtered athdf that satisfy condition
fns_early_time_rho_par = [
  fns_athdf[idx]
  for idx, result in sorted_aat_results if result
]

# -----------------------------------------------------------------------------
# process multiple hdf5 files by attribute: interfaces

fns_early_time_rho_serial = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=hdf5_has_early_time_rho,
  condition_negate=True  # files that satisfy ^ are dropped so negate
)

# reuse the above pool and call interface

fns_early_time_rho_parallel = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=hdf5_has_early_time_rho,
  condition_negate=True, # files that satisfy ^ are dropped so negate
  parallel_pool=pool
)

assert fns_early_time_rho_serial == fns_early_time_rho_parallel

#
# :D
#
