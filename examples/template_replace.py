#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Show functionality for template replacements.

Useful for parametrized parfiles.
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

# locate source examples
dir_templates = st.core.io.path_to_absolute(
  [st.package_path, "..", "examples", "templates"]
)

fn_template = "example_GRA_parfile"

# load the template
tpl = st.core.io.raw_load([dir_templates, fn_template])

# extract between delimiters
tpl_vars = st.core.primitives.str_delimiter_extract(
  tpl, delimiters=("[[", "]]")
)

# replacement rules as dict
rep_ru = {
  "REPO_HASH": st.core.hash_generate("mock data"),
  "par_val": 2,
  "ARB_LABEL": -22.2,
  "DYN_COM": "added"
}

# replace and print
tpl_parsed = st.core.primitives.str_delimiter_replace(
  tpl, rep_ru, delimiters=("[[", "]]")
)

print("Parsed template --------------:")
print(tpl_parsed)


#
# :D
#
