#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Show how to deploy a repository to some location
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

dir_tar = "/tmp/simtroller_deploy"
dir_rep = "${repos}/numerical_relativity/simtroller"  # env var expanded

if not st.core.io.path_is_extant(dir_rep):
  raise RuntimeError("Source repository does not exist.")

info_rep = st.core.shell.repository_get_info(dir_rep)

st.core.shell.rsync(dir_rep, dir_tar)

# make a tag based on repo. information in target
st.core.io.file_ensure_extant(
  [dir_tar, st.core.hash_generate(info_rep)]
)

#
# :D
#
