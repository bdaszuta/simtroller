#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ scalar data
"""
###############################################################################
# python imports
import numpy as np
import matplotlib.pyplot as plt

# package imports
import simtroller as st
###############################################################################

pool = st.core.parallel_pool_start() # spin up a worker pool

# -----------------------------------------------------------------------------
# specify directory containing simulation output in athdf
dir_base = "${NR_DATA}/GR-Athena++/bns/"
tag_sim = "d001_fix_ELR_PLM/output_0d"

# get only scalar output
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_txt = st.core.io.dir_filter(dir_sim_list,
                                mode="directories",
                                extensions=[".txt"])

# -----------------------------------------------------------------------------
# parse input parameters
dir_par = "${NR_DATA}/GR-Athena++/bns/gra_par/"
fn_par = "d001_fix.Lorene_bns.PLM.SMR.ELR"

max_radii = 9
par_dict = st.extensions.gra.parameter_file_to_dict([dir_par, fn_par])
radii = st.extensions.gra.parameter_dict_get_extraction_radii(
  par_dict, max_radii=max_radii)

fn_waveform = st.core.io.path_strip(par_dict["z4c"]["extract_filename"],
                                    mode="absolute_path")

fns_waveforms = [fn for fn in fns_txt if fn_waveform in fn]

# -----------------------------------------------------------------------------
# sanitize raw waveforms (de-duplication in time); downsample (interpolated)

# serial:
# raw_psi = [
#   st.extensions.gra.scalars_waveforms_process(fn, deduplicate=True)
#   for fn in fns_waveforms
# ]

parallel_work = [
  st.core.parallel_pool_apply_async(
    pool,
    st.extensions.gra.scalars_waveforms_process,
    (filename, ),
    kwds={"deduplicate": True},
    method="dill")
  for filename in fns_waveforms
]

raw_psi = [
  job.get() for ix, job in enumerate(parallel_work)
]

N_I = 1024
T_I = np.linspace(0, 2400, num=N_I)

psi_I = [
  st.extensions.gra.scalars_waveforms_interpolate(
    raw_psi[ix], T_I,
    d=10, method="FH", parallel_pool=pool)
  for ix, R in enumerate(radii)
]

# -----------------------------------------------------------------------------
# shift and cut waveforms based on retarded time
M = 1
ix_R = 0

psi_u_I = []

for ix_R in range(len(radii)):
  u = st.extensions.waveforms.map_t_to_u(raw_psi[ix_R][:, 0], radii[ix_R], M)
  u_I = np.linspace(0, u.max(), num=N_I)

  raw_psi_u = np.hstack((u[:, None], raw_psi[ix_R][:, 1:]))
  psi_u_I.append(
    st.extensions.gra.scalars_waveforms_interpolate(raw_psi_u, u_I,
      d=10, method="FH", parallel_pool=pool)
  )

psi_u_I = np.array(psi_u_I)

# -----------------------------------------------------------------------------
# now do raw plot

st.extensions.plot_set_defaults()  # set appearance defaults

ix_R = 4
ix_22 = st.extensions.gra.scalars_waveforms_ix_map(2, 2,
  is_real_part=True, parsed_offset=True)

plt.figure(1)

plt.plot(psi_I[ix_R][:, 0], radii[ix_R] * psi_I[ix_R][:, ix_22])
plt.xlabel("T")
plt.ylabel("R x psi_22")

plt.figure(2)

plt.plot(psi_u_I[ix_R][:, 0], radii[ix_R] * psi_u_I[ix_R][:, ix_22])
plt.xlabel("u")
plt.ylabel("R x psi_22")

plt.show()

# -----------------------------------------------------------------------------
# compare bam

# dir_bam_invar = ("/run/media/orbis/k-town/_Data/GR-Athena++/bns/BAM_ref/" +
#                  "run/bam27res64/Invariants")

dir_bam_invar = ("${NR_DATA}/BAM" +
                 "/G2_I14vs14_D4R33_45km_Z4_WENOZ/Invariants/")

Rpsi4 = st.core.io.csv_load([dir_bam_invar, "Rpsi4mode22_r1.l2"],
                             loadtxt_kwargs={"skiprows": 2})

plt.plot(Rpsi4[:, 0], Rpsi4[:, 1], "-g")
plt.plot(psi_I[ix_R][:, 0], radii[ix_R] * psi_I[ix_R][:, ix_22], "-r")

plt.xlim([0, 2500])

plt.xlabel("T")
plt.ylabel("R x psi_22")
plt.show()

#
# :D
#
