#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ hdf5 file and make level plot; this is not the
fastest implementation.
"""
###############################################################################
# python imports
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

# package imports
import simtroller as st
###############################################################################

st.extensions.plot_set_defaults()
pool = st.core.parallel_pool_start() # spin up a worker pool

###############################################################################
# specify directory containing simulation output in athdf and settings

dir_base = "${NR_DATA}/GR-Athena++/calibration"
tag_sim = "ng4_N128_NL12_X3072"

# interpolation and axes window
N_I = 1024
d = 0   # set to 0 for raw plot
x_ab = (-100, 100)
y_ab = (-100, 100)
NGHOST = 4
var = "z4c.chi"
variety = "x1x2"
physical_only = True
min_lev = 3  # minimal level to plot
ix_fn = 4    # index of filename to use

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])

###############################################################################
# filter hdf5 to files that have "var" and "variety" slices
def hdf5_has_var_slice(filename, var=None, variety=variety):
  VariableNames = st.core.io.hdf5_file_get_attr(filename, "VariableNames")
  fund_var_labels = st.extensions.gra.variable_label_to_sampling_tuple(var)

  fund_var = [
    st.core.io.hdf5_dataset_get(filename, dataset_name=var_label)
    for var_label in fund_var_labels
  ]

  sl_dim, sl_variety = st.extensions.gra.coordinates_identify_slice(
    *fund_var)

  return (variety == sl_variety) and var.encode() in VariableNames

# do the filtering
fns_filtered = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=partial(hdf5_has_var_slice, var=var),
  condition_negate=True, # files that satisfy ^ are dropped so negate
  parallel_pool=pool
)

###############################################################################
# extract times from filtered files and interpolate trackers
def hdf5_get_time(filename):
  return st.core.io.hdf5_file_get_attr(filename, "Time")

# schedule the parsing
par_res = st.core.io.hdf5_filenames_apply_parser(
  fns_filtered, hdf5_get_time, parallel_pool=pool)

sliced_Time = np.array(
  list(st.core.primitives.sequence_generator_flatten(par_res))
)


###############################################################################
# extract and interpolate fields (2d) -----------------------------------------
filename = fns_filtered[ix_fn]

var_data, field_data = st.extensions.gra.hdf5_get_field(
  filename, var, physical_only=physical_only, NGHOST=NGHOST)


###############################################################################
# get variable information ----------------------------------------------------
info_vars = st.extensions.gra.hdf5_get_vars(filename)

# grid spacing
extrema_dx_a = st.extensions.gra.coordinates_get_dx_extrema(
  info_vars["xf"][0]
)

extrema_dx_b = st.extensions.gra.coordinates_get_dx_extrema(
  info_vars["xf"][1]
)

###############################################################################
# prepare figure
fig = plt.figure(1, figsize=(8.5, 8.5))

gs = fig.add_gridspec(1,1, height_ratios=(1, ), width_ratios=(1, ))
ax_im = fig.add_subplot(gs[0, 0])

# overlay Mesh Structure
st.extensions.gra.plot_2d_MeshBlock_structure(
  ax_im, filename, NGHOST=NGHOST, color="k", plot_kwargs={"linewidth": 0.5}
)

# color-map to distinguish levels
cols = [
  'black', 'darkred', 'red', 'darksalmon', 'pink', 'salmon', 'magenta',
  'darkgreen', 'yellow', 'green', 'cyan', 'lime'
]

cmap = ListedColormap(cols[:(info_vars["Levels"].max()-min_lev) + 1])

# prepare structure level data
level_data = np.zeros_like(field_data)
for ix in range(field_data.shape[0]):  # iteration over MeshBlocks
  cur_level = info_vars["Levels"][ix]
  level_data[ix, :, :] = cur_level

st.extensions.gra.plot_2d_raw(
  ax_im, *var_data, level_data, cmap=cmap, vmin=min_lev
)

ax_im.set_xlim(x_ab)
ax_im.set_ylim(y_ab)

X_label = info_vars["xf_labels"][0]
Y_label = info_vars["xf_labels"][1]

ax_im.set_xlabel(X_label)
ax_im.set_ylabel(Y_label)

title = (f"Time={sliced_Time[ix_fn]} " +
         f"dx_extr[{X_label}]: {extrema_dx_a} " +
         f"dx_extr[{Y_label}]: {extrema_dx_b} " +
         f"# MeshBlock: {field_data.shape[0]}")

ax_im.set_title(title)

plt.subplots_adjust(top=0.967,
                    bottom=0.056,
                    left=0.072,
                    right=0.958,
                    hspace=0.2,
                    wspace=0.2)
plt.show()

#
# :D
#
