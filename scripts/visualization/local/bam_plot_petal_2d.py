#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse BAM csv files and interpolate.
"""
###############################################################################
# python imports
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# package imports
import simtroller as st
###############################################################################

st.extensions.plot_set_defaults()

###############################################################################

# -----------------------------------------------------------------------------
# specify directory containing simulation and settings

dir_sim = "${NR_DATA}/GR-Athena++/bns/BAM_ref/run/bam27res64/"
dir_sim_list = st.core.io.dir_list([dir_sim, "output_2d"], recursive=False)

var = "bssn_chi"
var = "grhd_rho"
var = "grhd_p"
var_slice = "xy"

fns = [fn for fn in dir_sim_list if var_slice in fn and var in fn]
fn_tracker = [dir_sim, "moving_puncture_distance.lxyz6"]

# interpolation and axes window
N_I = 1024
d = 0   # set to 0 for raw plot
d_slice = 1
x_ab = (-40, 40)
y_ab = (-40, 40)

r_0 = 0.5          # extrema disc radius
extrema_type = -1  # search for minima
extrema_type = +1  # search for maxima
scaling = "linear"
scaling = "log10"

T_cut = 2500  # throw data after this time

# -----------------------------------------------------------------------------
pool = st.core.parallel_pool_start() # spin up a worker pool

# Boxes can merge and therefore output times will vary between files;
# Therefore we accumulate per filename
parallel_work = {
  filename:
  st.core.parallel_pool_apply_async(
    pool,
    st.extensions.bam.csv_bam_load_times,
    (filename, ),
    kwds={"return_linenos": False},
    method="dill")
  for filename in fns
}

# process
T_data = {key: job.get() for key, job in parallel_work.items()}

# Load actual data
parallel_work = {
  filename:
  st.core.parallel_pool_apply_async(
    pool,
    st.extensions.bam.csv_bam_load,
    (filename, ),
    kwds={"reprocess_to_canonical": True},
    method="dill")
  for filename in fns
}

# process
frdata = {key: job.get() for key, job in parallel_work.items()}

# Re-process to: {Time: {filename: data}}
pdata = {}
for filename, dset in frdata.items():
  for T in T_data[filename]:
    st.core.primitives.dict_nested_set(
      (T, filename),
      pdata,
      dset[T]
    )

# trackers --------------------------------------------------------------------
trackers = st.extensions.bam.scalars_trackers_process(fn_tracker, T_cut=T_cut)

# down-sample to 2d data-slices we have data at
Time_array = np.sort(np.array(list(pdata.keys())))
ix_T_cut = np.argmin(np.abs(T_cut - Time_array))
Time_array = Time_array[:(ix_T_cut + 1)]

tra_1_I, tra_2_I = st.extensions.bam.scalars_trackers_interpolate(
  trackers, Time_array, method="FH", d=10)
# -----------------------------------------------------------------------------

###############################################################################
# make plots (w/ interpolation)

def make_petal_figure(data_dict, Time=None, var=None, x_ab=None, y_ab=None,
                      d=None, d_slice=None,
                      tra_1_I=None, tra_2_I=None, scaling=None):

  fig = plt.figure(1, figsize=(8.5, 8.5))

  gs = fig.add_gridspec(2,2, height_ratios=(5, 2), width_ratios=(2, 5))
  ax_im = fig.add_subplot(gs[0, 1])
  ax_cut_x = fig.add_subplot(gs[1, 1], sharex=ax_im)
  ax_cut_y = fig.add_subplot(gs[0, 0], sharey=ax_im)

  # settings
  x_I = np.linspace(*x_ab, num=N_I)
  y_I = np.linspace(*y_ab, num=N_I)

  # grid spacing
  extrema_dx = st.extensions.bam.coordinates_get_dx_extrema(data_dict)
  extrema_dx_a = tuple(extrema_dx[0, :])
  extrema_dx_b = tuple(extrema_dx[1, :])

  # interpolate 2d slice (needed for extrema search)
  var_I = st.extensions.bam.interpolate_2d_Box_in_Box_field(
    data_dict, x_I, y_I, d=d, method="FH"
  )

  # find extrema --------------------------------------------------------------
  ix_tr = np.argmin(np.abs(tra_1_I[:,0] - Time))
  x0_tr_1, y0_tr_1 = tra_1_I[ix_tr, 1:3]  # x-y from tracker at current time
  x0_tr_2, y0_tr_2 = tra_2_I[ix_tr, 1:3]  # x-y from tracker at current time

  # search based on tracker
  ix_1_max, jx_1_max = st.core.numerical.find_local_extrema_2d(
    x_I, y_I, var_I, x0_tr_1, y0_tr_1, r_0, extrema_type=extrema_type)

  ix_2_max, jx_2_max = st.core.numerical.find_local_extrema_2d(
    x_I, y_I, var_I, x0_tr_2, y0_tr_2, r_0, extrema_type=extrema_type)

  mx_1, my_1 = x_I[ix_1_max], y_I[jx_1_max]
  mx_2, my_2 = x_I[ix_2_max], y_I[jx_2_max]


  # slice X/Y to inferred extrema ---------------------------------------------
  if d_slice == 1:
    method_slice = "linear"
  else:
    method_slice = "FH"

  C_I_X_1 = st.extensions.bam.interpolate_2d_Box_in_Box_field(
    data_dict,
    x_I, np.array([my_1], dtype=x_I.dtype), d=d_slice, method=method_slice)

  C_I_Y_1 = st.extensions.bam.interpolate_2d_Box_in_Box_field(
    data_dict,
    np.array([mx_1], dtype=x_I.dtype), y_I, d=d_slice, method=method_slice)

  C_I_X_2 = st.extensions.bam.interpolate_2d_Box_in_Box_field(
    data_dict,
    x_I, np.array([my_2], dtype=x_I.dtype), d=d_slice, method=method_slice)

  C_I_Y_2 = st.extensions.bam.interpolate_2d_Box_in_Box_field(
    data_dict,
    np.array([mx_2], dtype=x_I.dtype), y_I, d=d_slice, method=method_slice)

  # plot fields ---------------------------------------------------------------
  if scaling == "linear":
    sf = lambda x: x
  elif scaling == "log10":
    sf = lambda x: np.log10(x)
  else:
    raise ValueError("Unknown scaling")

  if d == 0:
    st.extensions.bam.plot_2d_raw(ax_im, data_dict, cmap="inferno",
                                  lambda_apply=sf)
  else:
    st.extensions.gra.plot_2d_interpolated(
      ax_im, x_I, y_I, sf(var_I), cmap="inferno"
    )

  # overlay the grid structure
  st.extensions.bam.plot_2d_box_in_box_structure(ax_im, data_dict, color="w")

  ax_im.set_xlim([x_I.min(), x_I.max()])
  ax_im.set_ylim([y_I.min(), y_I.max()])

  # sliced petals
  ax_cut_x.plot(x_I, sf(C_I_X_1.flatten()), "-g", linewidth=0.8)
  ax_cut_y.plot(sf(C_I_Y_1.flatten()), y_I, "-c", linewidth=0.8)

  ax_cut_x.plot(x_I, sf(C_I_X_2.flatten()), "--g", linewidth=0.8)
  ax_cut_y.plot(sf(C_I_Y_2.flatten()), y_I, "--c", linewidth=0.8)


  # cosmetic ------------------------------------------------------------------
  # place extrema
  ax_im.plot(mx_1, my_1, 'o', color=[0, 1, 0], markersize=6)
  ax_im.text(mx_1 + 2, my_1-4, f"({mx_1:.2e},{my_1:.2e})",
            color=[0, 1, 0],
            fontsize=8)

  ax_im.plot(mx_2, my_2, 'o', color=[0, 1, 0], markersize=6)
  ax_im.text(mx_2 + 2, my_2-4, f"({mx_2:.2e},{my_2:.2e})",
            color=[0, 1, 0],
            fontsize=8)

  # place slicers
  ax_im.axhline(my_1, color="g", linewidth=0.8)
  ax_im.axvline(mx_1, color="c", linewidth=0.8)

  ax_im.axhline(my_2, color="g", linestyle="--", linewidth=0.8)
  ax_im.axvline(mx_2, color="c", linestyle="--", linewidth=0.8)

  # labels and metadata
  X_label = var_slice[0]
  Y_label = var_slice[1]

  ax_cut_x.set_xlabel(X_label)
  ax_cut_y.set_ylabel(Y_label)

  ax = fig.get_axes()[-1]

  fig.text(-0.5, 0.7, f"Time: {Time}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  fig.text(-0.5, 0.5, f"Field: {var}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  fig.text(-0.5, 0.3, f"dx_extr[{X_label}]: {extrema_dx_a}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  fig.text(-0.5, 0.1, f"dx_extr[{Y_label}]: {extrema_dx_b}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  # figure geometry etc
  divider_x = make_axes_locatable(ax_cut_x)
  cl_ax = divider_x.append_axes("right", size="5%", pad=0.02)
  cl_ax.set_visible(False)

  ax_im.xaxis.set(visible=False)
  ax_im.yaxis.set(visible=False)

  # deal with ticks
  ax_cut_x.yaxis.tick_right()
  ax_cut_y.xaxis.tick_bottom()

  for ax in fig.get_axes():
    ax.tick_params(direction="in")

  plt.subplots_adjust(wspace=0.01, hspace=0.05,
                      top=0.930, bottom=0.090,
                      left=0.090, right=0.930)

  plt.savefig(f"{var}_{scaling}_{Time:016.5f}.png")
  plt.clf()

  return Time

# distribute work to pool
par_res = [
  st.core.parallel_pool_apply_async(
    pool,
    make_petal_figure,
    (pdata[Time], Time),
    kwds={"var": var, "x_ab": x_ab, "y_ab": y_ab,
          "d": d, "d_slice": d_slice,
          "tra_1_I": tra_1_I, "tra_2_I": tra_2_I,
          "scaling": scaling},
    method="dill")
  for Time in Time_array
]

# process work
for ix, pr in enumerate(par_res):
  cur_Time = pr.get()
  print(cur_Time)

# Time = Time_array[2]
# data_dict = pdata[Time]
# make_petal_figure(data_dict, Time, var=var, x_ab=x_ab, y_ab=y_ab,
#   d=d, d_slice=d_slice, tra_1_I=tra_1_I, tra_2_I=tra_2_I,
#   scaling=scaling)


#
# :D
#
