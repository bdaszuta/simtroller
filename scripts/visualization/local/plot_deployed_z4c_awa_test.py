#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Plot the result of deployment/local/z4c_awa_test
"""
###############################################################################
# python imports
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# package imports
import simtroller as st
###############################################################################

st.extensions.plot_set_defaults()

###############################################################################
# specify directory containing simulation output in athdf and settings

dir_base = "/tmp/simtroller_deploy/awa/"
tag_sim = "data"

# dir_base = "${NR_DATA}/GR-Athena++/AwA/data"
# tag_sim = "robust_stability.FD_4_2_2.0_0.25"
# tag_sim = "robust_stability.GFD_Pade_4_2_2.0_0.25"
# tag_sim = "robust_stability.GFD_w_opt_4_2_2.0_0.25"

# tag_sim = "robust_stability.GFD_w_opt_ref_4_2_2.0_0.25"
# tag_sim = "robust_stability.GFD_Pade_ref_4_2_2.0_0.25"
# tag_sim = "robust_stability.FD_ref_4_2_2.0_0.25"

physical_only = True
NGHOST = 4
var = "con.H"
variety = "x1x2x3"

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])
fns_txt = st.core.io.dir_filter(dir_sim_list,
                                mode="directories",
                                extensions=[".txt"])

# -----------------------------------------------------------------------------
# inspect athdf to get the type of slices stored

pool = st.core.parallel_pool_start() # spin up a worker pool

###############################################################################
# filter hdf5 to files that have "var" and "variety" slices
def hdf5_has_var_slice(filename, var=None, variety=variety):
  VariableNames = st.core.io.hdf5_file_get_attr(filename, "VariableNames")
  fund_var_labels = st.extensions.gra.variable_label_to_sampling_tuple(var)

  fund_var = [
    st.core.io.hdf5_dataset_get(filename, dataset_name=var_label)
    for var_label in fund_var_labels
  ]

  sl_dim, sl_variety = st.extensions.gra.coordinates_identify_slice(
    *fund_var)

  return (variety == sl_variety) and var.encode() in VariableNames

# do the filtering
fns_filtered = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=partial(hdf5_has_var_slice, var=var),
  condition_negate=True, # files that satisfy ^ are dropped so negate
  parallel_pool=pool
)

###############################################################################
# extract times from filtered files and interpolate trackers
def hdf5_get_time(filename):
  return st.core.io.hdf5_file_get_attr(filename, "Time")

# schedule the parsing
par_res = st.core.io.hdf5_filenames_apply_parser(
  fns_filtered, hdf5_get_time, parallel_pool=pool)

sliced_Time = np.array(
  list(st.core.primitives.sequence_generator_flatten(par_res))
)

def parse_l_inf(filename, var, NGHOST=None, physical_only=True):
  var_data, field_data = st.extensions.gra.hdf5_get_field(
    filename, var, physical_only=physical_only, NGHOST=NGHOST)
  return np.abs(field_data).max()


# distribute work to pool
par_res = [
  st.core.parallel_pool_apply_async(
    pool,
    parse_l_inf,
    (filename, ),
    kwds={"var":var,
          "NGHOST": NGHOST,
          "physical_only": physical_only},
    method="dill")
  for filename in fns_filtered
]

# process work
vals = np.zeros(sliced_Time.size, dtype=np.float64)

for ix, pr in enumerate(par_res):
  vals[ix] = pr.get()
  print(sliced_Time[ix])

# make a plot
plt.semilogy(sliced_Time, vals, "-r")
plt.xlabel("Time")
plt.ylabel(f"max|{var}|")


# plt.show()

#
# :D
#
