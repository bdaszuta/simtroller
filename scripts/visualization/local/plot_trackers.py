#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ scalar data
"""
###############################################################################
# python imports
import numpy as np
import matplotlib.pyplot as plt

# package imports
import simtroller as st
###############################################################################

# specify directory containing simulation output in athdf

dir_base = "${NR_DATA}/GR-Athena++/bns/"
tag_sim = "d001_fix_ELR_PLM/output_0d"

# get only scalar output
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_txt = st.core.io.dir_filter(dir_sim_list,
                                mode="directories",
                                extensions=[".txt"])

# -----------------------------------------------------------------------------
# sanitize (de-deplication in time)

raw_tra_1 = st.extensions.gra.scalars_trackers_process(fns_txt[0],
                                                       deduplicate=True)
raw_tra_2 = st.extensions.gra.scalars_trackers_process(fns_txt[1],
                                                       deduplicate=True)

N_T_I = 1024

T_I = np.linspace(raw_tra_1[0,0], raw_tra_1[-1,0], num=N_T_I)
tra_1_I = st.extensions.gra.scalars_trackers_interpolate(
  raw_tra_1, T_I, method="FH", d=10)
tra_2_I = st.extensions.gra.scalars_trackers_interpolate(
  raw_tra_2, T_I, method="FH", d=10)

# -----------------------------------------------------------------------------
# now plot

st.extensions.plot_set_defaults()  # set appearance defaults

fig = plt.figure(1, figsize=(8.2, 3.9))
ax_3d = fig.add_subplot(121, projection="3d")
ax_3d.plot3D(*tra_1_I[:,1:].T, 'blue')
ax_3d.plot3D(*tra_2_I[:,1:].T, 'red')

z_max = abs(tra_1_I[:,-1]).max()
zlim = [-1.1 * z_max, 1.1 * z_max]

ax_3d.scatter3D(*tra_1_I[0, 1:].T, cmap='Blues')
ax_3d.scatter3D(*tra_2_I[0, 1:].T, cmap='Reds')
ax_3d.set_title(f"T_f={tra_1_I[-1,0]}")
ax_3d.set_zlim(zlim)


ax_xy = fig.add_subplot(122)
ax_xy.plot(*tra_1_I[:,1:-1].T, 'blue')
ax_xy.plot(*tra_2_I[:,1:-1].T, 'red')

ax_xy.scatter(*tra_1_I[0,1:-1].T, cmap='Blues')
ax_xy.scatter(*tra_2_I[0,1:-1].T, cmap='Reds')
ax_xy.set_aspect(1)
ax_xy.set_title(f"T_f={tra_1_I[-1,0]}")


plt.tight_layout()
plt.subplots_adjust(wspace=0.250, hspace=0.00,
                    top=0.92, bottom=0.08,
                    left=0, right=1.0)

plt.show()


#
# :D
#
