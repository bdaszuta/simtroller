#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Parse GR-Athena++ hdf5 files and make petal plots.

Notes:

For BNS with rho that has a jump use d=0
For metric quantities use d=SCHEME_ORDER
"""
###############################################################################
# python imports
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# package imports
import simtroller as st
###############################################################################

st.extensions.plot_set_defaults()

###############################################################################
# specify directory containing simulation output in athdf and settings

dir_base = "${NR_DATA}/GR-Athena++/bns/"
tag_sim = "d001_fix_ELR_PLM"

# interpolation and axes window
N_I = 1024
d = 0   # set to 0 for raw plot
x_ab = (-40, 40)
y_ab = (-40, 40)
NGHOST = 4
var = "adm.gxx"
var = "rho"
variety = "x1x2"
r_0 = 0.5          # extrema disc radius
extrema_type = +1  # search for maxima
physical_only = True  # control data to use for interpolation
scaling = "linear"
scaling = "log10"

# get only athdf files
dir_sim_list = st.core.io.dir_list([dir_base, tag_sim], recursive=False)
fns_athdf = st.core.io.dir_filter(dir_sim_list,
                                  mode="directories",
                                  extensions=[".athdf"])
fns_txt = st.core.io.dir_filter(dir_sim_list,
                                mode="directories",
                                extensions=[".txt"])

# -----------------------------------------------------------------------------
# inspect athdf to get the type of slices stored

pool = st.core.parallel_pool_start() # spin up a worker pool

###############################################################################
# get trackers
raw_tra_1 = st.extensions.gra.scalars_trackers_process(fns_txt[0],
                                                       deduplicate=True)
raw_tra_2 = st.extensions.gra.scalars_trackers_process(fns_txt[1],
                                                       deduplicate=True)

###############################################################################
# filter hdf5 to files that have "var" and "variety" slices
def hdf5_has_var_slice(filename, var=None, variety=variety):
  VariableNames = st.core.io.hdf5_file_get_attr(filename, "VariableNames")
  fund_var_labels = st.extensions.gra.variable_label_to_sampling_tuple(var)

  fund_var = [
    st.core.io.hdf5_dataset_get(filename, dataset_name=var_label)
    for var_label in fund_var_labels
  ]

  sl_dim, sl_variety = st.extensions.gra.coordinates_identify_slice(
    *fund_var)

  return (variety == sl_variety) and var.encode() in VariableNames

# do the filtering
fns_filtered = st.core.io.hdf5_filenames_filter(
  filenames=fns_athdf,
  condition_function=partial(hdf5_has_var_slice, var=var),
  condition_negate=True, # files that satisfy ^ are dropped so negate
  parallel_pool=pool
)

###############################################################################
# extract times from filtered files and interpolate trackers
def hdf5_get_time(filename):
  return st.core.io.hdf5_file_get_attr(filename, "Time")

# schedule the parsing
par_res = st.core.io.hdf5_filenames_apply_parser(
  fns_filtered, hdf5_get_time, parallel_pool=pool)

sliced_Time = np.array(
  list(st.core.primitives.sequence_generator_flatten(par_res))
)

tra_1_I = st.extensions.gra.scalars_trackers_interpolate(
  raw_tra_1, sliced_Time, method="FH", d=d)
tra_2_I = st.extensions.gra.scalars_trackers_interpolate(
  raw_tra_2, sliced_Time, method="FH", d=d)

###############################################################################
# make plots (w/ interpolation)

def make_petal_figure(filename, Time, var=None, x_ab=None, y_ab=None, d=None,
                      NGHOST=None,
                      tra_1_I=None, tra_2_I=None, physical_only=False,
                      scaling=None):
  fig = plt.figure(1, figsize=(8.5, 8.5))

  gs = fig.add_gridspec(2,2, height_ratios=(5, 2), width_ratios=(2, 5))
  ax_im = fig.add_subplot(gs[0, 1])
  ax_cut_x = fig.add_subplot(gs[1, 1], sharex=ax_im)
  ax_cut_y = fig.add_subplot(gs[0, 0], sharey=ax_im)

  # settings
  x_I = np.linspace(*x_ab, num=N_I)
  y_I = np.linspace(*y_ab, num=N_I)

  # extract and interpolate fields (2d) ---------------------------------------
  var_data, field_data = st.extensions.gra.hdf5_get_field(
    filename, var, physical_only=physical_only, NGHOST=NGHOST)

  var_I = st.extensions.gra.interpolate_2d_MeshBlock_field(
    *var_data, field_data, x_I, y_I, d=d)

  # find extrema --------------------------------------------------------------
  ix_tr = np.argmin(np.abs(tra_1_I[:,0] - Time))
  x0_tr_1, y0_tr_1 = tra_1_I[ix_tr, 1:3]  # x-y from tracker at current time
  x0_tr_2, y0_tr_2 = tra_2_I[ix_tr, 1:3]  # x-y from tracker at current time

  # search based on tracker
  ix_1_max, jx_1_max = st.core.numerical.find_local_extrema_2d(
    x_I, y_I, var_I, x0_tr_1, y0_tr_1, r_0, extrema_type=extrema_type)

  ix_2_max, jx_2_max = st.core.numerical.find_local_extrema_2d(
    x_I, y_I, var_I, x0_tr_2, y0_tr_2, r_0, extrema_type=extrema_type)

  mx_1, my_1 = x_I[ix_1_max], y_I[jx_1_max]
  mx_2, my_2 = x_I[ix_2_max], y_I[jx_2_max]

  # get variable information --------------------------------------------------
  info_vars = st.extensions.gra.hdf5_get_vars(filename)

  # grid spacing
  extrema_dx_a = st.extensions.gra.coordinates_get_dx_extrema(
    info_vars["xf"][0]
  )

  extrema_dx_b = st.extensions.gra.coordinates_get_dx_extrema(
    info_vars["xf"][1]
  )

  # slice X/Y to inferred extrema ---------------------------------------------
  if d > 0:
    C_I_X_1 = st.extensions.gra.interpolate_2d_MeshBlock_field(
      *var_data, field_data,
      x_I, np.array([my_1], dtype=field_data.dtype), d=d)

    C_I_Y_1 = st.extensions.gra.interpolate_2d_MeshBlock_field(
      *var_data, field_data,
      np.array([mx_1], dtype=field_data.dtype), y_I, d=d)

    C_I_X_2 = st.extensions.gra.interpolate_2d_MeshBlock_field(
      *var_data, field_data,
      x_I, np.array([my_2], dtype=field_data.dtype), d=d)

    C_I_Y_2 = st.extensions.gra.interpolate_2d_MeshBlock_field(
      *var_data, field_data,
      np.array([mx_2], dtype=field_data.dtype), y_I, d=d)

  else:
    C_I_X_1 = st.core.numerical.grid_2d_interpolate_uniform_scipy(
      *var_data, field_data, x_I.size, 1,
      (x_I.min(), x_I.max()), (my_1, my_1 + extrema_dx_b[0]),
      method="linear"
    )[-1].flatten()

    C_I_Y_1 = st.core.numerical.grid_2d_interpolate_uniform_scipy(
      *var_data, field_data, 1, y_I.size,
      (mx_1, mx_1 + extrema_dx_a[0]), (y_I.min(), y_I.max()),
      method="linear"
    )[-1].flatten()

    C_I_X_2 = st.core.numerical.grid_2d_interpolate_uniform_scipy(
      *var_data, field_data, x_I.size, 1,
      (x_I.min(), x_I.max()), (my_2, my_2 + extrema_dx_b[0]),
      method="linear"
    )[-1].flatten()

    C_I_Y_2 = st.core.numerical.grid_2d_interpolate_uniform_scipy(
      *var_data, field_data, 1, y_I.size,
      (mx_2, mx_2 + extrema_dx_a[0]), (y_I.min(), y_I.max()),
      method="linear"
    )[-1].flatten()


  # plot fields ---------------------------------------------------------------
  if scaling == "linear":
    sf = lambda x: x
  elif scaling == "log10":
    sf = lambda x: np.log10(x)
  else:
    raise ValueError("Unknown scaling")

  if d == 0:
    st.extensions.gra.plot_2d_raw(
      ax_im, *var_data, sf(field_data), cmap="inferno"
    )
  else:
    st.extensions.gra.plot_2d_interpolated(
      ax_im, x_I, y_I, sf(var_I), cmap="inferno"
    )

  # overlay Mesh Structure
  st.extensions.gra.plot_2d_MeshBlock_structure(
    ax_im, filename, NGHOST=NGHOST, color="w"
  )

  ax_im.set_xlim([x_I.min(), x_I.max()])
  ax_im.set_ylim([y_I.min(), y_I.max()])

  # sliced petals
  ax_cut_x.plot(x_I, sf(C_I_X_1.flatten()), "-g", linewidth=0.8)
  ax_cut_y.plot(sf(C_I_Y_1.flatten()), y_I, "-c", linewidth=0.8)

  ax_cut_x.plot(x_I, sf(C_I_X_2.flatten()), "--g", linewidth=0.8)
  ax_cut_y.plot(sf(C_I_Y_2.flatten()), y_I, "--c", linewidth=0.8)


  # cosmetic ------------------------------------------------------------------
  # place extrema
  ax_im.plot(mx_1, my_1, 'o', color=[0, 1, 0], markersize=6)
  ax_im.text(mx_1 + 2, my_1-4, f"({mx_1:.2e},{my_1:.2e})",
            color=[0, 1, 0],
            fontsize=8)

  ax_im.plot(mx_2, my_2, 'o', color=[0, 1, 0], markersize=6)
  ax_im.text(mx_2 + 2, my_2-4, f"({mx_2:.2e},{my_2:.2e})",
            color=[0, 1, 0],
            fontsize=8)

  # place slicers
  ax_im.axhline(my_1, color="g", linewidth=0.8)
  ax_im.axvline(mx_1, color="c", linewidth=0.8)

  ax_im.axhline(my_2, color="g", linestyle="--", linewidth=0.8)
  ax_im.axvline(mx_2, color="c", linestyle="--", linewidth=0.8)

  # labels and metadata
  X_label = info_vars["xf_labels"][0]
  Y_label = info_vars["xf_labels"][1]

  ax_cut_x.set_xlabel(X_label)
  ax_cut_y.set_ylabel(Y_label)

  ax = fig.get_axes()[-1]

  fig.text(-0.5, 0.7, f"Time: {Time}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  fig.text(-0.5, 0.5, f"Field: {var}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  fig.text(-0.5, 0.3, f"dx_extr[{X_label}]: {extrema_dx_a}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  fig.text(-0.5, 0.1, f"dx_extr[{Y_label}]: {extrema_dx_b}",
          horizontalalignment="left", verticalalignment="center",
          transform=ax_cut_x.transAxes)

  # figure geometry etc
  divider_x = make_axes_locatable(ax_cut_x)
  cl_ax = divider_x.append_axes("right", size="5%", pad=0.02)
  cl_ax.set_visible(False)

  ax_im.xaxis.set(visible=False)
  ax_im.yaxis.set(visible=False)

  # deal with ticks
  ax_cut_x.yaxis.tick_right()
  ax_cut_y.xaxis.tick_bottom()

  for ax in fig.get_axes():
    ax.tick_params(direction="in")

  plt.subplots_adjust(wspace=0.01, hspace=0.05,
                      top=0.930, bottom=0.090,
                      left=0.090, right=0.930)


  plt.savefig(f"{var}_{scaling}_{Time:016.5f}.png")
  plt.clf()
  return Time


# distribute work to pool
par_res = [
  st.core.parallel_pool_apply_async(
    pool,
    make_petal_figure,
    (filename, Time),
    kwds={"var": var, "x_ab": x_ab, "y_ab": y_ab, "d": d,
          "NGHOST": NGHOST, "tra_1_I": tra_1_I, "tra_2_I": tra_2_I,
          "physical_only": physical_only, "scaling": scaling},
    method="dill")
  for filename, Time in zip(fns_filtered, sliced_Time)
]

# process work
for ix, pr in enumerate(par_res):
  cur_Time = pr.get()
  print(cur_Time)

#
# :D
#
