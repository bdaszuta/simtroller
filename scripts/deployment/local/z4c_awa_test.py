#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 ,-*
(_)

@author: Boris Daszuta
@function: Deploy GR-Athena++ and run a reduced robust stability test.

It is assumed that spack is being used and has been properly activated.
This can be changed by modifying the associated .tpl file.
"""
###############################################################################
# python imports
import numpy as np

# package imports
import simtroller as st
###############################################################################

# settings ====================================================================
# where to deploy code repository
dir_tar = "/tmp/simtroller_deploy/awa/exec"

# script directory and filename
dir_cmp = "/tmp/simtroller_deploy/awa"
fn_cmp = "compile.sh"
fn_run = "run.sh"
fn_par = "input.par"

# where "development" branch folder exists
dir_nr = "${repos}/numerical_relativity"
dir_gra = [dir_nr, "athena", "gr_athena", "development"]

# location for par / runs / input files
dir_par = "/tmp/simtroller_deploy/awa/par"
dir_data = "/tmp/simtroller_deploy/awa/data"
dir_data_scalar = "/tmp/simtroller_deploy/awa/data_scalar"

# templates -----------------------------------------------
tpl_sh_compile = "z4c_awa_test.tpl.sh.compile"
tpl_sh_run     = "z4c_awa_test.tpl.sh.run"
tpl_par        = "z4c_awa_test.tpl.par"

# template replacement rules ---------------
rep_rules_compile = {
  "MAKE_THREADS": 16,
  "DIR_ATHENA":   dir_tar,
  "NGHOST": 4,
  "NEXTRAPOLATE": 5,
  "SPACK_ENV": "nr_gcc_openmpi"
}

# additional replacements for run
rep_rules_run = {
  "DIR_DATA": dir_data,
  "FILE_PAR": st.core.io.path_to_absolute([dir_par, fn_par], verify=False),
  "MPI_PROC": 4
}
rep_rules_run.update(rep_rules_compile)

# what to replace in the parameter file
rho = 2
N_M1 = 50
N_B = 4
h = 1 / (N_M1 * rho)

rep_rules_par = {
  "sss": dir_data_scalar,
  "test_choice": "robust_stability",
  "SHIFT_ETA":       2.0,
  "KO_DISS":         0.02,
  "KAP1":            0.02,
  "KAP2":            0.0,
  "CFL":             0.1,
  "TLIM":            1000,
  "AWA_AMPLITUDE":   1e-10 / rho ** 2,
  "AWA_D_X":         1.0,
  "AWA_D_Y":         1.0,
  # Mesh / MeshBlock
  "NM1":  rho * N_M1,
  "NM2":  N_B,
  "NM3":  N_B,
  "NB1":  10,
  "NB2":  N_B,
  "NB3":  N_B,
  "OMP_NUM_THREADS": 2,
}

# update grids
rep_rules_par.update({
  "LX": rep_rules_par["NM1"] * h / 2,
  "LY": rep_rules_par["NM2"] * h / 2,
  "LZ": rep_rules_par["NM3"] * h / 2,
})

# =============================================================================

# dir of this script ==========================================================
dir_scr = [
  st.core.io.path_cwd_get(),
  st.core.io.path_strip(__file__, mode="filename")]

# deploy ======================================================================
dir_rep = st.core.io.path_to_absolute(dir_gra)

if not st.core.io.path_is_extant(dir_rep):
  raise RuntimeError("Source repository does not exist.")

# extract branch and HEAD hash
info_rep = st.core.shell.repository_get_info(dir_rep)

# deploy repository to target (strips git meta)
if not st.core.io.path_is_extant(dir_tar):
  st.core.shell.rsync(dir_rep, dir_tar)
else:
  dir_tar = st.core.io.path_to_absolute(dir_tar)
  st.core.shell.print_signal(f"Something already deployed at {dir_tar}")

# make a tag based on repo. information in target
st.core.io.file_ensure_extant(
  [dir_tar, st.core.hash_generate(info_rep)]
)

# prepare compilation script ==================================================

# load the template, perform replacements and write
raw_tpl = st.core.io.raw_load([dir_scr, "tpl", tpl_sh_compile])

parsed_cmp_tpl = st.core.primitives.str_delimiter_replace(
  raw_tpl, rep_rules_compile, delimiters=("[[", "]]")
)

fn_res_cmp = st.core.io.path_to_absolute([dir_cmp, fn_cmp], verify=False)
st.core.io.raw_dump(parsed_cmp_tpl, fn_res_cmp)

# prepare run script ==========================================================
raw_tpl = st.core.io.raw_load([dir_scr, "tpl", tpl_sh_run])

parsed_run_tpl = st.core.primitives.str_delimiter_replace(
  raw_tpl, rep_rules_run, delimiters=("[[", "]]")
)

fn_res_run = st.core.io.path_to_absolute([dir_cmp, fn_run], verify=False)
st.core.io.raw_dump(parsed_run_tpl, fn_res_run)

# prepare parameter file ======================================================
raw_tpl = st.core.io.raw_load([dir_scr, "tpl", tpl_par])

rep_rules_par.update(
  {"REPO_BRANCH": info_rep["branch"],
   "REPO_HASH":   info_rep["HEAD_hash"]
   }
)

parsed_par_tpl = st.core.primitives.str_delimiter_replace(
  raw_tpl, rep_rules_par, delimiters=("[[", "]]")
)

fn_res_par = st.core.io.path_to_absolute([dir_cmp, "par", fn_par],
                                         verify=False)
st.core.io.raw_dump(parsed_par_tpl, fn_res_par)


# prepare output directory structures =========================================
st.core.io.dir_ensure_extant([dir_par])
st.core.io.dir_ensure_extant([dir_data])
st.core.io.dir_ensure_extant([dir_data_scalar])

# dump info ===================================================================

st.core.shell.print_info("info_rep")
print(info_rep)

st.core.shell.print_info("Compile with:")
print(f"bash {fn_res_cmp}")

st.core.shell.print_info("Run with:")
print(f"bash {fn_res_run}")


#
# :D
#
